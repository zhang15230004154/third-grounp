# 张伟山
## 2021.9.6
1. 文章阅读

- [函数的length是多少](https://juejin.cn/post/7003369591967596552)

- [继承方式有哪些](https://juejin.cn/post/7004019825924112391)

2. 源码阅读

3. leecode 刷题

- [“气球” 的最大数量](https://leetcode-cn.com/problems/maximum-number-of-balloons/)
- [找不同](https://leetcode-cn.com/problems/find-the-difference/)


## 2021.9.5

1. 文章阅读

- [原型，原型链](https://juejin.cn/post/7003267384358207501)

- [位运算](https://juejin.cn/post/7003227296941211684)

2. 源码阅读


3. leecode 刷题

- [合并排序的数组](https://leetcode-cn.com/problems/sorted-merge-lcci/)
- [Fizz Buzz](https://leetcode-cn.com/problems/fizz-buzz/)
- [最富有客户的资产总量](https://leetcode-cn.com/problems/richest-customer-wealth/)


## 2021.9.3

1. 文章阅读

- [异步编程梳理](https://juejin.cn/post/7002456485674369054)

- [深拷贝和浅拷贝](https://juejin.cn/post/7002446425036423205)

2. 源码阅读

3. leecode 刷题

- [重新排列数组](https://leetcode-cn.com/problems/shuffle-the-array/)
- [删除回文子序列](https://leetcode-cn.com/problems/remove-palindromic-subsequences/)


## 2021.9.2

1. 文章阅读

- [JS垃圾回收机制](https://juejin.cn/post/7002475280577085477)

- [说说作用域](https://juejin.cn/post/7002425151014830088#heading-1)

2. 源码阅读


3. leecode 刷题

- [找出数组最大公约数](https://leetcode-cn.com/problems/find-greatest-common-divisor-of-array/)
- [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)

## 2021.9.1

1. 文章阅读

- [JSON.stringify 的 “魅力” ](https://juejin.cn/post/7001454317450297380#heading-14)

- [js鼠标和键盘事件](https://juejin.cn/post/7001138780128149535)

2. 源码阅读


3. leecode 刷题

- [字符串中不同整数的数目](https://leetcode-cn.com/problems/number-of-different-integers-in-a-string/)
- [三除数](https://leetcode-cn.com/problems/three-divisors/)

### 2020.8.31

1. 文章阅读

- [mobx 是什么](https://www.bbsmax.com/A/LPdoegg8J3/)
- [mobx 有什么优点](https://www.bbsmax.com/A/LPdoegg8J3/)

2. 源码阅读

- []()
- []()

3. leetcode 刷题

- [字符串中的最大奇数](https://leetcode-cn.com/problems/largest-odd-number-in-string/)

### 2020.8.30

1. 文章阅读

- [WebSocket 是什么](https://baike.baidu.com/item/WebSocket/1953845?fr=aladdin)
- [WebSocket 有什么优点](https://baike.baidu.com/item/WebSocket/1953845?fr=aladdin)

2. 源码阅读

- []()

3. leetcode 刷题

- [三个数的最大乘积](https://leetcode-cn.com/problems/maximum-product-of-three-numbers/)

### 2020.8.29

1. 文章阅读

- []()
- []()

2. 源码阅读

- []()

3. leetcode 刷题
- [检查整数及其两倍数是否存在](https://leetcode-cn.com/problems/check-if-n-and-its-double-exist/)

### 2020-8-23

1. 文章阅读

- [VUE 双向数据绑定原理和实现](https://www.jianshu.com/p/e7ebb1500613)
- [vue 路由的实现原理是什么](https://www.cnblogs.com/gaosirs/p/10606266.html)

2. 源码阅读

- [\_.take(array, [n=1]) ](https://www.lodashjs.com/docs/lodash.take)

3. leetcode 刷题
- [反转字符串II](https://leetcode-cn.com/problems/reverse-string-ii/)

### 2020-8-22

1. 文章阅读

- [vue 的生命周期](https://www.cnblogs.com/wzndkj/p/9612647.html)
- [Vue 中的 watch 与 computed](https://www.cnblogs.com/gunelark/p/8492468.html)

2. 源码阅读

- [\_.slice(array, [start=0], [end=array.length])](https://www.lodashjs.com/docs/lodash.slice)

3. leetcode 刷题

### 2020-8-20

1. 文章阅读

- [vue2.0 中的$router 和 $route 的区别](https://www.cnblogs.com/czy960731/p/9288830.html)
- [Vue 中的 watch 与 computed](https://www.cnblogs.com/gunelark/p/8492468.html)

2. 源码阅读

- [\_.sortedIndex(array, value)](https://www.lodashjs.com/docs/lodash.sortedIndex)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1012/)

### 2020-8-18

1. 文章阅读

- [请求方式](https://zhuanlan.zhihu.com/p/109234011)
- [Http 请求传参的三种方式](https://blog.csdn.net/weixin_42264805/article/details/109199958)

2. 源码阅读

- [\_.sortedIndex(array, value)](https://www.lodashjs.com/docs/lodash.sortedIndex)

3. leetcode 刷题

- [无重复字符的最长子串](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1012/)

### 2020-8-17

1. 文章阅读

- [跨域](https://www.jianshu.com/p/8fa2acd103ea)
- [跨域缺点](https://www.cnblogs.com/geilishu/p/4999860.html)

2. 源码阅读

- [\_.reverse(array)](https://www.lodashjs.com/docs/lodash.reverse)
- [\_.slice(array, [start=0], [end=array.length])](https://www.lodashjs.com/docs/lodash.slice)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/leetbook/read/tencent/xxzqki/)

### 2021-8-16

1. 文章阅读

- [--js 对象转数组](https://www.cnblogs.com/ll15888/p/11993334.html)

2. 源码阅读

- [--join(array, [separator=','])](https://www.lodashjs.com/docs/lodash.join)
- [--last(array)](https://www.lodashjs.com/docs/lodash.last)

### 2021-8-15

1. 文章阅读

- [--vue 路由传参](https://www.jianshu.com/p/5dff306fdb0f)

2. 源码阅读

- [--differenceWith(array, [values], [comparator])](https://www.lodashjs.com/docs/lodash.differenceWith)

3. leetcode 刷题

- [--三数之和](https://leetcode-cn.com/leetbook/read/tencent/xxst6e/)

### 2021-8-13

1. 文章阅读

- [--Hooks 的问题](https://jasonandjay.github.io/study/zh/standard/Hooks.html#hooks%E7%9A%84%E9%97%AE%E9%A2%98)
- [--Hooks 的原理](https://jasonandjay.github.io/study/zh/standard/Hooks.html#hooks%E7%9A%84%E9%97%AE%E9%A2%98)

2. 源码阅读

- [--compact(array)](https://www.lodashjs.com/docs/lodsh.compact)

3. leetcode 刷题

- [--删除排序数组中的重复项](https://leetcode-cn.com/leetbok/read/top-interview-questions-easy/x2gy9m/)

### 2021-8-12

1. 文章阅读

- [--vue 常见的生命周期](https://www.jianshu.com/p/672e967e201c)
- [--vue 的双向绑定原理](https://www.cnblogs.com/zhenfei-jiang/p/7542900.html)

2. 源码阅读

- [--chunk(array, [size=1])](https://www.lodashjs.com/docs/lodash.chunk)
- [differenceBy](https://www.lodashjs.com/docs/lodash.differenceBy)

3. leetcode 刷题

- [--两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)


# 陈欢欢

### 2021-9-26
- [数据类型 typeof instanceof Object.prototype.toString.call()](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/C489D2666ED045E29EAA744E71AB30F9)

2. 源码阅读
- [.flatten](https://www.lodashjs.com/docs/lodash.flatten)

### 2021-9-25
- [call bind apply 的区别](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/C489D2666ED045E29EAA744E71AB30F9)

2. 源码阅读
- [.findIndex](https://www.lodashjs.com/docs/lodash.findIndex)

### 2021-9-24
- [es6的新特性都有哪些,箭头函数](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/C489D2666ED045E29EAA744E71AB30F9)
- [==和===区别是什么](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/C489D2666ED045E29EAA744E71AB30F9)

2. 源码阅读
- [findLastIndex](https://www.lodashjs.com/docs/lodash.findLastIndex)

### 2021-9-23
- [伪数组](https://blog.csdn.net/linjiehuijh/article/details/84788233)
- [webpack与grunt、gulp的不同](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/01FEB884B34848D0B7175F455616ED88)

2. 源码阅读
- [flatten](https://www.lodashjs.com/docs/lodash.flatten)

### 2021-9-22
- [v-if和v-show区别](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/D080D91D9E9E4D0DB4FAA04661D1CA73)

2. 源码阅读
- [head](https://www.lodashjs.com/docs/lodash.head)

### 2021-9-18
- [async与await](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/5273CE6D3E8B4C30863BB598A446F571)

2. 源码阅读
- [initial](https://www.lodashjs.com/docs/lodash.initial)

### 2021-9-17
- [闭包](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/49ADDC2D4BF247329C7564B160F7A6A7)
- [防抖节流](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/49ADDC2D4BF247329C7564B160F7A6A7)

2. 源码阅读
- [intersection](https://www.lodashjs.com/docs/lodash.intersection)

### 2021-9-16
- [vue和react的区别](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/49ADDC2D4BF247329C7564B160F7A6A7)
- [原型链](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/49ADDC2D4BF247329C7564B160F7A6A7)

2. 源码阅读
- [intersectionWith](https://www.lodashjs.com/docs/lodash.intersectionWith)

### 2021-9-15
- [Object.keys和Object.values](https://www.cnblogs.com/xzybk/p/14091293.html)
- [组件内date为什么必须是函数](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/49ADDC2D4BF247329C7564B160F7A6A7)

2. 源码阅读
- [fill](https://www.lodashjs.com/docs/lodash.fill)

### 2021-9-14
- [.sync修饰符](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/49ADDC2D4BF247329C7564B160F7A6A7)

2. 源码阅读
- [head](https://www.lodashjs.com/docs/lodash.head)

### 2021-9-13
- [http和https的区别](https://juejin.cn/post/7002061062593839118)

2. 源码阅读
- [flatten](https://www.lodashjs.com/docs/lodash.flatten)

### 2021-9-12
1. 文章阅读
- [带你掌握JS防抖与节流](https://juejin.cn/post/7002061062593839118)

2. 源码阅读
- [dropRightWhile](https://www.lodashjs.com/docs/lodash.dropRightWhile)

### 2021-9-10
1. 文章阅读
- [vue的实现原理](https://juejin.cn/post/6844904183938678798)

2. 源码阅读
- [dropRight](https://www.lodashjs.com/docs/lodash.dropRight)

### 2021-9-9
1. 文章阅读
- [浅谈React合成事件](https://juejin.cn/post/6991645668934680584)

2. 源码阅读
- [drop](https://www.lodashjs.com/docs/lodash.drop)

### 2021-9-8
1. 文章阅读
- [Ajax前后端交互利器详解(一)](https://blog.csdn.net/Augenstern_QXL/article/details/120116296?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)

2. 源码阅读
- [difference](https://www.lodashjs.com/docs/lodash.difference)

### 2021-9-7

1. 文章阅读

- [深浅拷贝与 for…of/infor…of 与 for…in](https://blog.csdn.net/qq_43432158/article/details/120130955?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)

2. 源码阅读

- [difference](https://www.lodashjs.com/docs/lodash.difference)

### 2021-9-6

1. 文章阅读

- [promise 是什么？为什么用 promise？怎样使用 promise？拓展 async 与 await？事件循环？](https://blog.csdn.net/a1016428360/article/details/120107731?utm_medium=distribute.pc_category.none-task-blog-hot-6.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-6.nonecase)

2. 源码阅读

- [箭头函数=＞](https://blog.csdn.net/qq_32442973/article/details/120106911?utm_medium=distribute.pc_category.none-task-blog-hot-7.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-7.nonecase)

3. leetcode 刷题

- [删除有序数组中的重复项 II](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/)

### 2021-9-4

1. 文章阅读

- [javascript/js 面向对象：原型、继承概念](https://blog.csdn.net/qq_42146383/article/details/120107147?utm_medium=distribute.pc_category.none-task-blog-hot-9.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-9.nonecase)

2. 源码阅读

- [作用域与执行上下文的概念及区别](https://blog.csdn.net/ksjdbdh/article/details/120111962?utm_medium=distribute.pc_category.none-task-blog-hot-10.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-10.nonecase)

3. leetcode 刷题

### 2021-9-3

1. 文章阅读

- [call,bind,apply 的区别](https://www.jianshu.com/p/e7ebb1500613)

2. 源码阅读

- [【JavaScript】ES6 对象](https://blog.csdn.net/Superman_H/article/details/120107862?utm_medium=distribute.pc_category.none-task-blog-hot-4.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-4.nonecase)

3. leetcode 刷题

### 2021-9-2

1. 文章阅读

- [JavaScript 事件——鼠标事件，键盘事件，冒泡事件](https://blog.csdn.net/m0_59150380/article/details/120121455?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)

2. 源码阅读

- [trim](https://www.lodashjs.com/docs/lodash.take)

### 2021-9-2

1. 文章阅读

- [async/await:让异步编程更简单](https://blog.csdn.net/weixin_52691965/article/details/120060385?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)

2. 源码阅读

- [scheduleWork](https://react.jokcy.me/book/flow/schedule-work.html)

3. leetcode 刷题

### 2021-9-1

1. 文章阅读

- [对 Node 的优点缺点提出自己的看法](https://www.jianshu.com/p/e7ebb1500613)

2. 源码阅读

- [trim](https://www.lodashjs.com/docs/lodash.take)

3. leetcode 刷题

### 2021-8-31

1. 文章阅读

- [事件的三个阶段](https://www.jianshu.com/p/e7ebb1500613)
- [阻止冒泡](https://www.cnblogs.com/gaosirs/p/10606266.html)

2. 源码阅读

- [charAt](https://www.lodashjs.com/docs/lodash.take)

3. leetcode 刷题

### 2021-8-30

1. 文章阅读

- [哪些内存会造成内存泄露](https://www.jianshu.com/p/e7ebb1500613)
- [IE 与其他浏览器不一样的特性](https://www.cnblogs.com/gaosirs/p/10606266.html)

2. 源码阅读

- [concat](https://www.lodashjs.com/docs/lodash.take)

3. leetcode 刷题

### 2021-8-29

1. 文章阅读

- [解释一下时间代理](https://www.jianshu.com/p/e7ebb1500613)

2. 源码阅读

- [match](https://www.lodashjs.com/docs/lodash.take)

3. leetcode 刷题

### 2021-8-27

1. 文章阅读

- [什么是闭包](https://blog.csdn.net/u012194956/article/details/82595270)

2. 阅读源码

- [includes](https://blog.csdn.net/weixin_38912024/article/details/100697963)
- [indexOf](https://www.w3school.com.cn/jsref/jsref_indexOf.asp)

### 2021-8-26

1. 文章阅读

- [vue 组件之间的通讯](https://www.cnblogs.com/barryzhang/p/10566515.html)

2. 阅读源码

- [forEach](https://www.runoob.com/jsref/jsref-forEach.html)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/leetbook/read/tencent/xxzqki/)

### 2021-8-26

1. 文章阅读

- [vue 组件之间的通讯](https://www.cnblogs.com/barryzhang/p/10566515.html)

2. 阅读源码

- [forEach](https://www.runoob.com/jsref/jsref-forEach.html)

3. leetcode 刷题

### 2021-8-25

1. 文章阅读

- [for...in 和 for...of](https://www.cnblogs.com/rogerwu/p/10738776.html)

2. 阅读源码

- [find](https://www.runoob.com/jsref/jsref-filter.html)

3. leetcode 刷题

- [findIndex](https://www.runoob.com/jsref/jsref-findindex.html)

### 2021-8-24

1. 文章阅读

- [vue 的路由传参方式](https://www.jianshu.com/p/d276dcde6656)

2. 阅读源码

- [filter](https://www.runoob.com/jsref/jsref-filter.html)

3. leetcode 刷题

- [多数元素](https://leetcode-cn.com/leetbook/read/tencent/x5yxxs/)

### 2021-8-23

1. 文章阅读

- [常用 hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B8%B8%E7%94%A8hooks)

2. 阅读源码

- [fill](https://www.runoob.com/jsref/jsref-fill.html)

3. leetcode 刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/tencent/x5mohi/)

### 2021-8-22

1. 文章阅读

- [vue 的生命周期](https://zhuanlan.zhihu.com/p/94601709)

2. 阅读源码

- [every](https://www.runoob.com/jsref/jsref-every.html)

### 2021-8-20

1. 文章阅读

- [vue 实现双向绑定原理](https://www.jianshu.com/p/7e3be3d619e0)

2. 阅读源码

- [copyWithin](https://www.runoob.com/jsref/jsref-copywithin.html)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/leetbook/read/tencent/xxzqki/)

### 2021-8-19

1. 文章阅读

- [阻止冒泡](http://caibaojian.com/javascript-stoppropagation-preventdefault.html)

2. 阅读源码

- [\_.stubString](https://www.lodashjs.com/docs/lodash.toUpper)

3. leetcode 刷题

- [冒泡排序](https://blog.csdn.net/lu_1079776757/article/details/80459370)

### 2021-8-18

1. 文章阅读

- [JavaScript 介绍 this 指向的各种情况](https://blog.csdn.net/chai_tian/article/details/119731046?utm_medium=distribute.pc_category.none-task-blog-hot-11.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-11.nonecase)

2. 阅读源码

- [\_.toUpper](https://www.lodashjs.com/docs/lodash.toUpper)

3. leetcode 刷题

- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

### 2021-8-17

1. 文章阅读

- [简单理解深拷贝和浅拷贝的区别](https://blog.csdn.net/weixin_44984664/article/details/104577580?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162915667416780255247326%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162915667416780255247326&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_click~default-2-104577580.first_rank_v2_pc_rank_v29&utm_term=%E6%B7%B1%E6%8B%B7%E8%B4%9D%E5%92%8C%E6%B5%85%E6%8B%B7%E8%B4%9D%E7%9A%84%E5%8C%BA%E5%88%AB&spm=1018.2226.3001.4187)

2. 阅读源码

- [\_.bind](https://www.lodashjs.com/docs/lodash.bind)

### 2021-8-16

1. 文章阅读

- [javascript 学习系列(8):数组中的 splice 方法](https://blog.csdn.net/weixin_43392489/article/details/119654871?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)
- [javascript 学习系列(7):数组中的 concat 方法](https://blog.csdn.net/weixin_43392489/article/details/119654836?utm_medium=distribute.pc_category.none-task-blog-hot-3.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-3.nonecase)
- [javascript 学习系列(5):数组中的 reduce 方法](https://blog.csdn.net/weixin_43392489/article/details/119654669?utm_medium=distribute.pc_category.none-task-blog-hot-4.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-4.nonecase)

2. 阅读源码

- [\_.remove](https://www.lodashjs.com/docs/lodash.join)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

### 2021-8-15

1. 文章阅读

- [--js 对象转数组](https://www.cnblogs.com/ll15888/p/11993334.html)

2. 源码阅读

- [--join(array, [separator=','])](https://www.lodashjs.com/docs/lodash.join)
- [--last(array)](https://www.lodashjs.com/docs/lodash.last)

### 2021-8-13

1. 文章阅读

- [iframe 有那些缺点?](https://www.cnblogs.com/ninghu996926/p/11677582.html)
- [CSS 优先级算法如何计算？](https://blog.csdn.net/yangxuesong5555/article/details/79417595)
- [vue 生命周期](https://www.cnblogs.com/happ0/p/8075562.html)

2. 阅读源码

- [\_.indexOf](https://www.lodashjs.com/docs/lodash.indexOf)
- [\_.every](https://www.lodashjs.com/docs/lodash.every)

3. leetcode 刷题

- [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/comments/)

### 2021-8-12

1. 文章阅读

- [JavaScript 闭包](https://blog.csdn.net/weixin_51033461/article/details/119561476?utm_medium=distribute.pc_category.none-task-blog-hot-6.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-6.nonecase)

2. 阅读源码

- [\_.compact](https://www.lodashjs.com/docs/lodash.compact)
- [\_.difference](https://www.lodashjs.com/docs/lodash.difference)

# 杜康鑫

### 2021-9-24

- 文章阅读

 - [这些原生DOM操作你还记住多少](https://juejin.cn/post/6966062224892756005)
 - [【js 进阶】全篇干货 ！一篇文章让你彻底弄懂栈、堆、队列、执行栈、上下文、事件循环（Event Loop）](https://juejin.cn/post/6966158666030383118)

2. 源码阅读

 - [【js 进阶】全篇干货 ！一篇文章让你彻底弄懂栈、堆、队列、执行栈、上下文、事件循环（Event Loop）](https://juejin.cn/post/6966158666030383118)

3. leecode 刷题

 - [括号生成](https://leetcode-cn.com/problems/generate-parentheses/)
 - [数组全排列](

- https://www.lodashjs.com/docs/lodash.findLastIndex)

### 2021-9-23

- 文章阅读

 - [2020 前端面试 | 第一波面试题总结](https://juejin.cn/post/6844904062257725447#heading-3)
 - [2020最新：100道有答案的前端面试题（上）](https://juejin.cn/post/6847902225423925255)

2. 源码阅读

 - [Android 原生webview传递header前端H5如何接收](https://juejin.cn/post/6965336033395212302)
 - [用5个示例理解 CSS 变量](https://juejin.cn/post/6966387853483835429)

3. leecode 刷题

 - [反转链表](https://leetcode-cn.com/problems/reverse-linked-list/submissions/)
 - [整数反转](

- https://www.lodashjs.com/docs/lodash.flatten)

### 2021-9-22

- 文章阅读

 - [前端监控和前端埋点方案设计](https://juejin.cn/post/6844903650603565063)
 - [Web自适应布局你需要知道的所有事儿](https://juejin.cn/post/6844903518956961805)

2. 源码阅读

 - [JavaScript 如何在线解压 ZIP 文件？](https://juejin.cn/post/6971197120250396680)

3. leecode 刷题

 - [三合一](

- https://www.lodashjs.com/docs/lodash.head)

### 2021-9-18

- [async与await](https://note.youdao.com/ynoteshare/index.html?id=5550819acdce386f0d67c7af71bda83a&type=notebook&_time=1631488716232#/5273CE6D3E8B4C30863BB598A446F571)

2. 源码阅读

- [initial](https://www.lodashjs.com/docs/lodash.initial)

### 2021-9-17

- 文章阅读

 - [前端自动上线系统](https://juejin.cn/post/6844903507498123271)
 - [Vue v3.1.0 Pluto 发布](https://juejin.cn/post/6971311349267709966)

2. 源码阅读

 - [JavaScript 如何在线解压 ZIP 文件？](https://juejin.cn/post/6971197120250396680)

3. leecode 刷题

 - [消失的数字](

- https://www.lodashjs.com/docs/lodash.intersection)

### 2021-9-16

- 文章阅读

 - [TypeScript 学习笔记](https://juejin.cn/post/6969457470783619103 )
 - [手写简易浏览器](https://juejin.cn/post/6969575610108772359)

2. 源码阅读

 - [生产环境js错误收集及定位源码位置](https://juejin.cn/post/6960108736966819848)
 - [Node.js eventloop + 线程池源码分析（建议精读）](https://juejin.cn/post/6951303053739819038)

3. leecode 刷题

 - [二叉树前序遍历](https://leetcode-cn.com/leetbook/read/data-structure-binary-tree/xeywh5/)
 - [二叉树中序遍历](

- https://www.lodashjs.com/docs/lodash.intersectionWith)

### 2021-9-15

- 文章阅读

 - [浏览器缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E5%9B%BE%E8%A7%A3)
 - [前端缓存最佳实践](https://juejin.cn/post/6844903737538920462)

2. 源码阅读

 - [Vue 源码解读（3）—— 响应式原理](https://juejin.cn/post/6950826293923414047)
 - [Vue 源码解读（4）—— 异步更新](https://juejin.cn/post/6951568091893465102)

3. leecode 刷题

- [配对交换](https://leetcode-cn.com/problems/exchange-lcci/)
- [最大数值](

- https://www.lodashjs.com/docs/lodash.fill)

### 2021-9-14

- 文章阅读

- [React进阶](https://juejin.cn/post/6963053793613185031)
- [webpack](https://juejin.cn/post/6961961165656326152)

2. 源码阅读

- [React 组件性能优化最佳实践](https://juejin.cn/post/6965747225154732069)
- [大道理小聪明系列 - Redux + Hooks 工程实践一则](https://juejin.cn/post/6965732983781195807)

3. leecode 刷题

- [有效的括号](https://leetcode-cn.com/problems/valid-parentheses/solution/valid-parentheses-fu-zhu-zhan-fa-by-jin407891080/)
- [组数总和](

- https://www.lodashjs.com/docs/lodash.head)

### 2021-9-13

- 文章阅读
  -[new Map()使用方法](https://www.jianshu.com/p/c9a55b591948)

 - [ES6学习☞set和map数据结构](https://www.jianshu.com/p/9be458a73e05)

2. 源码阅读

  - [用 ObjectComponent 重新定义 React 组件](https://juejin.cn/post/6969536778927603725)
  - [Android 原生webview传递header前端H5如何接收](https://juejin.cn/post/6965336033395212302)

3. leecode 刷题

- [数组每一项相加之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [合并对象](

- https://www.lodashjs.com/docs/lodash.flatten)

### 2021-9-12

1. 文章阅读

- [我们并不需要 Deno](https://juejin.cn/post/6969848850374721544)
- [前端需要了解的计算机网络知识](https://juejin.cn/post/6844904079974465544)

2. 源码阅读

- [生产环境js错误收集及定位源码位置](https://juejin.cn/post/6960108736966819848)
- [ MindSpore 人脸识别](https://juejin.cn/post/6965741051646574628)

3. leecode 刷题

- [左旋转字符串](https://leetcode-cn.com/problems/zuo-xuan-zhuan-zi-fu-chuan-lcof/)
- [数组中重复的数字](

1. https://www.lodashjs.com/docs/lodash.dropRightWhile)

### 2021-9-10

1. 文章阅读

 - [Web开发者需要知道s://juejin.cn/post/6844903576561516558)
 - [WebSocket：5分钟从入门到精通](https://juejin.cn/post/6844903544978407431)

2. 源码阅读

  - [Android 原生webview传递header前端H5如何接收](https://juejin.cn/post/6965336033395212302)
  - [用5个示例理解 CSS 变量](https://juejin.cn/post/6966387853483835429)

3. leecode 刷题

  - [最大重复子字符串](https://leetcode-cn.com/problems/maximum-repeating-substring/)
  - [速算机器人](

1. https://www.lodashjs.com/docs/lodash.dropRight)

### 2021-9-9

1. 文章阅读

  - [axios拦截器封装http请求，刷新token重发请求](https://juejin.cn/post/6844903894481371143)
  - [闭包](https://juejin.cn/post/6844903612879994887)

2. 源码阅读

3. leecode 刷题

  - [替换所有问号](https://leetcode-cn.com/problems/replace-all-s-to-avoid-consecutive-repeating-characters/)
  - [转换成小写字母](

1. https://www.lodashjs.com/docs/lodash.drop)

### 2021-9-8

1. 文章阅读

  - [前端面试](https://juejin.cn/post/6844903577220349959)
  - [带你了解路由的底层原理，用原生js手写一个路由](https://juejin.cn/post/6844904167404748814)

2. 源码阅读

3. leecode 刷题

  - [猜数字的的大小](https://leetcode-cn.com/problems/guess-number-higher-or-lower/)
  - [统计好三元组](

1. https://www.lodashjs.com/docs/lodash.difference)

### 2021-9-7

1. 文章阅读

  - [hash与history的区别](https://juejin.cn/post/6844904151206330375)
  - [5分钟前端国际化](https://juejin.cn/post/6844903505866522638)

2. 源码阅读

  - [聊聊前端国际化文案该如何处理](https://juejin.cn/post/6844903778471280653)

3. leecode 刷题

  - [编写一个函数来计算它们的交集。](https://leetcode-cn.com/problems/intersection-of-two-arrays/)
  - [3的幂](

1. https://www.lodashjs.com/docs/lodash.difference)

### 2021-9-6

1. 文章阅读

  - [九种跨域当时实现](https://juejin.cn/post/6844903767226351623)
  - [跨域](https://juejin.cn/post/6844903496521613320)

2. 源码阅读

  - [原生JS灵魂之问(中)，检验自己是否真的熟悉JavaScript？](https://juejin.cn/post/6844903974378668039)

3. leecode 刷题

  - [各位相加](https://leetcode-cn.com/problems/add-digits/)
  - [丢失的数](

1. https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/)

### 2021-9-4

1. 文章阅读

  - [Hooks很强大，为何还需要Mobx](https://juejin.cn/post/6930758273863778317)
  - [Mobx特别容易入门级教程](https://juejin.cn/post/6844903907441770509)

2. 源码阅读

  - [原生JS灵魂之问](https://juejin.cn/post/6844903974378668039)

3. leecode 刷题

  - [快乐数](https://leetcode-cn.com/problems/happy-number/)
  - [除自身以外的数组的乘积](https://leetcode-cn.com/problems/product-of-array-except-self/)



### 2021-9-3

1. 文章阅读

  - [155个JavaScript基础问题](https://juejin.cn/post/6966781111888265253)
  - [如何让10万条数据的小程序列表如丝般顺滑](https://juejin.cn/post/6966904317148299271)

2. 源码阅读

  - [1 小时顶 7 天！程序员工作中的巧思](https://juejin.cn/post/6965879044634050568)
  - [裁剪数组](https://www.lodashjs.com/docs/lodash.slice)

3. leecode 刷题

  - [相同的树](https://leetcode-cn.com/problems/same-tree/)
  - [用队列实现栈](

### 2021-9-2

1. 文章阅读

- [ 小程序]([https://jasonandjay.github.io/study/zh/miniapp/#%E5%B0%8F%E7%A8%8B%E5%BA%8F](https://jasonandjay.github.io/study/zh/miniapp/#小程序)
- [戏说前端 JavaScript 之『防抖节流』基础知识](https://juejin.cn/post/6974373490526978079)

2. 源码阅读

- [React 架构的演变 - Hooks 的实现](https://blog.shenfq.com/posts/2020/React%20%E6%9E%B6%E6%9E%84%E7%9A%84%E6%BC%94%E5%8F%98%20-%20Hooks%20%E7%9A%84%E5%AE%9E%E7%8E%B0.html)

3. leetcode 刷题

- [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)

### 2021-9-1

1. 文章阅读

- [我在工作中是如何使用 git 的](https://juejin.cn/post/6974184935804534815)
- [解决浏览器存储问题，不得不了解的 cookie,localStorage 和 sessionStorage](https://juejin.cn/post/6973404323904503844)

2. 源码阅读

- [React 架构的演变 - 更新机制](https://blog.shenfq.com/posts/2020/React%20%E6%9E%B6%E6%9E%84%E7%9A%84%E6%BC%94%E5%8F%98%20-%20%E6%9B%B4%E6%96%B0%E6%9C%BA%E5%88%B6.html)

3. leetcode 刷题

- [整数翻转](https://leetcode-cn.com/problems/reverse-integer/)

### 2021-8-31

1. 文章阅读

- [浏览器的兼容问题及解决方案整理](https://juejin.cn/post/6972937716660961317)
- [构建 Typescript 知识体系(五)](https://juejin.cn/post/6972342025832103950)

2. 源码阅读

- [React 架构的演变 - 从递归到循环](https://blog.shenfq.com/posts/2020/React%20%E6%9E%B6%E6%9E%84%E7%9A%84%E6%BC%94%E5%8F%98%20-%20%E4%BB%8E%E9%80%92%E5%BD%92%E5%88%B0%E5%BE%AA%E7%8E%AF.html)

3. leetcode 刷题

- [找不同](https://leetcode-cn.com/problems/find-the-difference/)

### 2021-8-30

1. 文章阅读

- [Axios 如何实现请求重试？](https://juejin.cn/post/6973812686584807432)小程序)
- [js 数组 find,some,filter,reduce 区别](https://juejin.cn/post/6973548322829107214)

2. 源码阅读

- [React 架构的演变 - 从同步到异步](https://blog.shenfq.com/posts/2020/React%20%E6%9E%B6%E6%9E%84%E7%9A%84%E6%BC%94%E5%8F%98%20-%20%E4%BB%8E%E5%90%8C%E6%AD%A5%E5%88%B0%E5%BC%82%E6%AD%A5.html)](https://react.jokcy.me/book/flow/begin-work/update-host-root.html)

3. leetcode 刷题

- [只出现一次的数字 III](https://leetcode-cn.com/problems/single-number-iii/)

### 2021-8-29

1. 文章阅读

- [ 小程序]([https://jasonandjay.github.io/study/zh/miniapp/#%E5%B0%8F%E7%A8%8B%E5%BA%8F](https://jasonandjay.github.io/study/zh/miniapp/#小程序)
- [小程序组件化](https://jasonandjay.github.io/study/zh/miniapp/小程序组件化.html)）

2. 源码阅读

- [dva 源码分析](https://juejin.cn/post/6844904078435155982)

3. leetcode 刷题

- [移动零](https://leetcode-cn.com/problems/move-zeroes/submissions/)

### 2021-8-28

1. 文章阅读

- [Canvas-字体](https://juejin.cn/post/6972060080720052254)
- [Node.js 入门系列（七）Express 框架](https://juejin.cn/post/6971824988932440071)

2. 源码阅读

- [一文带你看懂 UmiJS](https://juejin.cn/post/6844904197331091464)

3. leetcode 刷题

- [3 的幂](https://leetcode-cn.com/problems/power-of-three/submissions/)

### 2021-8-27

1. 文章阅读

- [一些开发中的 CSS 小技巧](https://juejin.cn/post/6971604819635093512)
- [那些高级/资深的前端是如何回答 JavaScript 面试题的 （一）](https://juejin.cn/post/6971727286856843295)

2. 源码阅读

- [ElementUI 2.X 源码学习:源码剖析之工程化(五)](https://juejin.cn/post/6971434343516340238)

3. leetcode 刷题

- [4 的幂](https://leetcode-cn.com/problems/power-of-four/submissions/)

### 2021-8-26

1. 文章阅读

- [React 18 发布计划](https://juejin.cn/post/6971643043459907597)
- [js 原型链](https://juejin.cn/post/6971410630347587614)

2. 源码阅读

- [登录校验-session、token](https://juejin.cn/post/6969457980198780942)

3. leetcode 刷题

- [N 叉树的前序遍历](https://leetcode-cn.com/problems/n-ary-tree-preorder-traversal/)

### 2021-8-24

1. 文章阅读

- [函数的方法 call()、apply()、bind()的区别？](https://juejin.cn/post/6971029774995030029)
- [js - 正则表达式](https://juejin.cn/post/6971036721442095111)

2. 源码阅读

- [Vue 源码阅读 - 文件结构与运行机制](https://juejin.cn/post/6844903630458486798#heading-4)

3. leetcode 刷题

- [子集](https://leetcode-cn.com/problems/subsets/)

### 2021-8-23

1. 文章阅读

- [ 小程序]([https://jasonandjay.github.io/study/zh/miniapp/#%E5%B0%8F%E7%A8%8B%E5%BA%8F](https://jasonandjay.github.io/study/zh/miniapp/#小程序)
- [小程序组件化](https://jasonandjay.github.io/study/zh/miniapp/小程序组件化.html)）

2. 源码阅读

- [-updateHostRoot](https://react.jokcy.me/book/flow/begin-work/update-host-root.html)

3. leetcode 刷题

- [[电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/)]

### 2021-8-22

1. 文章阅读

- [ node-qrcode](https://www.npmjs.com/package/qrcode)
- [微信小程序](https://mp.weixin.qq.com/cgi-bin/wx?token=&lang=zh_CN)

2. 源码阅读

- [-renderRoot](https://react.jokcy.me/book/flow/render-root.html)

3. leetcode 刷题

- [[盛最多水的容器](https://leetcode-cn.com/problems/container-with-most-water/)

### 2021-8-20

1. 文章阅读

- [ 响应式布局 与 Bootstrap 框架](https://www.bilibili.com/video/BV1Nt4y1q7pn?p=14&spm_id_from=pageDriver)
- [如何处理兼容问题](<[https://jasonandjay.github.io/study/zh/standard/Compatibility.html#%E5%A6%82%E4%BD%95%E5%A4%84%E7%90%86%E5%85%BC%E5%AE%B9%E9%97%AE%E9%A2%98](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#如何处理兼容问题)>)

2. 源码阅读

- [-performUnitOfWork](https://react.jokcy.me/book/flow/perform-unit-of-work.html)

3. leetcode 刷题

- [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)

### 2021-8-19

1. 文章阅读

- [JavaScript 字符串方法](https://www.w3school.com.cn/js/js_string_methods.asp)
- [JavaScript 数组迭代方法](https://www.w3school.com.cn/js/js_array_iteration.asp)

2. 源码阅读

- [-scheduleWork](https://react.jokcy.me/book/flow/schedule-work.html)

3. leetcode 刷题

- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)

### 2021-8-18

1. 文章阅读

- [--[React 路由传参的三种方式](https://www.runoob.com/w3cnote/js-refresh-current-page.html)
- [--webpack4 的简易配置](https://jasonandjay.github.io/study/zh/webpack/)

2. 源码阅读

- [-在 FiberScheduler 中的全局变量](https://react.jokcy.me/book/flow/scheduler-global.html)

3. leetcode 刷题

- [字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi/)

### 2021-8-17

1. 文章阅读

- [--js 刷新当前页面](https://www.runoob.com/w3cnote/js-refresh-current-page.html)
- [--在 React 中使用 element-ui](https://blog.csdn.net/August_leo/article/details/85538096)

2. 源码阅读

- [--expirationTime 公式](https://react.jokcy.me/book/update/expiration-time.html)

3. leetcode 刷题

- [无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)

### 2021-8-16

1. 文章阅读

- [--css 设置背景图片模糊，内容不模糊](https://blog.csdn.net/ohehehou/article/details/51975539)
- [--CSS background-image 属性](https://www.w3school.com.cn/cssref/pr_background-image.asp)

2. 源码阅读

- [--ReactDOM.render](https://react.jokcy.me/book/update/react-dom-render.html)

3. leetcode 刷题

- [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/comments/)

### 2021-8-15

1. 文章阅读

- [--渐进式 web 应用](https://jasonandjay.github.io/study/zh/standard/Pwa.html#%E8%83%8C%E6%99%AF)
- [--网络安全](https://jasonandjay.github.io/study/zh/standard/Security.html#%E9%87%8D%E8%A6%81%E6%80%A7)

2. 源码阅读

- [--React 中的数据结构](https://react.jokcy.me/book/api/react-structure.html)

3. leetcode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

### 2021-8-13

1. 文章阅读

- [--浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#概念)
- [--单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html)

2. 源码阅读

- [--React.Children](https://react.jokcy.me/book/api/react-children.html)

3. leetcode 刷题

- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

### 2021-8-12

1. 文章阅读

- [--前后端是如何交互的](https://jasonandjay.github.io/study/zh/standard/Cooperation.html)
- [--移动端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html)

2. 源码阅读

- [ReactElement](https://blog.csdn.net/qq_39136928/article/details/81914253)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

# 范立業

### 2020-9-3

1. 文章阅读

- [mobx 是什么](https://www.bbsmax.com/A/LPdoegg8J3/)
- [mobx 有什么优点](https://www.bbsmax.com/A/LPdoegg8J3/)

2. 源码阅读

### 2021-8-27

- []()
- []()

3. leetcode 刷题

### 2020-9-2

1. 文章阅读

- [WebSocket 是什么](https://baike.baidu.com/item/WebSocket/1953845?fr=aladdin)
- [WebSocket 有什么优点](https://baike.baidu.com/item/WebSocket/1953845?fr=aladdin)

2. 源码阅读

- []()

3. leetcode 刷题

### 2021-9-1

1. 文章阅读

- [我在工作中是如何使用 git 的](https://juejin.cn/post/6974184935804534815)

2. 源码阅读

- 无

3. leetcode 刷题

- 无

### 2021-8-31

1. 文章阅读

- [浏览器的兼容问题及解决方案整理](https://juejin.cn/post/6972937716660961317)

2. 源码阅读

- 无

3. leetcode 刷题

- 无

### 2021-8-30

1. 文章阅读

- [Axios 如何实现请求重试？](https://juejin.cn/post/6973812686584807432)小程序)

2. 源码阅读

- 无

3. leetcode 刷题

- 无

### 2021-8-29

1. 文章阅读

- [小程序组件化](https://jasonandjay.github.io/study/zh/miniapp/小程序组件化.html)）

2. 源码阅读

- 无

3. leetcode 刷题

- 无

### 2021-8-28

1. 文章阅读

- [Node.js 入门系列（七）Express 框架](https://juejin.cn/post/6971824988932440071)

2. 源码阅读

- [一文带你看懂 UmiJS](https://juejin.cn/post/6844904197331091464)

3. leetcode 刷题

- [3 的幂](https://leetcode-cn.com/problems/power-of-three/submissions/)

### 2021-8-27

1. 文章阅读

- [JavaScript 字符串方法](https://www.w3school.com.cn/js/js_string_methods.asp)

2. 源码阅读

- 无

### 2021-8-26

1. 文章阅读

- [vue 组件之间的通讯](https://www.cnblogs.com/barryzhang/p/10566515.html)

2. 阅读源码

- [forEach](https://www.runoob.com/jsref/jsref-forEach.html)

3. leetcode 刷题

- [重新排列字符串](https://leetcode-cn.com/problems/shuffle-string/Https://leetcode-cn.com/problems/shuffle-string/)

### 2021-8-25

1. 文章阅读

- [for...in 和 for...of](https://www.cnblogs.com/rogerwu/p/10738776.html)

2. 阅读源码

- [find](https://www.runoob.com/jsref/jsref-filter.html)
- [findIndex](https://www.runoob.com/jsref/jsref-findindex.html)

3. leetcode 刷题

### 2021-8-24

1. 文章阅读

- [vue 的路由传参方式](https://www.jianshu.com/p/d276dcde6656)

2. 阅读源码

- [filter](https://www.runoob.com/jsref/jsref-filter.html)

3. leetcode 刷题 -[多数元素](https://leetcode-cn.com/leetbook/read/tencent/x5yxxs/)

### 2021-8-23

1. 文章阅读

- [常用 hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B8%B8%E7%94%A8hooks)

2. 阅读源码

- [fill](https://www.runoob.com/jsref/jsref-fill.html)

3. leetcode 刷题

- 无

### 2021-8-22

1. 文章阅读

- [vue 的生命周期](https://zhuanlan.zhihu.com/p/94601709)

2. 阅读源码

- [every](https://www.runoob.com/jsref/jsref-every.html)

### 2021-8-20

1. 文章阅读

- [--浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#概念)
- [--单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-19

1. 文章阅读

- [--css 设置背景图片模糊，内容不模糊](https://blog.csdn.net/ohehehou/article/details/51975539)
- [--CSS background-image 属性](https://www.w3school.com.cn/cssref/pr_background-image.asp)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-18

1. 文章阅读

- [--渐进式 web 应用](https://jasonandjay.github.io/study/zh/standard/Pwa.html#%E8%83%8C%E6%99%AF)
- [--网络安全](https://jasonandjay.github.io/study/zh/standard/Security.html#%E9%87%8D%E8%A6%81%E6%80%A7)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-20

1. 文章阅读

- [--浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#概念)
- [--单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-19

1. 文章阅读

- [--css 设置背景图片模糊，内容不模糊](https://blog.csdn.net/ohehehou/article/details/51975539)
- [--CSS background-image 属性](https://www.w3school.com.cn/cssref/pr_background-image.asp)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-18

1. 文章阅读

- [--渐进式 web 应用](https://jasonandjay.github.io/study/zh/standard/Pwa.html#%E8%83%8C%E6%99%AF)
- [--网络安全](https://jasonandjay.github.io/study/zh/standard/Security.html#%E9%87%8D%E8%A6%81%E6%80%A7)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-17

1. 文章阅读

- [--JS 简单得一些功能](https://juejin.cn/post/6921509748785283086)
- [--移动端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html)

2. 源码阅读

- [--slice](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/slice.md)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

### 2021-8-16

1. 文章阅读

- [--ES5、Es6 数组方法](https://juejin.cn/post/6945996368880091173)

2. 阅读源码

- 无

3. leetcode 刷题

- 无

### 2021-8-15

1. 文章阅读

- [--JS 简单得一些功能](https://juejin.cn/post/6921509748785283086)
- [--移动端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html)

2. 源码阅读

- [--slice](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/slice.md)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

### 2021-8-13

1. 文章阅读

- [--前后端是如何交互的](https://jasonandjay.github.io/study/zh/standard/Cooperation.html)
- [--移动端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html)

2. 源码阅读

- [ReactElement](https://blog.csdn.net/qq_39136928/article/details/81914253)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

### 2021-8-12

1. LeeCode 刷题

- [--猜数字](https://leetcode-cn.com/problems/guess-numbers/)

2. 文章阅读

- [React 基础知识点总结](https://www.jianshu.com/p/9f2f25e344e3)
- [--React 面试题](https://www.jianshu.com/p/53928dca095b)

3. 源码阅读

- [--indexOf](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/indexOf.md)

# 丁帅

### 2020-9-9

1. 文章阅读

- [umi request post 请求参数](https://blog.csdn.net/youngsailor/article/details/88952395?ops_request_misc=&request_id=&biz_id=102&utm_term=umi%E4%BC%A0%E5%8F%82&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-4-88952395.pc_search_result_control_group&spm=1018.2226.3001.4187)

2. 源码阅读

- [HTML5 游戏开发快速提升](https://juejin.cn/book/6901095904892321800)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

### 2020-9-8

1. 文章阅读

- [vue2.0 中的$router 和 $route 的区别](https://www.cnblogs.com/czy960731/p/9288830.html)
- [Vue 中的 watch 与 computed](https://www.cnblogs.com/gunelark/p/8492468.html)

2. 源码阅读

- [\_.sortedIndex(array, value)](https://www.lodashjs.com/docs/lodash.sortedIndex)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1012/)

### 2021-9-7

1. 文章阅读

- [什么是闭包](https://blog.csdn.net/u012194956/article/details/82595270)

2. 阅读源码

- [includes](https://blog.csdn.net/weixin_38912024/article/details/100697963)
- [indexOf](https://www.w3school.com.cn/jsref/jsref_indexOf.asp)

### 2021-9-5

1. 文章阅读

- [vue 组件之间的通讯](https://www.cnblogs.com/barryzhang/p/10566515.html)

2. 阅读源码

- [forEach](https://www.runoob.com/jsref/jsref-forEach.html)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/leetbook/read/tencent/xxzqki/)

### 2021-8-30

1. 文章阅读

- [ 小程序]([https://jasonandjay.github.io/study/zh/miniapp/#%E5%B0%8F%E7%A8%8B%E5%BA%8F](https://jasonandjay.github.io/study/zh/miniapp/#小程序)
- [小程序组件化](https://jasonandjay.github.io/study/zh/miniapp/小程序组件化.html)）

2. 源码阅读

- [-updateHostRoot](https://react.jokcy.me/book/flow/begin-work/update-host-root.html)

3. leetcode 刷题

- [[电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/)]

### 2021-8-27

1.  文章阅读

- [Vue 配置 axios 跨域代理\_\_BaiMoci](https://juejin.cn/post/7000140081293950984)

2.  阅读源代码

- [CSS-还在用 JS 计数吗？来试试纯 CSS 计数器！！！](https://blog.csdn.net/m0_56344602/article/details/119940323)

3. leetcode 刷题

- [ 四数之和](https://leetcode-cn.com/problems/4sum/)

### 2021-8-26

1.  文章阅读

- [基于 websocket 实现《全双工协议》的 TS 同步调用之 Request/Response/Notify 原理与实战~](https://juejin.cn/post/7000140081293950984)

2.  阅读源代码

- [JavaScript 进阶第二章（循环结构 ）](https://blog.csdn.net/m0_56344602/article/details/119940323)

3. leetcode 刷题

- [ 整数反转](https://leetcode-cn.com/problems/reverse-integer/)

### 2021-8-25

1. 文章阅读

- [JavaScript 介绍 this 指向的各种情况](https://blog.csdn.net/chai_tian/article/details/119731046?utm_medium=distribute.pc_category.none-task-blog-hot-6.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-6.nonecase)

2. 阅读源代码

- [计算机网络面试突击](https://leetcode-cn.com/leetbook/detail/networks-interview-highlights/)

3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/)

### 2021-8-24

1. 文章阅读

- [css 和 css3 零散知识](https://blog.csdn.net/qq_45882139/article/details/119867824?utm_medium=distribute.pc_category.none-task-blog-hot-3.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-3.nonecase)

2. 阅读源代码

- [我用 index 作为 key 也没啥问题啊](https://juejin.cn/post/6999932053466644517)

3.  leetcode 刷题

- [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)

### 2021-8-23

1. 文章阅读

- [JavaScript 本地存储](https://blog.csdn.net/weixin_50697957/article/details/119869177?utm_medium=distribute.pc_category.none-task-blog-hot-20.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-20.nonecase)

2. 阅读源代码

- [聊一聊移动端适配](https://juejin.cn/post/6999438892441026591)

3.  leetcode 刷题

- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

### 2021-8-22

1. 文章阅读

- [弄懂 CSS 必考题](https://juejin.cn/post/6999160403066355743)

2. 阅读源代码

- [使用 HTML、CSS 和 JavaScript 的简单模拟时钟](https://blog.csdn.net/qq_44273429/article/details/119724717?utm_medium=distribute.pc_category.none-task-blog-hot-7.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-7.nonecase)

3.  leetcode 刷题

- [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/comments/)

### 2021-8-20

1. 文章阅读

- [前端面试知识点](https://blog.csdn.net/qq_39221436/article/details/119738302?utm_medium=distribute.pc_category.none-task-blog-hot-8.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-8.nonecase)

2. 阅读源代码

- [React 进阶实践指南](https://juejin.cn/book/6945998773818490884/section/6948353204413268001)

3.  leetcode 刷题

- [反转字符串 II](https://leetcode-cn.com/problems/reverse-string-ii/)

### 2021-8-19

1. 文章阅读

- [Javascript 学习笔记](https://blog.csdn.net/weixin_46264622/article/details/119759769?utm_medium=distribute.pc_category.none-task-blog-hot-4.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-4.nonecase)

2. 阅读源代码

- [Vue 3.0 企业级项目实战](https://juejin.cn/book/6933939264455442444/section/6933939264577241091)

3.  leetcode 刷题

- [ 三数之和](https://leetcode-cn.com/problems/3sum/)

### 2021-8-19

1. 文章阅读

- [JavaScript 介绍 this 指向的各种情况](https://blog.csdn.net/chai_tian/article/details/119731046?utm_medium=distribute.pc_category.none-task-blog-hot-6.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-6.nonecase)

2. 阅读源代码

- [计算机网络面试突击](https://leetcode-cn.com/leetbook/detail/networks-interview-highlights/)

3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/)

### 2021-8-18

1. 文章阅读

- [React 状态管理一些思考--Redux](https://juejin.cn/post/6997376981599780878)

2. 阅读源代码

- [❤️ 使用 HTML、CSS 和 JavaScript 的简单模拟时钟 ❤️](https://blog.csdn.net/qq_44273429/article/details/119724717?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)

3.  leetcode 刷题

- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

### 2021-8-17

1. 文章阅读

- [JavaScript 基础系列笔记——目录](https://blog.csdn.net/weixin_50697957/article/details/119712247?utm_medium=distribute.pc_category.none-task-blog-hot-8.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-8.nonecase)

2. 阅读源代码

- [Vue 学习笔记整理](https://juejin.cn/post/6971674228101742629)

3. leetcode 刷题

- [ 最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/)

### 2021-8-16

1. 文章阅读

- [前端面试知识点](https://juejin.cn/post/6996815121855021087)

2. 阅读源代码

- [React 进阶实践指南](https://juejin.cn/book/6945998773818490884)

3. leetcode 刷题

- [JavaScript 实现监听路由变化](https://blog.csdn.net/qq_42367749/article/details/119653295?utm_medium=distribute.pc_category.none-task-blog-hot-8.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-8.nonecase)

### 2021-8-15

1. 文章阅读

- [什么是 Node.js](https://blog.csdn.net/m0_56649557/article/details/119581769?utm_medium=distribute.pc_category.none-task-blog-hot-9.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-9.nonecase)

2. 阅读源代码

- [Vue 3.0 企业级项目实战](https://juejin.cn/book/6933939264455442444/section/6933939264577241091)

3. leetcode 刷题

- [javascript 学习系列(5):数组中的 reduce 方法](https://blog.csdn.net/weixin_43392489/article/details/119654669?utm_medium=distribute.pc_category.none-task-blog-hot-4.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-4.nonecase)

### 2021-8-13

1. 文章阅读

- [cookies,sessionStorage,localStorage 得区别](https://blog.csdn.net/mapbar_front/article/details/99776372)
- [谈一谈 js 原型链机制?](https://blog.csdn.net/luo1831251387/article/details/119491381?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)

2. 阅读源码

- [\_.chunk](https://www.lodashjs.com/docs/lodash.chunk)
- [\_.concat](https://www.lodashjs.com/docs/lodash.concat)

3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/solution/)

### 2021-8-12

1. 文章阅读

- [判断一个字符串'aabcdobdackoppz'中出现次数最多的字符，并统计其次数](https://blog.csdn.net/weixin_45863614/article/details/119540513?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)
- [DW 基础教程（一）](https://blog.csdn.net/weixin_57762542/article/details/119519918?utm_medium=distribute.pc_category.none-task-blog-hot-9.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-9.nonecase);

2. 阅读源码

- [\_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/solution/)
