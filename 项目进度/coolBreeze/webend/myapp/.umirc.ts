import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {
    compact:true
  },
  // 配置dav
  dva: {
    immer: true,
    hmr: true,
  },
   // 引入国际化
   locale: {},
   // 响应式
 
  //  extraPostCSSPlugins: [px2rem({remUnit: 75})],
   scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],

  //配置服务器子路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812B/zhangweishan/xiaolou/' : '/',
  //配置服务器上的路由前缀
  base:"/1812B/zhangweishan/xiaolou/",
  //配置服务器上按需加载
  dynamicImport: {
    loading: '@/components/Loading',
  },
  //文件名加hash后缀
  hash:true
});


