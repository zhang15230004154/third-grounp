//留言板
import { msglist ,msgtuijian ,msgboardId} from '@/services';
import { getmsglist ,msgtuijiantype,msgboardIdtypye} from '@/types';
import { RootObjects } from '@/types/about';
import { Effect, ImmerReducer, Reducer } from 'umi';

export interface msgAboutModelState {
    msgboard: getmsglist [],
    comontcount:number,
    msgtuijians:msgtuijiantype [],
    getmsgboard:msgboardIdtypye []
}

export interface msgAboutModelType {
    namespace: 'msgboard';
    state: msgAboutModelState;
    effects: {
        msglist: Effect;
        msgtuijian: Effect;
        msgboardId:Effect
    };
    reducers: {
        save: Reducer<msgAboutModelState>;
    };
}

const getlistModel: msgAboutModelType = {
    namespace: 'msgboard',

    state: {
        msgboard: [],
        comontcount:0,
        msgtuijians:[],
        getmsgboard:[]
    },

    effects: {
        //获取详情页面
        *msgboardId({ payload }, { call, put }) {
            let result = yield call(msgboardId,payload);
             console.log(payload);
            if (result.success == true) {
                yield put({
                    type: 'save',
                    payload: {
                        getmsgboard: result.data, 
                    }
                })
            } 
        }, 
        *msgtuijian({ payload }, { call, put }) {
            let result = yield call(msgtuijian);
             console.log(result.data);
            if (result.success == true) {
                yield put({
                    type: 'save',
                    payload: {
                        msgtuijians: result.data, 
                    }
                })
            }
        }, 
        *msglist({ payload }, { call, put }) {
            let result = yield call(msglist);
             console.log(result);
            if (result.success == true) {
                yield put({
                    type: 'save',
                    payload: {
                         msgboard: result.data[0] ,
                         comontcount:result.data[1]
                    }
                })
            }
        }, 
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    },
};

export default getlistModel;