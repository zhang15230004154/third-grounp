import { getAboutList, getCommont, getPublic, getRecommend } from '@/services';
import { IArticleItem, RootObject } from '@/types';
import { PublicObject, RootObjects } from '@/types/about';
import { Effect, ImmerReducer, Reducer } from 'umi';

export interface AboutModelState {
  about: RootObject [],
  commont:RootObjects [];
  commontConut:number;
  recommend: IArticleItem[];
  public:Partial <PublicObject>
}

export interface AboutModelType {
  namespace: 'about';
  state: AboutModelState;
  effects: {
    getAboutList: Effect;
    getCommont:Effect;
    getRecommend:Effect;
    getPublic:Effect
  };
  reducers: {
    save: Reducer<AboutModelState>;
  };
}

const AboutModel: AboutModelType = {
  namespace: 'about',

  state: {
    about: [],
    commont:[],
    commontConut:0,
    recommend:[],
    public:{}
  },

  effects: {
    *getAboutList({ payload }, { call, put }) {
        let result=yield call(getAboutList)
        // console.log('*****11',result)
        if(result.statusCode==200){
            yield put({
                type:'save',
                payload:{about:result.data}
            })
        }
    },
    *getCommont({ payload }, { call, put }) {
      let result=yield call(getCommont,payload)
      // console.log('*****11',result)
      if(result.statusCode==200){
          yield put({
              type:'save',
              payload:{
                commont:result.data[0],
                commontConut:result.data[1]
              }
          })
      }
  },

  *getRecommend({ payload }, { call, put }) {
    let result = yield call(getRecommend);
    //  console.log(result);
    if (result.success == true) {
      yield put({
        type: 'save',
        payload: { recommend: result.data }
      })
    }
  },
//发表
  *getPublic({ payload }, { call, put }) {
    // console.log(payload)
    let result = yield call(getPublic);
    //  console.log(result);
    if (result.success == true) {
      yield put({
        type: 'save',
        payload: {public : result.data }
      })
      // message.success('评论成功 已提交审核');
    }
  },

  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default AboutModel;