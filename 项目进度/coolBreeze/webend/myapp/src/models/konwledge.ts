//知识小册
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { KnowledgeList,Classify,getDetail, getArticleDetails } from "../services/index"
import {KnowledgeListType,ClassifyListType,KnowledgeDetailType,KnowledgeDetailChildren} from "../types/konwledge"
export interface knowledgeModelState {
  knowledgeList:KnowledgeListType[],
  knowledgeCount: number,
  clssifyList:ClassifyListType[]
  knowledgeDetailList:KnowledgeDetailType[]
  articleDetailList:[]
  knowledgeChildDetailList:any
}

export interface knowledgeModelType {
  namespace: 'knowledge';
  state: knowledgeModelState;
  effects: {
    getKnowledgeList: Effect;
    getClassify:Effect;
    getDetail:Effect;
    getArticleDetails:Effect;
    getChildDetail:Effect;

    
  };
  reducers: {
    save: Reducer<knowledgeModelState>,

    // 启用 immer 之后
    // save: ImmerReducer<knowledgeModelState>;
  };
  subscriptions: { setup: Subscription };
}

const knowledgeModel: knowledgeModelType = {
  namespace: 'knowledge',

  state: {
    knowledgeList: [],//知识小册列表
    knowledgeCount: 0, //知识小册列表数量
    clssifyList:[],//分类列表
    knowledgeDetailList:[],//知识小册详情列表
    articleDetailList:[],//文章详情列表
    knowledgeChildDetailList:[],
  },

  effects: {
    *getKnowledgeList({type, payload }, { call, put }) { //获取知识小册列表
      let result = yield call(KnowledgeList)
      if (result.success) {
        yield put({
          type: "save",
          payload: {
            knowledgeList: result.data[0],
            knowledgeCount: result.data[1],
          }
        })
      }
    },
    *getClassify({payload},{call,put}){ //获取右侧分类
        let result = yield call(Classify)
        if (result.success) {
          yield put({
            type: "save",
            payload: {
              clssifyList:result.data
            }
          })
        }
    },
    *getDetail({payload},{call,put}){ //获取详情
      console.log('-------',payload)
      let result = yield getDetail(payload)
      if (result.success) {
        yield put({
          type: "save",
          payload: {
            knowledgeDetailList:result.data
          }
        })
      }
      
    },
    *getChildDetail({payload},{call,put}){ //获取详情
      let result = yield getDetail(payload)
      if (result.success) {
        yield put({
          type: "save",
          payload: {
            knowledgeChildDetailList:result.data
          }
        })
      }
      
    },

    *getArticleDetails({payload},{call,put}){ //获取详情
      let result = yield getArticleDetails(payload)
      console.log(result)
      if (result.success) {
        yield put({
          type: "save",
          payload: {
            articleDetailList:result.data
          }
        })
      }
      
    }

  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  },
  subscriptions: { //订阅
     setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/knowledge') {  //如果地址栏变为'/knowledge' 调用getKnowledgeList
          console.log("wtmd---------")
          dispatch({
            type: 'getKnowledgeList',
          });
        }
      });
    },
  },
};

export default knowledgeModel;
