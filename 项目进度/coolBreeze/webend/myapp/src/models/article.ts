import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {EggObject, IArticleComment, IArticleDetail, IArticleItem,LeetObject,LinuxObject,NewObject,ReadObject,RootObject, RootObjects, SwiperObject} from '../types/article'
import {getArticleList,getleetList,getlinuxList, getlabelLists, getRecommend,getwebList,getEggList,getreadList,getnewsList, getArticleDetail, getArticleComment,getSwiper} from '../services/modules/article'
import { IRootState } from '@/types';
export interface ArticleModelState {
  recommend: IArticleItem [];
  articleList:IArticleItem [];
  articleCount:number;
  getlabelList:RootObject [];
  getwebList:RootObjects[];
  webCount:number,
  getEggList:EggObject [];
  eggcount:number;
  getreadList:ReadObject[];
  readcount:number;
  getlinuxList:LinuxObject [];
  linuxcount:0,
  getleetList:LeetObject [];
  leetcount:0,
  getnewsList:NewObject [];
  newscount:0,
  articleCommentCount:0,
  articleComment:IArticleComment[],
  articleDetail: Partial<IArticleDetail>,
  getSwiper:SwiperObject[],
  // getShareList:Partial<ShareObject>
}

export interface ArticleModelType {
  namespace: 'article';
  state:ArticleModelState;
  effects: {
    getRecommend: Effect;
    getArticleList:Effect;
    getlabelLists:Effect;
    getwebList:Effect;
    getEggList:Effect;
    getreadList:Effect;
    getlinuxList:Effect;
    getleetList:Effect;
    getnewsList:Effect;
    getArticleDetail:Effect;
    getArticleComment:Effect;
    getSwiper:Effect;
    // getShareList:Effect;
  };
  reducers: {
    save: Reducer<ArticleModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<ArticleModelState>;
  };
}

const ArticleModel: ArticleModelType = {
  namespace: 'article',

  state: {
    recommend:[],
    articleList:[],
    articleCount:0,
    getlabelList:[],
    getwebList:[],
    webCount:0,
    getEggList:[],
    eggcount:0,
    getreadList:[],
    readcount:0,
    getlinuxList:[],
    linuxcount:0,
    getleetList:[],
    leetcount:0,
    getnewsList:[],
    newscount:0,
    articleComment:[],
    articleCommentCount:0,
    articleDetail:{},
    getSwiper:[],
    // getShareList:{}
  },

  effects: {
    *getRecommend({payload},{call,put}){
         let result=yield call(getRecommend);
        //  console.log(result);
         if(result.success==true){
           yield put({
             type:'save',
             payload:{recommend:result.data}
           })
         }
    },
    *getArticleList({payload},{call,put}){
      let result=yield call(getArticleList,payload);
      // console.log(result);
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
             articleList:result.data[0],
             articleCount:result.data[1]
            }
         })
       }
    },
    *getlabelLists({payload},{call,put}){
      let result=yield call(getlabelLists,payload);
      // console.log(result,'aaaaaaaaaaaaaaaaaaa');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getlabelList:result.data
            }
         })
       }
    },
    *getwebList({payload},{call,put}){
      let result=yield call(getwebList,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getwebList:result.data[0],
            webCount:result.data[1]
            }
         })
       }
    },
    *getEggList({payload},{call,put}){
      let result=yield call(getEggList,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getEggList:result.data[0],
            eggcount:result.data[1]
            }
         })
       }
    },
    *getreadList({payload},{call,put}){
      let result=yield call(getreadList,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getreadList:result.data[0],
            readcount:result.data[1]
            }
         })
       }
    },
    *getlinuxList({payload},{call,put}){
      let result=yield call(getlinuxList,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getlinuxList:result.data[0],
            linuxcount:result.data[1]
            }
         })
       }
    },
    *getleetList({payload},{call,put}){
      let result=yield call(getleetList,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getleetList:result.data[0],
            leetcount:result.data[1]
            }
         })
       }
    },
    *getnewsList({payload},{call,put}){
      let result=yield call(getnewsList,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            getnewsList:result.data[0],
            newscount:result.data[1]
            }
         })
       }
    },
    *getArticleDetail({payload},{call,put}){
      let result=yield call(getArticleDetail,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
            articleDetail:result.data
            }
         })
       }
    },
    *getArticleComment({payload},{call,put,select}){
      let preArticleComment=yield select((state:IRootState)=>state.article.articleComment);
      let result=yield call(getArticleComment,payload.id,payload.page);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         let articleComment=result.data[0];
         if(payload.page!==1){
           articleComment=[...preArticleComment,...articleComment]
         }
         yield put({
           type:'save',
           payload:{
            articleComment:result.data[0],
            articleCommentCount:result.data[1]
            }
         })
       }
    },
    *getSwiper({payload},{call,put}){
      let result=yield call(getSwiper,payload);
      // console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
       if(result.success==true){
         yield put({
           type:'save',
           payload:{
             getSwiper:result.data
            }
         })
       }
    },
    // *getShareList({payload},{call,put}){
    //   let result=yield call(getShareList,payload);
    //   console.log(result.data,'111111aaaaaaaaaaaaaaaaaaaaaaaaaaaa111111111111111111111');
    //    if(result.success==true){
    //      yield put({
    //        type:'save',
    //        payload:{
    //         getShareList:result.data
    //         }
    //      })
    //    }
    // },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};

export default ArticleModel;

