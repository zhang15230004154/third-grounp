import { getDocmentList, getChivesDetail, getChivesYd, getcatgorys, getrecommends, getarchivelike, getarchivecommend } from '../services/modules/archives';
import { RootsObject } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { ArchiveTDetail, ArchiveYd, Arecommend, cATEGORYS, Archivelikes, Archivecommend } from "../types/archives"
export interface DocmentModelState {
    docment: RootsObject[];
    // 详情
    ArchivesList: Partial<ArchiveTDetail>,
    // 归档
    ArchivesYd: ArchiveYd[],
    // 文章分类
    Catgorys: cATEGORYS[],
    // 推荐阅读
    Recomends: Arecommend[],
    // 点赞
    Archivelike: Partial<Archivelikes>,
    // 评论
    Archivecommendlists: Archivecommend[]
}

export interface DocmentModelType {
    namespace: 'docment';
    state: DocmentModelState;
    effects: {
        getDocmentList: Effect;
        getChivesDetail: Effect;
        getChivesYd: Effect;
        getcatgorys: Effect;
        getrecommends: Effect;
        getarchivelike: Effect;
        getarchivecommend: Effect;
    };
    reducers: {
        save: Reducer<DocmentModelState>;
    };
}

const DocmentModel: DocmentModelType = {
    namespace: 'docment',

    state: {
        docment: [],
        ArchivesList: {},
        ArchivesYd: [],
        Catgorys: [],
        Recomends: [],
        Archivelike: {},
        Archivecommendlists: []
    },
    effects: {
        *getDocmentList({ payload }, { call, put }) {
            let result = yield call(getDocmentList)
            // console.log('aaaaa', result)
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: { docment: result.data }
                })
            }
        },
        // 归档详情
        *getChivesDetail({ payload }, { call, put }) {
            let result = yield call(getChivesDetail, payload)
            // console.log(result)
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: { ArchivesList: result.data }
                })
            }
        },
        // 归档阅读
        *getChivesYd({ payload }, { call, put }) {
            let result = yield call(getChivesYd, payload);
            // console.log(result, "1111")
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        ArchivesYd: result.data
                    }
                })
            }
        },
        // 文章分类
        *getcatgorys({ payload }, { call, put }) {
            let result = yield call(getcatgorys, payload);
            // console.log(result, "1111")
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        Catgorys: result.data
                    }
                })
            }
        },
        // 每个详情里的阅读
        *getrecommends({ payload }, { call, put }) {
            let result = yield call(getrecommends, payload);
            // console.log(result, "1111")
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        Recomends: result.data
                    }
                })
            }
        },
        // 点赞
        *getarchivelike({ payload }, { call, put }) {
            let result = yield call(getarchivelike, payload);
            console.log(result, "1111")
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        Archivelike: result.data
                    }
                })
            }
        },
        // 评论
        *getarchivecommend({ payload }, { call, put }) {
            let result = yield call(getarchivecommend, payload.id, payload.page);
            // console.log(result.data, "1111")
            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        Archivecommendlists: result.data[0]
                    }
                })
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    },
};

export default DocmentModel;