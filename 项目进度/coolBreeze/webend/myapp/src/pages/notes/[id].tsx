import classNames from "classnames";
import React, { useEffect } from "react";
import styles from "./detail.less"
import { IRouteComponentProps, useDispatch, useSelector } from "umi"
import { IRootState } from "@/types/models"
import { fomatTime } from "@/utils/index";
import HightLight from "@/components/HightLight/index";
import { Comment, Avatar, Form, Button, List, Input } from 'antd';
import moment from 'moment';
import {KnowledgeDetailChildren} from "@/types/konwledge"
import { Anchor } from 'antd';
const { Link } = Anchor;



const Id: React.FC<IRouteComponentProps<{ id: string, type: string }>> = (props) => {

    const dispatch = useDispatch()
    useEffect(() => {
      let arr = props.match.params.id.split(',')
      const parentId = arr[0];
      const childId = arr[1];
      console.log(parentId,childId)
      dispatch({
          type:"knowledge/getChildDetail",
          payload:childId
      })
      dispatch({
        type:"knowledge/getDetail",
        payload:parentId
      })
    }, [])
    const shareKnowList = useSelector((state: IRootState) => state.knowledge)
    let { knowledgeDetailList,knowledgeChildDetailList}=shareKnowList
    console.log(knowledgeDetailList,knowledgeChildDetailList)
    
    let floorList  = knowledgeChildDetailList.toc?JSON.parse(knowledgeChildDetailList.toc):'' 
    console.log(floorList)
    const getNotesDetailButtom=(id:string,parenId:string)=>{
        let childId = id
        let parentId =parenId
        props.history.push(`/notes/${parentId},${childId}`)
    }
    return (

        <div className={classNames(styles.aticleDetail_box)}>
        <div className={classNames(styles.aticleDetail_leftbox)}>
            <div className={classNames(styles.aticleDetail_left)}>
                <div><img src={knowledgeChildDetailList.cover} /></div>
                <div className={classNames(styles.aticleDetail_title)}>
                    <h1>{knowledgeChildDetailList.title}</h1>
                    <i>发布于{fomatTime(knowledgeChildDetailList.publishAt)}•阅读量{knowledgeChildDetailList.views}</i>
                </div>
                <HightLight>
                    <div dangerouslySetInnerHTML={{ __html:knowledgeChildDetailList.html!}}></div>
                </HightLight>
                <div className={classNames(styles.aticleDetail_buttom)}>
                    <span>发布于{fomatTime(knowledgeChildDetailList.publishAt)}</span>
                </div>
                <div>
                    
                </div>

            </div>


        </div>

        <div className={classNames(styles.aticleDetail_rightbox)}>
            <div className={classNames(styles.aticleDetail_righttop)}>
            <h3>{knowledgeDetailList.title}</h3>
                {
                    knowledgeDetailList.children&&knowledgeDetailList.children.map((item:KnowledgeDetailChildren,index:number)=>{  
                        return(
                            <p key={index} onClick={()=>getNotesDetailButtom(item.id,item.parentId)}>{item.title}</p>
                        )
                    })
                }

            </div>
            <div className={classNames(styles.aticleDetail_rightbuttom)}>
                <h3>目录</h3>
                <Anchor affix={true} showInkInFixed={true}>
                {
               
                 floorList&&floorList.map((item: { text: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; },index:number)=>{
                    return(
                        <Link href={`#${item.text}`} title={item.text} />
                    )
                 })
               }
               </Anchor>

            </div>

        </div>





    </div>
    )
}
export default Id