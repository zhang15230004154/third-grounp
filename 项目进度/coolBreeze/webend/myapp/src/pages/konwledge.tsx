import classNames from "classnames";
import React, { useEffect, useState } from "react"
import { useSelector, useDispatch,IRouteComponentProps } from "umi"
import {ShareAltOutlined,EyeOutlined } from '@ant-design/icons';
import moment from 'moment'
import styles from './knowledge.less';
import { fomatTime } from "@/utils/index";
import { IRootState} from "@/types/models"
import {KnowledgeListType} from "@/types/konwledge"
import {IArticleItem} from "@/types/article"
import Konwledge from "@/components/Konwledge";

const knowledge: React.FC<IRouteComponentProps> = (props) => {
    const dispatch = useDispatch()
    useEffect(() => {  //页面挂载时调用知识小册仓库的getKnowledgeList方法 请求列表
        dispatch({
            type: "knowledge/getKnowledgeList",

        })
        dispatch({
            type:"article/getRecommend"
        })
        dispatch({
            type:"knowledge/getClassify"
        })
    }, [])

    let shareKnowList = useSelector((state:IRootState) => state.knowledge) //共享knowledgeList状态
    let shareArticleList = useSelector((state:IRootState) => state.article) //共享article状态
    let { knowledgeList, knowledgeCount,clssifyList } = shareKnowList //解构状态
    let {recommend} = shareArticleList 
    
    const getKonwledgeDetail=(id:string)=>{ //跳转知识小册详情
      props.history.push(`/konwledgeDetail/${id}`)
    }
    const getArticleDetail=(item:IArticleItem)=>{//跳转文章详情
        props.history.push(`/aticleDetail/${item.id}`)
       dispatch({
           type:"knowledge/getArticleDetail",
           payload:item.id
       })
    }
    moment.updateLocale('en', {
        relativeTime : {
            future: ". %s",
            past:   "%s",
            s  : '几秒前',
            ss : '%d秒前',
            m:  "几分钟前",
            mm: "%d分钟前",
            h:  "几小时前",
            hh: "%d小时前",
            d:  "几天前",
            dd: "%d天前",
            M:  "几月前",
            MM: "%d月前",
            y:  "几年前",
            yy: "%d年前"
        }
    });
    return (
        <div className={classNames(styles.know_box)} >
            <div className={classNames(styles.know_left)}>
                {
                    knowledgeList.map(((item:KnowledgeListType, index: number) => {

                        return (
                            <div className={classNames(styles.know_item)} key={index} onClick={()=>getKonwledgeDetail(item.id)}>
                                <h3>{item.title}</h3><span>{moment(item.publishAt).fromNow()}</span>
                                <dl key={index}>

                                    <dt><img src={item.cover} /></dt>
                                    <dd>
                                        <p>{item.summary}</p>
                                        <span><EyeOutlined/>{item.views}</span>
                                        <span><ShareAltOutlined twoToneColor="#eb2f96"/>分享</span>
                                    </dd>
                                </dl>
                            </div>
                        )
                    }))
                }
            </div>
           <Konwledge></Konwledge>
        </div>
    )
}
export default knowledge
