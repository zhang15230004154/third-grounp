import classNames from 'classnames';
import styles from './archives.less'
import Archiveyd from "../components/Archiveyd"
import Archivelist from "../components/Archivelist"
export default function IndexPage() {
  return <div className={classNames(styles.archives)}>
    <Archivelist />
    {/* 归档右侧组件 */}
    <Archiveyd />
  </div>

}