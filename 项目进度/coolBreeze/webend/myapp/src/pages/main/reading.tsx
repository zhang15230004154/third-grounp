import { useDispatch, useSelector } from "@/.umi/core/umiExports";
import { IRootState } from "@/types";
import classNames from "classnames";
import React, { useEffect, useState } from "react";
import styles from './web.less'
import moment from "moment";
import { NavLink } from "umi";
import Recomment from "@/components/Recomment";
import Routercomponent from "@/components/Routercomponent";


const IndexPage: React.FC = () => {

    let [page, setPage] = useState(1);
    let dispatch = useDispatch();
    const article = useSelector((state: IRootState) => state.article);
    useEffect(() => {
        console.log("aaa");
        dispatch({
            type: 'article/getreadList',
            payload: page,
        })
    }, [])
    console.log(article, 'ewweweweww')
    return <div className={classNames(styles.wed)}>
        <div className={classNames(styles.tops)}>
            <p><span>阅读</span> 分类文章</p>
            <p>共搜索到 <span>{article.readcount}</span> 篇</p>
        </div>
        <div className={classNames(styles.bot)}>
        <Routercomponent></Routercomponent>
            <ul>
                {
                    article.getreadList.map(item => {

                        return <li className={classNames(styles.li)} key={item.id}>
                            <p className={classNames(styles.title)}>{item.title}&emsp;&emsp;<span>{moment(item.updateAt).fromNow()}</span></p>
                            <p>

                            </p>
                            <dl>
                                <dt>{item.cover && <img className={classNames(styles.img)} src={item.cover} alt="" />}</dt>
                                <dd>
                                    <p className="ziduan">{item.summary}</p>

                                    <p className='ziduan'>
                                        <br />
                                        <span>点赞{item.likes}</span>
                                        <span>浏览{item.views}</span>
                                    </p>
                                </dd>
                            </dl>
                        </li>
                    })
                }
            </ul>
        </div>
        <Recomment></Recomment>
    </div>
}

export default IndexPage