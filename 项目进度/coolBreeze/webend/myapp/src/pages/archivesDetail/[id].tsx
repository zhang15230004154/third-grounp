import React from "react";
import { useEffect, useState } from "react";
import classNames from "classnames";
import { IRootState } from "@/types/models";
import { fomatTime } from "../../utils/index"
import { IRouteComponentProps, useDispatch, useSelector, history } from "umi"
import styles from "./index.less"
import moment from 'moment';
import QRCode from 'qrcode'
import { BackTop, Affix, Modal } from "antd";
import ImageView from '@/components/ImageView';
import HightLight from '@/components/HightLight';
const ArchiveDetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
    let id = props.match.params.id
    const { ArchivesList, Recomends, Archivelike, Archivecommendlists } = useSelector((state: IRootState) => state.docment)
    let recomendlist = Recomends.slice(0, 6);
    console.log(Archivecommendlists)
    // 默认状态
    const [visible, setvisble] = useState(false)
    const [imgSrc, setImgSrc] = useState('');
    const dispatch = useDispatch();
    // 详情
    useEffect(() => {
        dispatch({
            type: "docment/getChivesDetail",
            payload: id
        })
    }, [id])
    // 详情阅读
    useEffect(() => {
        dispatch({
            type: "docment/getrecommends",
            payload: id
        })
    }, [id])
    // 点赞
    useEffect(() => {
        dispatch({
            type: "docment/getarchivelike",
            payload: id
        })
    }, [id])
    // 评论
    useEffect(() => {
        dispatch({
            type: "docment/getarchivecommend",
            payload: { id, page: 1, pageSize: 6 }
        })
    }, [])
    // 详情
    function getArchivesid(id: string) {
        history.push({
            pathname: `/archivesDetail/${id}`,
        })
    }
    // 点赞
    function getvisble() {
        setvisble(true)
    }
    // 下载
    function getarchivelike() {
        setvisble(false)
        window.location.href = "/article"
    }

    moment.updateLocale('en', {
        relativeTime: {
            future: " . %s",
            past: "%s",
            s: '几秒前',
            ss: '%d秒前',
            m: "几分钟前",
            mm: "%d分钟前",
            h: "几小时前",
            hh: "%d小时前",
            d: "几天前",
            dd: "%d天前",
            M: "几月前",
            MM: "%d月前",
            y: "几年前",
            yy: "%d年前"
        }
    });

    useEffect(() => {
        if (Object.keys(Archivelike).length) {
            QRCode.toDataURL(`https://blog.wipi.tech/article/${id}`)
                .then(url => {
                    console.log('url...', url)
                    setImgSrc(url);
                })
                .catch(err => {
                    console.error('err...', err)
                })
        }
    }, [Archivelike]);
    return <div className={classNames(styles.archives_box)}>
        <div className={classNames(styles.archive_positionss)}>
            <p>
                <span className={classNames(styles.archive_nums)}>{Archivelike.likes}</span>
                <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" className={classNames(styles.archive_xin)}><path d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z" fill="currentColor"></path></svg>
            </p>
            <p>
                <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" ><path d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z" fill="currentColor"></path></svg>
            </p>
            <p>
                <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" onClick={() => { getvisble() }}><path d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z" fill="currentColor"></path></svg>
            </p>
        </div>
        <Modal
            title="分享海报"
            cancelText="关闭"
            okText="下载"
            visible={visible}
            onOk={() => { getarchivelike() }}
            onCancel={() => { setvisble(false) }}
        >
            <h1>
                <img src={Archivelike.cover} alt="" />
                {Archivelike.title}
            </h1>
            <div dangerouslySetInnerHTML={{ __html: Archivelike.summary! }}></div>

            <div className={classNames(styles.archive_weima)}>
                <img src={imgSrc} alt="" />
                <div className={classNames(styles.archive_weima_con)}>
                    <h5>识别二维码看文章</h5>
                    <h5>原文分享来自<a href={`https://blog.wipi.tech/article/${id}`} className={classNames(styles.archive_as)}>小楼又清风</a></h5>
                </div>
            </div>
        </Modal>
        <ImageView>
            <HightLight>
                <div className={classNames(styles.archives__box_left)}>
                    {ArchivesList.cover && <img src={ArchivesList.cover} className={classNames(styles.archive_imgs)} />}
                    <p className={classNames(styles.h1)}>
                        <h1>{ArchivesList.title}</h1>
                        发布于{fomatTime(ArchivesList.publishAt!)}•阅读量{ArchivesList.views}
                    </p>
                    <div dangerouslySetInnerHTML={{ __html: ArchivesList.html! }}></div>
                    <p>发布于{fomatTime(ArchivesList.publishAt!)}  | 版权信息：非商用-署名-自由转载</p>
                    <BackTop />
                    <strong style={{ color: 'rgba(64, 64, 64, 0.6)' }}> gray </strong>
                    <div className={styles.archive_sparks}>
                        <div className={styles.archive_sparkes_one}>
                            <h5>评论</h5>
                            <textarea placeholder="请输入评论内容（支持 Markdown）"></textarea>
                        </div>
                        <div className={classNames(styles.archive_scrtops)}>

                        </div>
                    </div>
                </div>
            </HightLight>
        </ImageView>
        <div className={classNames(styles.archives_contents)}>
            <Affix >
                <ul className={classNames(styles.archives__box_right)}>
                    <h3 className={classNames(styles.archives_box_h3)}>推荐阅读</h3>
                    {
                        recomendlist.map((item, index) => {
                            return <li key={item.id}>
                                <span onClick={() => { getArchivesid(item.id) }}>
                                    {item.title}&emsp;
                                    {moment([item.createAt]).fromNow()}
                                </span>
                            </li>
                        })
                    }
                </ul>
            </Affix>
        </div>
    </div>
}
export default ArchiveDetail