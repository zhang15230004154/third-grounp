
// 详情页
import classNames from "classnames";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "umi"
import styles from "./detail.less"
import { Button } from 'antd';
import { ShareAltOutlined, EyeOutlined } from '@ant-design/icons';
import moment from 'moment'
import { fomatTime } from "@/utils/index";
import {IRootState} from "@/types/models"
import {KnowledgeListType,KnowledgeDetailChildren, KnowledgeDetailType} from "@/types/konwledge"
import { IArticleItem } from "@/types";


const Detail: React.FC = (props: any) => {
    const dispatch = useDispatch()
    useEffect(() => { //页面挂载
        let { id } = props.match.params //通过动态id
        dispatch({ //获取详情页
            type: "knowledge/getDetail",
            payload: id
        })
        dispatch({ //再次获取知识小册列表 
            type: "knowledge/getKnowledgeList",
        })
    }, [])
    let shareKnowList = useSelector((state:IRootState) => state.knowledge) //共享knowledgeList状态
    let {knowledgeDetailList, knowledgeList } = shareKnowList
    let { id } = props.match.params
    let otherList = knowledgeList.filter((item:KnowledgeListType) => item.id != id) //筛选出右侧其他知识笔记

    moment.updateLocale('en', {
        relativeTime: {
            future: ". %s",
            past: "%s",
            s: '几秒前',
            ss: '%d秒前',
            m: "几分钟前",
            mm: "%d分钟前",
            h: "几小时前",
            hh: "%d小时前",
            d: "几天前",
            dd: "%d天前",
            M: "几月前",
            MM: "%d月前",
            y: "几年前",
            yy: "%d年前"
        }
    });

    const getKonwledgeDetail = (id:string) => { //右侧点击详情
        props.history.push(`/konwledgeDetail/${id}`)
        location.reload();
    }
    const getNotesDetail=(knowledgeDetailList: KnowledgeDetailType)=>{//跳转文章详情
        let childId = knowledgeDetailList.children[0].id
        let parentId = knowledgeDetailList.children[0].parentId
  
        props.history.push(`/notes/${parentId},${childId}`)
    
    }
    const getNotesDetailButtom=(id:string,parenId:string)=>{

        let childId = id
        let parentId =parenId
  
        props.history.push(`/notes/${parentId},${childId}`)
    }


    return (<>

        <div className={classNames(styles.detail_box)} style={{
            backgroundImage: `url(${knowledgeDetailList.cover})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            filter: 'blur(20px)',
            position: "absolute",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundPositionX: '50%',
            backgroundPositionY: '17%',
            opacity: .2,
          
        }}>
        </div>
        <div className={classNames(styles.detail)}>
            <div className={classNames(styles.detail_left)}>
                <h3>{knowledgeDetailList.title}</h3>
                <dl>
                    <dt><img src={knowledgeDetailList.cover} /></dt>
                    <p className={classNames(styles.title)}>{knowledgeDetailList.title}</p>
                    <p>{knowledgeDetailList.summary}</p>
                    <p>{knowledgeDetailList.views}次阅读<span>{fomatTime(knowledgeDetailList.publishAt)}</span></p>
                    <Button
                    size="large"
                    onClick={()=>getNotesDetail(knowledgeDetailList)}
                    >开始阅读</Button>
                </dl>

                <ul>
                    {
                        knowledgeDetailList.children && knowledgeDetailList.children.map((item:KnowledgeDetailChildren, index: number) => {
                            return (
                                <li key={index}
                                onClick={()=>getNotesDetailButtom(item.id,item.parentId)}
                                >{item.title} <span className={classNames(styles.time)}>{fomatTime(item.publishAt)}</span></li>
                            )
                        })
                    }
                </ul>
            </div>


            <div className={classNames(styles.detail_right)}>
                <h3>其他知识笔记</h3>
                {
                    otherList.map((item:KnowledgeListType, index: number) => {
                        return (
                            <div className={classNames(styles.right_item)} key={index} onClick={() =>getKonwledgeDetail(item.id)}>
                                <h4>{item.title}</h4><span>{moment(item.publishAt).fromNow()}</span>
                                <dl key={index} className={classNames(styles.dlitem)}>

                                    <dt><img src={item.cover} /></dt>
                                    <dd className={classNames(styles.dditem)}>
                                        <p>{item.summary}</p>
                                        <span><EyeOutlined />{item.views}</span>
                                        <span><ShareAltOutlined twoToneColor="#eb2f96" />分享</span>
                                    </dd>
                                </dl>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    </>
    )
}
export default Detail
