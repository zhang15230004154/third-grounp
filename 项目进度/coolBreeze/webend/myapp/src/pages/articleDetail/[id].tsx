import { IRootState } from "@/types";
import { fomatTime } from "@/utils";
import classNames from "classnames";
import styles from './detail.less'
import React, { useEffect, useState } from "react";
import { IRouteComponentProps, useDispatch, useSelector, } from 'umi'
import HightLight from "@/components/HightLight";
import moment from "moment";
import ImageView from "@/components/ImageView";

const ArticleDetail: React.FC<IRouteComponentProps<{ id: string }>> = props => {
    // console.log('ididid',props.match.params.id);
    let id = props.match.params.id;
    console.log(id, 'ididiididid')
    const dispatch = useDispatch();
    const { articleDetail, articleComment } = useSelector((state: IRootState) => state.article)
    const [commentPage, setcommentPage] = useState();
    let [page, setPage] = useState(1);
    const article = useSelector((state: IRootState) => state.article)
    useEffect(() => {
        dispatch({
            type: 'article/getArticleDetail',
            payload: id,
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: 'article/getArticleComment',
            payload: {
                id,
                page: commentPage
            }
        })

    }, [commentPage]);
    useEffect(() => {
        dispatch({
          type: 'article/getRecommend',
          payload: ''
        })
      }, [])
      useEffect(() => {
        dispatch({
          type: 'article/getArticleList',
          payload: page
        })
      }, [page])
    if (!Object.keys(articleDetail).length) {
        return null;
    }
    return <div className={classNames(styles.tps)}>
      <ImageView>
      <img src={articleDetail.cover} alt="" />
        <h1>{articleDetail.title}</h1>
        <div>
            <p>
                发布于{fomatTime(articleDetail.publishAt!)}·阅读量{articleDetail.views}
            </p>
            <HightLight>
                <div dangerouslySetInnerHTML={{ __html: articleDetail.html! }}></div>
            </HightLight>
            <p>发布于{fomatTime(articleDetail.publishAt!)} | 版权信息：非商用-署名-自由转载</p>
        </div>
        </ImageView>
        <div className={classNames(styles.right)}>
        <div className={classNames(styles.top)}>
          <p>推荐阅读</p>
          <ul>
            {
              article.articleList.map(item => {
                return <li key={item.id}>{item.title} • <span>{moment(item.updateAt).fromNow()}</span></li>
              })
            }
          </ul>
        </div>
        <div className={classNames(styles.bottom)}>
          <p>文章标签</p>
          <ul className={classNames(styles.ul)}>
            {
              article.getlabelList.map(item => {
                return <li key={item.id}>{item.label}[{item.articleCount}]</li>
              })
            }
          </ul>
        </div>
      </div>
    </div>
}
export default ArticleDetail