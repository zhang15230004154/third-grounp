import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink, Router } from 'umi';
import { IRootState } from '../types/models'
import { Carousel } from 'antd';
import styles from './index.less';
import moment from 'moment';
import './article.css'
import { history } from 'umi'
import Recomment from '../components/Recomment'
import ImageView from '@/components/ImageView';
import Routercomponent from '../components/Routercomponent'
import Unix from '@/components/Unix';

export default function IndexPage() {
  let [page, setPage] = useState(1);
  let dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);
  // console.log(article.getSwiper,"swiperswiperswiper")

  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
      payload: ''
    })
  }, [])
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page])
  useEffect(() => {
    dispatch({
      type: 'article/getlabelLists',
      payload: ''
    })
  }, [])
  useEffect(()=>{
    dispatch({
      type: 'article/getSwiper',
      payload: ''
    })
  },[])
  moment.updateLocale('en', {
    relativeTime: {
      future: " . %s",
      past: "%s",
      s: '几秒前',
      ss: '%d秒前',
      m: "几分钟前",
      mm: "%d分钟前",
      h: "几小时前",
      hh: "%d小时前",
      d: "几天前",
      dd: "%d天前",
      M: "几月前",
      MM: "%d月前",
      y: "几年前",
      yy: "%d年前"
    }
  });
  return (
    <div className={classNames(styles.main)}>
      <ImageView>
        <Carousel autoplay className={classNames(styles.swiper)}>
            {
              article.getSwiper.map(item=>{
                return <div key={item.id}>
                  <h2 className={classNames(styles.h2)}>{item.title}</h2>
                  <span className={classNames(styles.span)}><time dateTime="2021-05-13 09:06:41">3 个月前·{item.views}次阅读</time></span>
                  <img src={item.cover} alt="" />
                </div>
              })
            }
        </Carousel>
      </ImageView>
      <section>
          <Routercomponent></Routercomponent>
       <Unix></Unix>
      </section>
       <Recomment></Recomment>
    </div>
  );
}
