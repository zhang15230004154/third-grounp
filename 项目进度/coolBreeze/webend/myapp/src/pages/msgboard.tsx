import React, { useState } from "react";
import classnames from "classnames"
import styles from "./msgboard.less"
import { IRootState } from "../types"
import { NavLink, useDispatch, useSelector } from "umi"
import { useEffect } from "react";
import { Pagination } from 'antd';
import { Avatar, Icon } from 'antd';
import { fomatTime } from "@/utils/index";
import moment from 'moment'

import 'antd/dist/antd.css';
interface kist {
    name: string,
    content: string,
    userAgent: string
}
const Msgboard: React.FC = () => {
    const dispatch = useDispatch()
    const [page, setpage] = useState(1)
    const [kist, kists] = useState<kist[]>([])
    const [count, counts] = useState("")
    const mlist = useSelector((state: IRootState) => state.msgboard)
    console.log(mlist);

    function getpage(page: number) {
        setpage(page)
        console.log(page);

        // if (mlist.msgboard) {
        //     kists(mlist.msgboard.slice((page - 1) * 6, page * 6))
        //     //     console.log(kists);
        // }
    }

    //评论推荐
    useEffect(() => {
        dispatch({
            type: "msgboard/msgtuijian",
        })
    }, [])
    //评论页数 
    //评论列表
    useEffect(() => {
        dispatch({
            type: "msgboard/msglist",
            // payload:page
        })
    }, [])
    // function shareArticle(e: React.MouseEvent, item: IArticleItem){
        // e.stopPropagation();
        // e.preventDefault();
        // share(item);
    // }

    moment.updateLocale('en', {
        relativeTime: {
            future: ". %s",
            past: "%s",
            s: '几秒前',
            ss: '%d秒前',
            m: "几分钟前",
            mm: "%d分钟前",
            h: "几小时前",
            hh: "%d小时前",
            d: "几天前",
            dd: "%d天前",
            M: "几月前",
            MM: "%d月前",
            y: "几年前",
            yy: "%d年前"
        }
    });
    return <div className={classnames(styles.msgher)}>
        <div className={classnames(styles.msgherr)}>
            <h2>留言板</h2>
            <p>请勿灌水 🤖</p>
        </div>
        <div className={classnames(styles.msgheader)}>
            <div className={classnames(styles.msgheader1)}>
                <h5>评论</h5>
                <div className={classnames(styles.msglists)}>
                    <div className={classnames(styles.msginput)}>
                        <input type="text" value={count}
                            onChange={
                                e => counts(e.target.value)
                            } />
                        <button>发布</button>
                    </div>
                    <div className={classnames(styles.msgslist)}>
                        {
                            mlist.msgboard.slice((page - 1) * 6, page * 6).map((itme: any, index: number) => {
                                return <dl key={index}>
                                    <dt><Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }} >{itme.name}</Avatar>{itme.name}</dt>
                                    <dd>{itme.content}</dd>
                                    <dd>{itme.userAgent} · {moment(itme.createAt).fromNow()}</dd>
                                    <dd className={classnames(styles.dd)}>
                                        {
                                            itme.children && itme.children.map((itmes: any, index: number) => {
                                                return <div key={index}>
                                                    <p><Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf' }} >{itmes.name}</Avatar>{itmes.name}</p>
                                                    <p>{itmes.content}</p>
                                                    <p>{itmes.userAgent}</p>
                                                </div>
                                            })
                                        }
                                    </dd>
                                </dl>
                            })
                        }
                        <div className={classnames(styles.fenye)}>
                            <Pagination size="small" defaultCurrent={page} total={21} onChange={getpage} />
                        </div>
                    </div>
                </div>
            </div>
            <div className={classnames(styles.msgfooter)}>
                {
                    mlist.msgtuijians.map((itme: any, index: number) => {
                        return <NavLink to={`/msgboardDetail/${itme.id}`} key={index} className={classnames(styles.dls)}>
                            {/* <dl> */}
                            <dt>
                                <h3><b>{itme.title}</b><span>|&ensp;{moment(itme.publishAt).fromNow()}</span><span> {index == 0 ? null : "|"}&ensp;{itme.category && itme.category.label}</span></h3>
                                {
                                    index == 0 ? <p ><span style={{ color: "black" }}> 点赞 {itme.likes}&ensp;·</span><span style={{ color: "black" }}>{itme.views}</span><span style={{ color: "black" }}>·&ensp;分享</span></p> : null
                                }
                            </dt>
                            <dd>
                                <h1 >{itme.cover && <img src={itme.cover} />}</h1>
                                <p style={{ color: "black" }} className={classnames(styles.red)}>
                                    {itme.summary}
                                    {
                                        index != 0 ? <b className={styles.p1}><span > 点赞 {itme.likes}&ensp;· </span><span>{itme.views}</span><span> ·&ensp;分享</span></b>
                                            : null
                                    }
                                </p>
                            </dd>
                            {/* </dl> */}
                        </NavLink>
                    })
                }
            </div>
        </div>
    </div>
}
export default Msgboard