import { useDispatch, useSelector } from "@/.umi/plugin-dva/exports"
import { IRouteComponentProps } from "@umijs/renderer-react"
import React from "react"
import { useEffect } from "react"
import { msgboardIdtypye } from "@/types/msgboard"
import styles from "./detail.less"
import classNames from "classnames"
import HightLight from "@/components/HightLight"
import ImageView from "@/components/ImageView"
import { fomatTime } from "@/utils/index";
import moment from 'moment'
const MsgboardDetail: React.FC<IRouteComponentProps<{ id: string }>> = props => {
    const dispatch = useDispatch()
    const msgboardId = useSelector((state: any) => state.msgboard)
    console.log(msgboardId);
    useEffect(() => {
        const { id } = props.match.params
        dispatch({
            type: "msgboard/msgboardId",
            payload: id
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: "msgboard/msgtuijian",
            // payload:page
        })
    }, [])
    moment.updateLocale('en', {
        relativeTime: {
            future: ". %s",
            past: "%s",
            s: '几秒前',
            ss: '%d秒前',
            m: "几分钟前",
            mm: "%d分钟前",
            h: "几小时前",
            hh: "%d小时前",
            d: "几天前",
            dd: "%d天前",
            M: "几月前",
            MM: "%d月前",
            y: "几年前",
            yy: "%d年前"
        }
    });
    if (!Object.keys(msgboardId.getmsgboard).length) {
        return null
    }
    return <div className={classNames(styles.detailq)}>
        <div className={classNames(styles.detailqq)}>
            <div className={classNames(styles.detailher)}>
                <div className={classNames(styles.imgs)}>
                    <ImageView>
                        {
                            msgboardId.msgtuijians.map((itme: any, index: number) => {
                                return msgboardId.getmsgboard.id == itme.id ? <h3 key={index}><img src={itme.cover} /> </h3> : null
                            })
                        }
                    </ImageView>
                </div>
                <h1 style={{ textAlign: "center", }}>{msgboardId.getmsgboard.title}</h1>
                <p style={{ textAlign: "center", fontSize: "14px" }}>
                    <i>
                        发布于{msgboardId.getmsgboard.publishAt!}·阅读量{msgboardId.getmsgboard.views}
                    </i>
                </p>
                <div className={classNames(styles.detailt)}>
                    <HightLight>
                        <div dangerouslySetInnerHTML={{ __html: msgboardId.getmsgboard.html! }}></div>
                    </HightLight>
                </div>
                <p style={{ textAlign: "center", fontSize: "14px" }}>发布于{msgboardId.publishAt!} | 版权信息：非商用-署名-自由转载</p>
            </div>
            <div className={classNames(styles.detailfoll)}>
                <div className={classNames(styles.detailtop)}>
                    <h4>推荐阅读</h4>
                    <ul>
                        {
                            msgboardId.msgtuijians.map((itme: any, index: number) => {
                                return <li key={index}>{itme.title} · <span className={classNames(styles.time)}>{moment(itme.publishAt).fromNow()}</span>
                                </li>
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    </div>
}
export default MsgboardDetail