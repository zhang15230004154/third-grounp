import { Button, Input, Modal } from "antd"
import { useEffect, useState } from "react"
import style from './index.less'
interface Props {
    bool: boolean,
    getBool(): void
}
const Login: React.FC<Props> = (props) => {
    const [user, setUser] = useState('')
    const [email, setEmail] = useState('');
    const [visible, setVisible] = useState(props.bool);
    const [flag, setFlag] = useState(false);

    useEffect(() => {
        setVisible(props.bool);
    }, [props.bool])

    function handleOk() {
        localStorage.user = JSON.stringify({ name: user, email: email })
        setVisible(false);
    }
    function handleCancel() {
        console.log('Clicked cancel button');
        setVisible(false);
        props.getBool()
    }
    return <div>
        <Modal
            title="请设置你的信息"
            visible={visible}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>取消</Button>,
                <Button key="submit" type="primary" onClick={handleOk} disabled={!flag}>设置</Button>
            ]}
        >
            <p>
                *名称&emsp;<Input style={{ width: '85%' }} value={user} onChange={(e) => {
                    setUser(e.target.value)
                }} />
            </p>
            <br />
            <div className={style.email}>
                *邮箱：<Input style={{ width: '85%' }} value={email} onChange={(e) => {
                    setEmail(e.target.value);
                    setFlag(/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(email));
                }} />
                {/* {
                    flag ? '' : <p className={style.wrong}>输入合法邮箱地址，以便在收到回复时邮件通知</p>
                } */}
            </div>
        </Modal>
    </div>
}

export default Login