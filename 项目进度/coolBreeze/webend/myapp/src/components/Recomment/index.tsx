import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink } from 'umi';
import { IRootState } from '../../types/models'
import styles from './index.less';
import moment from 'moment';



export default function IndexPage() {
  let [page, setPage] = useState(1);
  let dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article)

  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
      payload: ''
    })
  }, [])
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page])
  useEffect(() => {
    dispatch({
      type: 'article/getlabelLists',
      payload: ''
    })
  }, [])
  moment.updateLocale('en', {
    relativeTime: {
      future: " . %s",
      past: "%s",
      s: '几秒前',
      ss: '%d秒前',
      m: "几分钟前",
      mm: "%d分钟前",
      h: "几小时前",
      hh: "%d小时前",
      d: "几天前",
      dd: "%d天前",
      M: "几月前",
      MM: "%d月前",
      y: "几年前",
      yy: "%d年前"
    }
  });
  return (
    <div className={classNames(styles.main)}>
      <div className={classNames(styles.right)}>
        <div className={classNames(styles.top)}>
          <p>推荐阅读</p>
          <ul>
            {
              article.articleList.map(item => {
                return <NavLink to={`/articleDetail/${item.id}`} key={item.id}>
                  <li>{item.title} • <span>{moment(item.updateAt).fromNow()}</span></li>
                </NavLink>
              })
            }
          </ul>
        </div>
        <div className={classNames(styles.bottom)}>
          <p>文章标签</p>
          <ul className={classNames(styles.ul)}>
            {
              article.getlabelList.map(item => {
                return <li key={item.id}>{item.label}[{item.articleCount}]</li>
              })
            }
          </ul>
        </div>
      </div>
    </div>
  );
}
