import { useDispatch, useSelector, NavLink } from 'umi';
import './index.less'

const Routercomponent: React.FC = props=>{
 return <div className="heads">
    <NavLink to='/article'>所有</NavLink>
    <NavLink to='/main/fe'>前端</NavLink>
    <NavLink to='/main/be'>后端</NavLink>
    <NavLink to='/main/reading'>阅读</NavLink>
    <NavLink to='/main/linux'>Linux</NavLink>
    <NavLink to='/main/leetcode'>LeetCode</NavLink>
    <NavLink to='/main/news'>要闻</NavLink>
 </div>
}

export default Routercomponent;