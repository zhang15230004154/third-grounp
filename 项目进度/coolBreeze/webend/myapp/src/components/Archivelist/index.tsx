import React from "react";
import classNames from 'classnames';
import moment from 'moment';
import { useEffect } from 'react';
import { useDispatch, useSelector, history, NavLink } from 'umi';
import { IRootState } from '../../types/models'
import styles from '../../pages/archives.less'
import { BackTop } from "antd"
const Archivelist: React.FC = () => {
    moment.updateLocale('en', {
        relativeTime: {
            future: " . %s",
            past: "%s",
            s: '几秒前',
            ss: '%d秒前',
            m: "几分钟前",
            mm: "%d分钟前",
            h: "几小时前",
            hh: "%d小时前",
            d: "几天前",
            dd: "%d天前",
            M: "几月前",
            MM: "%d月前",
            y: "几年前",
            yy: "%d年前"
        }
    });
    let dispatch = useDispatch()
    let { docment } = useSelector((state: IRootState) => state.docment);
    useEffect(() => {
        dispatch({
            type: 'docment/getDocmentList',
            payload: ''
        })
    }, [])
    // 通过点击的id跳转详情
    function getArchivesid(id: string) {
        history.push({
            pathname: `/archivesDetail/${id}`,
        })
    }
    return <div className={classNames(styles.left)}>
        <div className={classNames(styles.lefts)}>
            <h1 className={classNames(styles.reds)}>归档</h1>
            <p>共计<span className={classNames(styles.red)}>33</span>篇</p>
        </div>
        {
            Object.keys(docment)
                .sort((a, b) => {
                    return Number(b) - Number(a)
                })
                .map((item, index) => {
                    return <div key={index} className={classNames(styles.leftContent)}>
                        <p>{item}</p>
                        {
                            Object.keys((docment)[Number(item)]).map((item2, index2) => {
                                return <div key={index2}>
                                    <h3>{item2}</h3>
                                    <ul className={classNames(styles.time)}>
                                        {
                                            Object.values(
                                                (docment as any)[Number(item)][item2],
                                            ).map((item3: any) => {
                                                return (
                                                    <li key={item3.id} onClick={() => { getArchivesid(item3.id) }}>
                                                        <a href="#">
                                                            <span>
                                                                {moment(item3.createAt).format('MM-DD')}
                                                            </span>
                                                            <span className={classNames(styles.titles)}>{item3.title}</span>
                                                        </a>
                                                    </li>
                                                );
                                            })
                                        }
                                    </ul>
                                    <BackTop />
                                    <strong style={{ color: 'rgba(64, 64, 64, 0.6)' }}> gray </strong>
                                </div>
                            })
                        }
                    </div>
                })
        }
    </div>
}
export default Archivelist