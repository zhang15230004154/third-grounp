import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, NavLink, useHistory } from 'umi';
import { IRootState } from '../../types/models'
import styles from './index.less';
import moment from 'moment';
import { Modal } from 'antd';
import { IArticleItem } from '@/types';
import share from '../Share';

interface IProps{
  articleList?: IArticleItem[], 
  [key:string]: any
}
const IndexPage:React.FC<IProps>=props=> {
  let [page, setPage] = useState(1);
  let [visible, setVisible] = useState(false)
  let history=useHistory()
  let dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);
  // const {getShareList}=useSelector((state:IRootState)=>state.article);
  // console.log(getShareList,'asasasaasa')
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
      payload: ''
    })
  }, [])
  useEffect(()=>{
    dispatch({
      type:'article/getShareList',
      payload:''
    })
  })
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page])
  useEffect(() => {
    dispatch({
      type: 'article/getlabelLists',
      payload: ''
    })
  }, [])
 
  moment.updateLocale('en', {
    relativeTime: {
      future: " . %s",
      past: "%s",
      s: '几秒前',
      ss: '%d秒前',
      m: "几分钟前",
      mm: "%d分钟前",
      h: "几小时前",
      hh: "%d小时前",
      d: "几天前",
      dd: "%d天前",
      M: "几月前",
      MM: "%d月前",
      y: "几年前",
      yy: "%d年前"
    }
  });
  // function shareArticle(){
  //    setVisible(true)
  // }
  function shareArticle(e: React.MouseEvent, item: IArticleItem){
    e.stopPropagation();
    e.preventDefault();
    share(item);
}
  return (
    <div className={classNames(styles.main)}>
      <section>
        {
          article.articleList.map(item => {

            return <div className={classNames(styles.li)} key={item.id}>
              <p 
              onClick={()=>history.push(`/articleDetail/${item.id}`)} 
              className={classNames(styles.title)}>{item.title}&emsp;&emsp;<span>{moment(item.updateAt).fromNow()}</span></p>

              <dl>
                <dt  onClick={()=>history.push(`/articleDetail/${item.id}`)}>{item.cover && <img className={classNames(styles.img)} src={item.cover} alt="" />}</dt>
                <dd>
                  <p  onClick={()=>history.push(`/articleDetail/${item.id}`)} className="ziduan">{item.summary}</p>

                  <p className='ziduan'>
                    <br />
                    <br />
                    <span> <svg viewBox="64 64 896 896" focusable="false" data-icon="heart" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M923 283.6a260.04 260.04 0 00-56.9-82.8 264.4 264.4 0 00-84-55.5A265.34 265.34 0 00679.7 125c-49.3 0-97.4 13.5-139.2 39-10 6.1-19.5 12.8-28.5 20.1-9-7.3-18.5-14-28.5-20.1-41.8-25.5-89.9-39-139.2-39-35.5 0-69.9 6.8-102.4 20.3-31.4 13-59.7 31.7-84 55.5a258.44 258.44 0 00-56.9 82.8c-13.9 32.3-21 66.6-21 101.9 0 33.3 6.8 68 20.3 103.3 11.3 29.5 27.5 60.1 48.2 91 32.8 48.9 77.9 99.9 133.9 151.6 92.8 85.7 184.7 144.9 188.6 147.3l23.7 15.2c10.5 6.7 24 6.7 34.5 0l23.7-15.2c3.9-2.5 95.7-61.6 188.6-147.3 56-51.7 101.1-102.7 133.9-151.6 20.7-30.9 37-61.5 48.2-91 13.5-35.3 20.3-70 20.3-103.3.1-35.3-7-69.6-20.9-101.9zM512 814.8S156 586.7 156 385.5C156 283.6 240.3 201 344.3 201c73.1 0 136.5 40.8 167.7 100.4C543.2 241.8 606.6 201 679.7 201c104 0 188.3 82.6 188.3 184.5 0 201.2-356 429.3-356 429.3z"></path></svg> 点赞{item.likes}</span>
                    <span>  •  </span>
                    <span> <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg> 浏览{item.views}</span>
                    <span>  •  </span>
                    <span onClick={e => shareArticle(e, item)}> <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg> 分享</span>
                  </p>
                </dd>
              </dl>
            </div>
          })
        }
       
      </section>
      <Modal
        title='分享'
        visible={visible}
        onOk={() => { setVisible(false) }}
        onCancel={() => { setVisible(false) }}
      >
      
      </Modal>
    </div>
  );
}



export default IndexPage
