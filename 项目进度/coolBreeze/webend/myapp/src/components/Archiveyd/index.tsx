import React from "react";
import classNames from 'classnames';
import moment from 'moment';
import { useEffect } from 'react';
import { useDispatch, useSelector, history, NavLink } from 'umi';
import { IRootState } from '../../types/models'
import styles from '../../pages/archives.less'
import { Affix, BackTop } from "antd"
const Archivesyd: React.FC = () => {
    moment.updateLocale('en', {
        relativeTime: {
            future: " . %s",
            past: "%s",
            s: '几秒前',
            ss: '%d秒前',
            m: "几分钟前",
            mm: "%d分钟前",
            h: "几小时前",
            hh: "%d小时前",
            d: "几天前",
            dd: "%d天前",
            M: "几月前",
            MM: "%d月前",
            y: "几年前",
            yy: "%d年前"
        }
    });
    let dispatch = useDispatch()
    let { docment, ArchivesYd, Catgorys } = useSelector((state: IRootState) => state.docment);

    // 阅读
    useEffect(() => {
        dispatch({
            type: "docment/getChivesYd",
            patload: ""
        })
    }, [])
    // 文章分类
    useEffect(() => {
        dispatch({
            type: "docment/getcatgorys",
            payload: ""
        })
    }, [])
    // 通过点击的id跳转详情
    function getArchivesid(id: string) {
        history.push({
            pathname: `/archivesDetail/${id}`,
        })
    }
    return <div className={classNames(styles.right)}>
        <Affix>
            <div>
                <div className={classNames(styles.top)}>
                    <p>推荐阅读</p>
                    <ul>
                        {
                            ArchivesYd.map((item, index) => {
                                return <li key={item.id} onClick={() => { getArchivesid(item.id) }}>{item.title}{moment([item.createAt]).fromNow()}</li>
                            })
                        }
                    </ul>
                </div>
                <div className={classNames(styles.bottom)}>
                    <p>文章标签</p>
                    <ul className={classNames(styles.ul)}>
                        {
                            Catgorys.map((item, index) => {
                                console.log(item.value)
                                return <li key={item.id}>
                                    <NavLink to={`/main/${item.value}`}>{item.label}</NavLink>
                                    <span>共计{item.articleCount}篇文章</span>
                                </li>
                            })
                        }
                    </ul>
                </div>
            </div>
        </Affix>
    </div>
}
export default Archivesyd