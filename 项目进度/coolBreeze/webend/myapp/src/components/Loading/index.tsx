import { useSelector } from '@/.umi/plugin-dva/exports';
import { IRootState } from '@/types';
import { Spin } from 'antd';
import classNames from 'classnames';
import styles from './index.less';

const Loading=()=>{
    const { global, language } = useSelector((state: IRootState) => {
        return {
            global: state.loading.global,
            language: state.language
        }
    })
    return <>
     {global && <Spin className={classNames(styles.loading)} />}
    </>
}
export default Loading