//留言板
export interface getmsglist {
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId?: any;
    replyUserName?: any;
    replyUserEmail?: any;
    createAt: string;
    updateAt: string;
    children: Child[];
  }
  export interface msgtuijiantype {
    id: string;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category?: any;
    tags: any[];
  }
  //id
  export interface msgboardIdtypye {
    id: string;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    password?: any;
  }
  interface Child {
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId: string;
    replyUserName: string;
    replyUserEmail: string;
    createAt: string;
    updateAt: string;
  }