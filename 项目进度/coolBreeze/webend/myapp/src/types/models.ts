import { AboutModelState } from "@/models/about";
import { ArticleModelState } from "@/models/article";
import { LanguageModelState } from "@/models/languages";
import {DocmentModelState} from '../models/archives'
import {msgAboutModelState} from "../models/msgboard"
export interface IRootState{
  knowledge: any;
  language:LanguageModelState
  article:ArticleModelState
  getlabelList:ArticleModelState
  docment:DocmentModelState
  about:AboutModelState
  commont:AboutModelState
  public:AboutModelState
  msgboard:msgAboutModelState
  msgboardId:msgAboutModelState
  loading:{
    global:boolean
  }
}

