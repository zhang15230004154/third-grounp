//关于
export interface RootObject {
  id: string;
  cover: string;
  name: string;
  path: string;
  order: number;
  content: string;
  html: string;
  toc: string;
  status: string;
  publishAt: string;
  views: number;
  createAt: string;
  updateAt: string;
}

export interface RootObjects {
  id: string;
  name: string;
  email: string;
  content: string;
  html?: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: Child[];
}

export interface Child {
  id: string;
  name: string;
  email: string;
  content: string;
  html?: any;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}


//发表
export interface PublicObject {
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
}