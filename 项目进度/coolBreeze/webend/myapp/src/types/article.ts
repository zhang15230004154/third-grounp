export interface IArticleItem {
    [x: string]: any;
    id: string;
    title: string;
    cover?: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category?: Category;
    tags: Category[];
  }
  
  interface Category {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
  }



export interface RootObject {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export interface RootObjects {
  id: string;
  title: string;
  cover: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category: Category;
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}


export interface EggObject {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category: Category;
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

export interface ReadObject {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category: Category;
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

export interface LinuxObject {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category: Category;
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
export interface LeetObject {
  id: string;
  title: string;
  cover: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category: Category;
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
export interface NewObject {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category: Category;
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

export interface IArticleDetail {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  password?: any;
}
export interface IArticleComment {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: any[];
}
export interface IArticleDetail {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  password?: any;
}
export interface SwiperObject {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

export interface IPosterItem{
  width: number;
  height: number;
  html: string;
  name: string;
  pageUrl: string;
}


//文章标签类型
export interface tagtype {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Tag[];
}

interface Tag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}