//知识小册
export interface KnowledgeListType{
    id: string;
    parentId: null;
    order: number;
    title: string;
    cover: string;
    summary: string;
    content: null;
    html: null;
    toc: null;
    status: string;
    views: number;
    likes: number;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
  }
  export interface ClassifyListType{
      id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
  }
  export interface KnowledgeDetailChildren {
      id: string;
      parentId: string;
      order: number;
      title: string;
      cover: null;
      summary: null;
      content: string;
      html: string;
      toc: string;
      status: string;
      views: number;
      likes: number;
      isCommentable: boolean;
      publishAt: string;
      createAt: string;
      updateAt: string;
    }
  export interface KnowledgeDetailType{
      id: string;
      parentId: null;
      order: number;
      title: string;
      cover: string;
      summary: string;
      content: null;
      html: null;
      toc: null;
      status: string;
      views: number;
      likes: number;
      isCommentable: boolean;
      publishAt: string;
      createAt: string;
      updateAt: string;
      children: KnowledgeDetailChildren[];
  }
  