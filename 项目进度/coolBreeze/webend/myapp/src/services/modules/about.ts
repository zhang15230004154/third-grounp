import { request } from "umi";
//获取关于列表
export function getAboutList(){
    return request('/api/page/about')
}

export function getCommont(page=1,pageSize=6){
    return request ('/api/comment/host/a5e81ffe-0ad0-4be9-acca-c0462b1b98a1',{
        params:{
            page,
            pageSize
        },
        method:'GET'
    })
}
//发表
// export function getPublic(content:string,email:string,hostId:string,name:string,url:string){
//     return request('/api/comment',{
//         date:{
//          content,
//          email,
//          hostId,
//          name,
//          url
//         },
//     }) 
// }
export function getPublic(){
    return request('/api/comment') 
}