import { IPosterItem } from "@/types";
import { request } from "umi";
// 获取文章列表
export function getArticleList(page:number,pageSize=6,status='publish'){
    //表示请求地址栏上面的查询参数
    return request('/api/article',{
        params:{
            page,
            pageSize,
            status,
        },
        
    })
}
// 获取推荐文章
export function getRecommend(id?:string){
    let queryString='';
    id&&(queryString=`?ariicleId=${id}`)
    return request(`/api/article/recommend${queryString}`)
}
//获取轮播图
export function getSwiper(){
    return request('/api/article/all/recommend')
}
//获取文章标签
export function getlabelLists(articleStatus='publish'){
    return request('/api/tag',{
        params:{
            articleStatus
        }
    })
}
//获取前端数据
export function getwebList(page:number,pagesize:6,status='publish'){
    
    return request('/api/article/category/fe',{
        params:{
           page,
           pagesize,
           status
        },
        
    })
}

//获取后端数据
export function getEggList(page:number,pagesize:6,status='publish'){
    
    return request('/api/article/category/be',{
        params:{
           page,
           pagesize,
           status
        },
        
    })
}

//获取阅读数据
export function getreadList(page:number,pagesize:6,status='publish'){
    
    return request('/api/article/category/reading',{
        params:{
           page,
           pagesize,
           status
        },
        
    })
}

//获取linux数据
export function getlinuxList(page:number,pagesize:6,status='publish'){
    
    return request('/api/article/category/linux',{
        params:{
           page,
           pagesize,
           status
        },
       
    })
}

//获取leetcode数据
export function getleetList(page:number,pagesize:6,status='publish'){
    
    return request('/api/article/category/leetcode',{
        params:{
           page,
           pagesize,
           status
        },
        
    })
}

//获取news数据
export function getnewsList(page:number,pagesize:6,status='publish'){
    
    return request('/api/article/category/news',{
        params:{
           page,
           pagesize,
           status
        },
       
    })
}
//获取文章详情页数据
export function getArticleDetail(id:string){
    return request(`/api/article/${id}/views`,{
        method:'POST'
    })
}

//获取文章评论
export function getArticleComment(id:string,page=1,pagesize=6){
    return request(`/api/comment/host/${id}?page=${page}&pagesize=${pagesize}`)
}
export function genePoster(data: IPosterItem){
    return request('/api/poster', {
        data,
        method: 'POST'
    })
}

//文章标签内容
export function getTag(type:string,page=1,pageSize=12,status='publish'){
    return request(`/api/article/tag/${type}`,{
        params:{
            page,
            pageSize,
            status
        }
    })
}
export function getessay(articleStatus='publish'){
    return request('/api/tag',{
        params:{
            articleStatus
        }
    })
}

//获取文章分类
export function getclassify(articleStatus='publish'){
    return request('/api/category',{
        params:{
            articleStatus 
        }
    })
}