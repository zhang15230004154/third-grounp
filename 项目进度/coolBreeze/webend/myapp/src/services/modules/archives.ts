import { request } from "umi";

//获取归档列表
export function getDocmentList() {
    return request('/api/article/archives')
}
// 归档详情
export function getChivesDetail(id: string) {
    return request(`/api/article/${id}/views`, {
        method: "POST"
    })
}
// 归档阅读
export function getChivesYd() {
    return request('/api/article/recommend')
}
// 文章分类
export function getcatgorys() {
    return request('/api/category?articleStatus=publish')
}
// 每个详情页的推荐阅读
export function getrecommends(id: string) {
    return request(`/api/article/recommend?articleId=${id}`, {
        method: "GET"
    })
}
// 点赞
export function getarchivelike(id: string) {
    return request(`/api/article/${id}/likes`, {
        method: "POST"
    })
}
// 评论
export function getarchivecommend(id: string, page = 1, pageSize = 6) {
    return request(`/api/comment/host/${id}`, {
        params: {
            page,
            pageSize
        }
    })
}
