//知识小册
import {request} from "umi"
export function KnowledgeList(){ //获取知识小册文章
   return request('/api/Knowledge')
}
export function Classify(){
   return request('/api/category')
}
export function getDetail(id:string){
   return request(`/api/knowledge/${id}`)
}
export function getArticleDetails(id:string){
   return request(`/api/article/${id}`)
}
