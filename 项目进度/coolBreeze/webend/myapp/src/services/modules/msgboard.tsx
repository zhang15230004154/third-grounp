//留言板
import { request } from "umi";
export function msglist(page=1,pageSize=21){
    return request("/api/comment/host/8d8b6492-32e5-44e5-b38b-9a479d1a94bd",{
        params:{
            page,
            pageSize
        },
        method:"GET"
    })
}
export function msgtuijian(){ 
    return request("/api/article/recommend",{
        method:"GET"
    })
}
//获取详情id
export function msgboardId(id:string){ 
    console.log(123); 
    return request(`/api/article/${id}`)
}