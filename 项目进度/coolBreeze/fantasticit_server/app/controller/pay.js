'use strict';

const Controller = require('egg').Controller;
const AlipaySdk = require('alipay-sdk');
const AlipayFormData = require('alipay-sdk/lib/form');

// 普通公钥模式
const alipaySdk = new AlipaySdk({
    // 参考下方 SDK 配置
    appId: '2021000118608286',
    privateKey: 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCgeTRqSB7XjnkVRb1o1E4lsfxaI5R9fS/tGGQ3xFJuJNH5lgVbKISNKXksXflt7d5V2mB7yCBKA7UJ441GdKvegnu7B+43D6uzhbPpdT4NTW1slyETWB7XaDblOa7/Ge3N/A63AoJPFxUN/G+yghpeJxk/DvXzwi5DzIn8SD5Q8LidGi295MKa5vT6D/eU2hYU2OKScZQuPruq7rayftGIsYUHp1nc/WjTXjhKxfLUrVZqvt6wUd+NWzyQgxkVmu7oua5t7VS+qTbYy5HaNxkNQ+8opTlCqEXImm+5bYV441hGy6N85VACYaL4/MCi1oRH8i2RDTScUJeid+cwciynAgMBAAECggEAU+9xqhG1CBx+WtEShE1f3rmc++zWU1jyg3oZmU23q+LYbUq53Hdx9X/24Gz69TPQ8mj5xGTl9rC/tb9u+qY163mmjCpWwDgTICBT/U2UulKAZKcb5slVwGfDJtyoGsiOKQqpC7JlREcXfSxHpEAG2K5stx31KZ2so0vN9MY4yg56+6bAJP/ofkVKYL6aGWc7VYWIHtYVFqDmx8jM3V91EWrd9InuyqAVaBxRSTOPlGf5loDF6mRG8vuVaFi/VN6HscUZderG6x+Ko9r1YFLakZnlAaguPcHd4Go2ynRu/KsornkgjBnfzJE3TNCbA6iClEOihLFStesXj5dL8+Nr4QKBgQDPWtgtXdEIV3B+JI0wi3fjbsnXQDXYHE+E6/ixuOhaJbeefYpygVIU8lIlv0KapdIRNAnFw2OY3TXa3UMFeT/+ULba1bEVRUQ0CBTLs5e2rmhBFUDRwtFCkiusXhgWk1bkelOQGJX0V8NZLvDQDbI10Y/iLLtGdh4fwx+agmTRsQKBgQDGHsstdlbTNpTzpCG5DH7NgI4I83noSn+EeBdzC+HcfbhT3MdJYW2YiiSBVK7mdWdfXQ+fdm65s+nXwcRmOd/BnAudZA+sAl8e00VkIxS7VcBCi6ZsXnqcs/a58SKfzBa1CwQcSOBcKd4ejG2FBk1uhG/4Y34E40qpUkAokfJh1wKBgQCfip1UYp/oFCcY15TO6jCHjIG+ncgQDNBeTJWgYM5eNjEOwDdbHQeD2GK+amebx8RWujxerpSeVG4QhTgBKSbEzWO0AsFqxU3R3vnpNbigtZa/pNOn0Ybi+sahrBBKpnkv+LL85v3R8Aj9QLHG0xLwd9qGQrahY1aQR7i0UGm0MQKBgAVecEMbId83a3Hu0LsjC4ymHzM+5d7Vpf8BAiGsAMLo5UXghcYHTeEEXVjj4pl8NQDwlKvNmdKOfZfLOHdMujOU9vuc6o/OwKUlNIwyJy2sdTlYb3ogVVJJzjXbwwRxk2v0kHfzwaC9wbcyNwWT1iVYkXA5wgB/6FYOVI7E5byhAoGADIATItdITBL6wAWXXmRTA6Q1SsKwqiKe0/J6ArtHwhIKL10CFKvHXhSNK9TUKkR6HLxQEOiL3PPpZfo8mg7YfWgJMDrlbYkp5W7aaPQoLUbIdH/3aXVOTyjgqjIN3XIDIViradV73YW+0mnCEGrNBmTOZgn85bEGI8k9cTQ0Ow4=',
    //可设置AES密钥，调用AES加解密相关接口时需要（可选）
    encryptKey: 'sLD3BiyCifxjBhm3DDOM4Q=='
});

class PayController extends Controller {
    async index() {
        const { ctx } = this;
        ctx.body = 'hi, egg';
    }

    // 创建支付
    async create() {
        const { ctx } = this;
        let {totalMoney,id} = ctx.request.body
        const formData = new AlipayFormData();
        formData.setMethod('get');
        formData.addField('notifyUrl', 'http://www.com/notify');
        formData.addField('bizContent', {
            outTradeNo: id,
            productCode: 'FAST_INSTANT_TRADE_PAY',
            totalAmount: totalMoney,
            subject: '商品',
            body: '商品详情',
        });
        res.setHeader('Access-Control-Allow-Origin', '*');

        alipaySdk.exec(
            'alipay.trade.page.pay',
            {},
            { formData: formData },
        ).then(result => {
            // result 为 form 表单
            console.log(result);
            ctx.body={data:result}
        }).catch(err => {
          ctx.body={mag:err.toString()}
        })
    }
}

module.exports = PayController;
