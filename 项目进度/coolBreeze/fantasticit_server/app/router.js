'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  /**
   *  Method	Path	Route Name	Controller.Action
    GET	/mail	mail	app.controllers.mail.index
    GET	/mail/new	new_post	app.controllers.mail.new
    GET	/mail/:id	post	app.controllers.mail.show
    GET	/mail/:id/edit	edit_post	app.controllers.mail.edit
    POST	/mail	mail	app.controllers.mail.create
    PUT	/mail/:id	post	app.controllers.mail.update
    DELETE	/mail/:id	post	app.controllers.mail.destroy
   */
 
  // Restful方式添加邮件路由
  router.resources('/mail', controller.mail);
  //添加邮箱路由
  router.resources('/pay',controller.pay)
};
