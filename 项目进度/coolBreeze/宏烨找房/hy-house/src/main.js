import Vue from 'vue'
import App from './App'
//引入vuex并挂载
import store from "../src/store/index"

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  store,
  ...App
})
app.$mount()
