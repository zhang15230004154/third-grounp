export interface Workbench {
    id: string;
    title: string;
    cover?: string;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    totalAmount?: string;
    isPay: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    tags: any[];
    category?: any;
}

export interface WorkCommend {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: string;
  replyUserName?: string;
  replyUserEmail?: string;
  createAt: string;
  updateAt: string;
}


export interface TypeRespose {
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
}


