export interface IKnowledge {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
export interface iFiles {
  id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
};

export interface CCObject {
  id?: string;
  originalname?: string;
  filename?: string;
  type?: string;
  size?: number;
  url?: string;
  createAt?: string;
}
export interface Ibooks {
  title: string;
  cover: string;
  summary: string;
  isCommentable: boolean;
  parentId?: any;
  content?: any;
  html?: any;
  toc?: any;
  id: string;
  order: number;
  status: string;
  views: number;
  likes: number;
  publishAt: string;
  createAt: string;
  updateAt: string;
}