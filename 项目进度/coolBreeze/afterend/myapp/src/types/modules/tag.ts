 export interface TypeTag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export interface Idata{
  label:string,
  value:string
}