export interface MailObject {
  id: string;
  from: string;
  to: string;
  subject: string;
  text?: string;
  html?: string;
  createAt: string;
}

export interface IReplyMail{
  to: string;
  subject: string;
  html: string;
}