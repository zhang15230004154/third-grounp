
export interface TypeAllariticle {
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  toc?: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Tag[];
  category?: Tag;
  content?: string;
  html?: string;
}

interface Tag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
