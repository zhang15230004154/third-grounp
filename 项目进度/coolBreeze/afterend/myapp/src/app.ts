
//umi运行时配置文件
import { RequestConfig } from 'umi';
// import {createLogger} from 'redux-logger'
import { message } from 'antd';
// 引入mobx的Provider
import StateContext from '@/context/stateContext'
import store from '@/models/index'
import React from 'react';
import { getToken } from "@/utils/cookie"

// 覆盖根组件
export function rootContainer(container: React.ReactNode) {
    return React.createElement(StateContext.Provider, { value: store }, container);
}

// NProgree.configure({ showSpinner: false, trickleSpeed: 500 });
export const dva = {
    config: {
        // onAction: createLogger(),
        onError(e: Error) {
            message.error(e.message, 3);
        },
    },
};
// 路由切换配置
// export function onRouteChange({ matchedRoutes }:any) {

//   }

let baseUrl = 'https://creationapi.shbwyz.com'

export const request: RequestConfig = {
    timeout: 10000,
    errorConfig: {},
    middlewares: [],
    // 请求拦截器
    requestInterceptors: [(url, options) => {
        let authorization = getToken();
        if (authorization) {
            options = { ...options, headers: { authorization: 'Bearer ' + authorization } };
        }
        if (!/https?/.test(url)) {
            url = `${baseUrl}${url}`;

        }
        return {
            url,
            options,
        };
    }],

    //响应拦截器
    responseInterceptors: [response => {
        const codeMaps: { [key: number]: string } = {
            400: '账号密码错误',
            403: '禁止访问',
            404: '找不到资源',
            500: '服务器内部错误',
            502: '网关错误。',
            503: '服务不可用，服务器暂时过载或维护。',
            504: '网关超时。',
        };
        //处理网络请求错误
        if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
            message.error(codeMaps[response.status])
        }
        // 处理业务逻辑请求错误
        // if(response.data.success!==true){
        //     message.error(response.data.msg)
        // }
        // console.log('response.....',response)
        return response
    }]
};