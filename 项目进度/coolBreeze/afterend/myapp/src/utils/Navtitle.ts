import Icon, {
    FormOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    UploadOutlined,
    AppstoreOutlined,
    BarChartOutlined,
    CloudOutlined,
    ShopOutlined,
    TeamOutlined,
    MenuUnfoldOutlined,
    GithubOutlined
 } from '@ant-design/icons';
export const Navtitle = [
    {
       to: '/home/workbench',
       title: '工作台',
       icon: MenuUnfoldOutlined,
    },
    {
       to: '/home/article',
       title: '文章管理',
       icon: MenuFoldOutlined,
       children: [
          {
             to: '/home/article/allarticle',
             title: '所有文章',
             icon: ShopOutlined
          }, {
             to: '/home/article/classify',
             title: '分类管理',
             icon: MenuFoldOutlined
          }, {
             to: '/home/article/label',
             title: '标签管理',
             icon: CloudOutlined
          }
       ]
    }, {
       to: '/home/pagesetup',
       title: '页面管理',
       icon: UserOutlined
    }, {
       to: '/home/knowledge',
       title: '知识小册',
       icon: VideoCameraOutlined
    }, {
       to: '/home/poster',
       title: '海报管理',
       icon: UploadOutlined
    }, {
       to: '/home/comment',
       title: '评论管理',
       icon: TeamOutlined
    }, {
       to: '/home/mail',
       title: '邮件管理',
       icon: ShopOutlined
    }, {
       to: '/home/file',
       title: '文件管理',
       icon: CloudOutlined
    }, {
       to: '/home/search',
       title: '搜索记录',
       icon: MenuUnfoldOutlined
    }, {
       to: '/home/browse',
       title: '访问统计',
       icon: BarChartOutlined
    }, {
       to: '/home/user',
       title: '用户管理',
       icon: AppstoreOutlined
    },
    {
       to: '/home/setting',
       title: '系统设置',
       icon: VideoCameraOutlined
    }
 ]
 export const Menus = [
    {
       to: '/main/ameditor',
       title: '新建文章-协同编辑器'
    },
    {
       to: '/main/editor',
       title: '新建文章'
    },
    {
       to: '/main/editor',
       title: '新建页面'
    }
 ]
