//面包屑
export const breadcrumbNameMap:any = {
    '/home': '工作台',
    '/home/workbench': '',
    '/home/article': '文章',
    '/home/article/allarticle': '所有文章',
    '/home/article/classify': '分类管理',
    '/home/article/label':'标签管理',
    '/home/pagesetup':'页面管理',
    '/home/knowledge':'知识小册',
    '/home/poster':'海报管理',
    '/home/comment':'评论管理',
    '/home/mail':'邮件管理',
    '/home/file':'文件管理',
    '/home/search':'搜索记录',
    '/home/browse':'访问统计',
    '/home/user':'用户管理',
    '/home/setting':'系统设置'
  };

