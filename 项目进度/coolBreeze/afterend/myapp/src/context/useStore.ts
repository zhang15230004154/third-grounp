import stateContext from "./stateContext";
import {useContext} from 'react';

export default function(){
    return useContext(stateContext)
};

// 接收一个context对象并返回该context的值，当前context值由上一层组件中距离当前组件最近的value props决定

// 1.首先我们需要创建一个context来挂载我们useReducer创建的redux
// //context.js 创建context
// import { createContext } from 'react';
// export default createContext(null);


//2. 在context上挂载我们useReducer创建出的redux，通过context提供的Provider组件
// //APP父组件 挂载redux
// import reducer from './store/reducer';
// import PersonalContext from '../../context';
// const [personalState, personalDispatch] = useReducer(reducer, { name: '123' });

// <PersonalContext.Provider value={{ personal: { personalState, personalDispatch } }}>
//    <Home />
// </PersonalContext.Provider>

// 3.接下来我们就可以通过这个context来获取到挂载在上面的redux，使用方法有两种：useContext方法和context提供的Consumer组件
// //Home子组件 使用redux
// import React, { useContext } from 'react';
// import Personale from '../../context';

// const Home= () => {
//   const personal = useContext(Personale);
//   console.log(personal);
//   return (
//     <Personale.Consumer>
//         {value=>{console.log(value)}}
//     </Personale.Consumer>
//   );
// };
// export default Home;