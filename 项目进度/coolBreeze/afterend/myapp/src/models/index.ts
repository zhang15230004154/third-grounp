import Mail from './modules/mail'
import File from './modules/file'
import Search from './modules/search'
import User from './modules/user'
import Comment from "./modules/comment"
import Workbenchs from "./modules/workbench";
import Browseid from './modules/browseid'
import Tag from './modules/tag'
import Catecory from './modules/catecory'
import pageset from "./modules/pageset"
import Allarticle from './modules/allAriticle'
import Knowledges from "./modules/Knowledge"
import Pageeditor from "./modules/pageeditor"
import Article from './modules/article'
import Setting from "./modules/setting"
export default {
    
    Mail: new Mail(),
    File: new File(),
    Search: new Search(),
    User: new User(),
    Workbench: new Workbenchs(),
    comment: new Comment(),
    Pageset: new pageset(),
    Knowledges: new Knowledges(),
    browse:new Browseid(),
    tag:new Tag,
    catecory:new Catecory(),
    allarticle:new Allarticle(),
    Pageeditor:new Pageeditor(),
    Article:new Article(),
    Setting : new Setting()
}


