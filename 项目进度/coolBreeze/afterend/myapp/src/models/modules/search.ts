
import {message} from 'antd'
import { getSearchList,deleteSearchList } from '../../services/modules/search'
import {makeAutoObservable} from 'mobx'
import { SearchObject } from '@/types'
class Search{
    SearchList:SearchObject[]=[]
    SearchNum=0
    static Search:any
    constructor(){
        makeAutoObservable(this)
    }
    async getSearchList(page:number,params:{[key:string]:string|boolean},pageSize:number){
        let result=await getSearchList(page,params,pageSize);
        if(result.data){
            this.SearchList=result.data[0]
            this.SearchNum=result.data[1]
        }
    }
    async deleteSearchList(id:string){
        let result=await deleteSearchList(id);
        let params={}
        if(result.data){
            this.getSearchList(1,params,12)
        }
    }
    async deleteSearch(ids:string[]){
        message.loading('操作中', 0);
        let params={}
        Promise.all(ids.map(id => deleteSearchList(id)))
            .then(res => {
                message.destroy()
                message.success('删除成功');
                this.getSearchList(1,params,12);
            })
            .catch(err => {
                message.destroy()
                message.error('删除失败');
            })
    }
}
export default Search