import { makeAutoObservable, runInAction } from "mobx"
import { getList, tabUpdata, delPage } from "@/services/modules/pageset"
interface IPageType {
    id: string
    cover: string
    name: string
    path: string
    order: number
    content: string
    html: string
    toc: string
    status: string
    publishAt: string
    views: number
    createAt: string
    updateAt: string
}
class Pageset {
    page: number = 0
    pageSize: number = 0
    value = ''
    pageList: IPageType[] = [];
    pageCount: number = 0;
    constructor() {
        makeAutoObservable(this);
    }

    async getPageList(page: number, pageSize: number, value: any) {
        let result = await getList(page, pageSize, value)
        this.page = page
        this.pageSize = pageSize
        this.value = value
        if (result.data) {
            runInAction(() => {
                this.pageList = result.data[0]
                this.pageCount = result.data[1]
            })
        }
    }
    async setTabUpdata(id: string, value: { status: string }) {
        let result = await tabUpdata(id, value)
        if (result.data) {
            this.getPageList(this.page, this.pageSize, this.value)

        }
    }
    async setDelPage(id: string) {
        let result = await delPage(id)
        if (result.data) {
            this.getPageList(this.page, this.pageSize, this.value)

        }
    }



}

export default Pageset;