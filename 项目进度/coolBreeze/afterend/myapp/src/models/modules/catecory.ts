import { getCategoryList,addCategory, deleteCategory, updateCategory } from "@/services";
import { Idata, TypeCategory } from "@/types";
import { makeAutoObservable, runInAction } from "mobx";

class Catecory{
    categoryList:TypeCategory[]=[]
     constructor(){
        makeAutoObservable(this);
    }
    async getCategoryList(){
        let result=await getCategoryList()
        if(result.data){
            runInAction(()=>{
                this.categoryList=result.data
            })
        }
        return result.data
    }
    async addCategory(values:Idata,token:string){
        let result=await addCategory(values,token);
        if(result.data){
            runInAction(()=>{
                this.categoryList=[...this.categoryList,result.data]
            })
        }
    }
    async deleteCategory(id:string){
        let result=await deleteCategory(id);
        if(result.data){
            this.getCategoryList()
        }
    }
    async updateCategory(id:string,data:Idata){
        let result=await updateCategory(id,data);
        if(result.data){
            this.getCategoryList()
        }
    }
}

export default Catecory