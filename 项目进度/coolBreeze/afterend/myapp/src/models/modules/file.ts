
import { getFileList } from '@/services/modules/file'
import { FileObject } from '@/types'
import {makeAutoObservable, runInAction} from 'mobx'
class File {
    FileList:FileObject[]=[]
    FileNum=0
    // file
    static File:any
    constructor(){
        makeAutoObservable(this)
    }
    async getFileList(page:number,params:{[key:string]:string|boolean},pageSize:number){
        let result=await getFileList(page,params,pageSize);
        console.log(result,'reault******')
        if(result.data){
            runInAction(()=>{
                this.FileList=result.data[0]
                this.FileNum=result.data[1]
            })
        }
        return {FileList:this.FileList,FileNum:this.FileNum}
    }
}
export default File