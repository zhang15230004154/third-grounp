import { addLabel, getTagList } from "@/services";
import { Idata, TypeTag } from "@/types/modules/tag";
import { makeAutoObservable, runInAction } from "mobx";



class Tag{
     tagList:TypeTag[]=[]
     constructor(){
        makeAutoObservable(this);
    }
    async getTagList(){
        let result=await getTagList()
        if(result.data){
            runInAction(()=>{
                this.tagList=result.data
            })
        }
        return result.data
    }
    async addTags(values:Idata,token:string){
        let result=await addLabel(values,token);
        if(result.data){
            runInAction(()=>{
                this.tagList=[...this.tagList,result.data]
            })
        }
    }

}

export default Tag