//访问统计
import { getbrowse} from '@/services'; 
import { browseType} from '@/types';
import { Effect, ImmerReducer, Reducer } from 'umi';

export interface browseModelState {
    browseArr: browseType [], 
    comontcount:number
}

export interface browseModelType {
    namespace: 'browse';
    state: browseModelState;
    effects: {
        getbrowse: Effect; 
    };
    reducers: {
        save: Reducer<browseModelState>;
    };
}

const browses: browseModelType = {
    namespace: 'browse',

    state: {
        browseArr: [], 
        comontcount:0
    },

    effects: { 
        *getbrowse({ payload }, { call, put }) {
            let result = yield call(getbrowse);
             console.log(result);
            if (result.success == true) {
                yield put({
                    type: 'save',
                    payload: {
                        browseArr: result.data[0],
                        comontcount:result.data[1]
                    }
                })
            }
        }, 
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    },
};

export default browses;