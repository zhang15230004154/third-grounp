import {makeAutoObservable} from 'mobx'
import { allAriticleList, deletePublish, updatePublish } from '@/services'
import { TypeAllariticle } from '@/types'
import { message } from 'antd'

class Allarticle{
    article: TypeAllariticle[] = []
    articleNum: number = 0
    constructor(){
        makeAutoObservable(this)
    }
    //异步函数
    async allAriticleList(page:number,pageSize:number,params:{[key:string]:string|boolean}){
        let result=await allAriticleList(page,pageSize,params)
        if(result.statusCode==200){
            this.article=result.data[0]
            this.articleNum=result.data[1]
        }
        // console.log('allll',result)
    }
   
    async updatePublish(ids:string[],status:{[key:string]:string|boolean}){
        let params = {};
      Promise.all(ids.map(id=>updatePublish(id,status))).then(res=>{
          message.success('操作成功')
           this.allAriticleList(1,8,params)
      }).catch(err=>{
          message.error('操作失败')
      })
    }

    async deletePublish(ids:string[]){
        let params = {};
        Promise.all(ids.map(id=>deletePublish(id)))
        .then(res=>{
            // message.success('删除成功');
            this.allAriticleList(1,8,params)
        })
        .catch(err=>{
            // message.error('删除失败');
        })
    }
}
export default Allarticle