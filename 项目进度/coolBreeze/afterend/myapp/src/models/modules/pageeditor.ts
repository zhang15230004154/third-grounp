
import {newPage} from '@/services/modules/pageeditor'
import {makeAutoObservable} from 'mobx'
class Pageeditor {

    constructor(){
        makeAutoObservable(this)
    }
    async getNewPage(value:any){
       let result = newPage(value)
       console.log(result)        
 
    }
}
export default Pageeditor