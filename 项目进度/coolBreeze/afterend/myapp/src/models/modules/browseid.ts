import { getbrowsdel, getbrowse, getIP ,deletebrowse,getGoogle} from '@/services';
import { browseType } from "@/types"
import { message } from "antd"
import { makeAutoObservable, runInAction } from "mobx"

class Browseid {
    browseArr: browseType[] = [];
    comontcount: number = 0;
    getip: string[] = []
    constructor() {
        makeAutoObservable(this)
    }
    //列表
    async getbrowse(page = 1) {
        let result = await getbrowse(page)
        console.log(result.data); 
        if (result.data) {
            runInAction(() => {
                this.browseArr = result.data[0]
                this.comontcount = result.data[1]
            })
        }
    }
    //搜索 
    async getbrowsdel(ids: string) { 
        let result = await getbrowsdel(ids) 
        console.log(result); 
        if(result.success==true){  
            // this.browseArr
            // this.getbrowse()
            message.success("删除成功")
        }else{
            message.success("删除失败")
        } 
    }
    //批量删除
    async deletebrowse(ids:string[]){
        Promise.all(ids.map(id=>deletebrowse(id)))
        .then(res=>{
            message.success("批量删除成功")
            // this.getbrowse()
        })
        .catch(err=>{
            message.error("批量删除失败")
        })
    }
    async getIP(ip: string ,browser:string,engine:string) { 
        
        let result=await getIP(ip,browser,engine) 
        console.log(result); 
        if(result.success==true){  
            this.browseArr=result.data[0]
            this.comontcount=result.data[1] 
            // this.getbrowse(result.data[1])
            message.success("搜索成功")
        }else{
            message.success("搜索失败")
        } 
    }
    // async getGoogle(browser: string) {
    //     let result=await getGoogle(browser) 
    //     if(result.success==true){
    //         // console.log(result.data); 
    //         this.browseArr=result.data[0]
    //         this.comontcount=result.data[1] 
    //         // this.getbrowse()
    //         message.success("搜索成功")
    //     }else{
    //         message.success("搜索失败")
    //     } 
    // }
}
export default Browseid
function urlas(ip: string, browser: string, urlas: any) {
    throw new Error('Function not implemented.');
}

