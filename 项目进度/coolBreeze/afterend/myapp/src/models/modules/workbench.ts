import { deleteCommend, updateCommend, workbenchs, workcommend } from "@/services/modules/workbench";
import {  Workbench, WorkCommend } from "@/types";
import { makeAutoObservable } from "mobx"

class Workbenchs {
  replyMail(arg0: { to: string; subject: string; html: any; }) {
      throw new Error("Method not implemented.");
  }
  workbench: Workbench[] = []
  workbenchnum: number = 0

  workCommend: WorkCommend[] = []
  workCommendNum: number = 0

  constructor() {
    makeAutoObservable(this);
  }
  async workbenchs(page: number, pageSize: number) {
    let result = await workbenchs(page, pageSize)
    // console.log(result)
    if (result.data) {
      this.workbench = result.data[0]
      this.workbenchnum = result.data[1]
    }
  }

  async workcommend(page = 1) {
    let result = await workcommend(page)
    // console.log(result)
    if (result.data) {
      this.workCommend = result.data[0]
      this.workCommendNum = result.data[1]
    }
  }

  // Promise.all可以将多个Promise实例包装成一个新的Promise实例。同时，成功和失败的返回值是不同的，成功的时候返回的是一个结果数组，而失败的时候则返回最先被reject失败状态的值。

  // 更改状态
  async updateCommend(id: string, pass: boolean) {
    let result = await updateCommend(id, pass)
    //   console.log(result)
    if (result.statusCode == 200) {
      this.workcommend()
    }
  }

  // 删除评论
  async deleteCommend(id: string) {
    let result = await deleteCommend(id)
    // console.log(result)
    if (result.data) {
      this.workcommend()
    }
  }
  



  //回复评论
  // async resposedCommend(content: string, createByAdmin: boolean, email: string, hostId: number, name: string, parentCommentId: number, replyUserName: string, url: string) {
  //   let result = await resposedCommend(content, createByAdmin, email, hostId, name, parentCommentId, replyUserName, url)
  //   console.log(result)
  // }

}




export default Workbenchs