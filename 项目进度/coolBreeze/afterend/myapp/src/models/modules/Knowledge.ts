import { makeAutoObservable } from "mobx"
import { GETKnowledge, GETKnowledgedetal, GETKnowledgeshang, GETfiles, GETfileslist, GETbooks } from "../../services/modules/Knowledge"
import { IKnowledge, iFiles, Ibooks } from "../../types/index"
class Knowledge {
    knowledgelist: IKnowledge[] = []
    count = 0
    id = ""
    ids = ""
    filelist: iFiles[] = []
    filen = 0
    filesid = {}
    books = {}
    constructor() {
        makeAutoObservable(this)
    }
    // 知识小测数据
    async GETKnowledge(page: number, pageSize: number, params: { [key: string]: string | boolean }) {
        let result = await GETKnowledge(page, pageSize, params);
        if (result.data) {
            this.knowledgelist = result.data[0]
            this.count = result.data[1]
        }
    }
    // 删除
    async GETKnowledgedetal(id: any) {
        let result = await GETKnowledgedetal(id)
        // console.log(result)
        if (result.data) {
            this.id = result.data
        }
    }
    // 上传
    async GETKnowledgeshang(id: string,status:string) {
        let result = await GETKnowledgeshang(id,status);
        console.log(result)
        if (result.data) {
            this.ids = result.data
            // this.GETKnowledge()
        }
    }
    // 文件上传
    async GETfiles(page: number) {
        let result = await GETfiles(page);
        // console.log(result, "1111")
        if (result.data) {
            this.filelist = result.data[0]
            this.filen = result.data[1]
        }
    }
    // 上传文件里面的更新
    async GETfileslist(id: string, data: Ibooks) {
        console.log(data);
        let resulit = await GETfileslist(id, data);
        if (resulit.data) {
            this.filesid = resulit.data
            this.GETKnowledge()
        }
        console.log(this.filesid)
    }
    // 文件里面的创建
    async GETbooks({ title, summary }: { title: string, summary: string }) {
        let result = await GETbooks({ title, summary });
        if (result.data) {
            this.books = result.data
            this.knowledgelist = [...this.knowledgelist, result.data]
        }
    }
}
export default Knowledge