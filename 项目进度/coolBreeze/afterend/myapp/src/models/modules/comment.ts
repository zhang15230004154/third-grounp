// import { GETcomment, Getcommenture, } from "@/services";
import { deleteComment, GETcomment, updateComment } from "@/services";
import { makeAutoObservable, runInAction } from "mobx"
import { message } from 'antd';
import { request } from "@/.umi/plugin-request/request";
import { ICommentItem } from "@/types";
class commentModel {
    commentlist = [];
    commentCount: number = 0;
    constructor() {
        makeAutoObservable(this)
    }
    // 评论
    async GETcomment(page = 1, params: { [key: string]: string | boolean }) {
        let result = await GETcomment(page, params);
        // console.log(result)
        if (result.data) {
            runInAction(() => {
                this.commentlist = result.data[0];
                this.commentCount = result.data[1];
            })
        }
    }
    // 批量更改评论状态(通过或者未通过)
    async updateComment(ids: string[], status:{[key:string]:boolean}){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>updateComment(id, status)))
        .then(res=>{
            message.destroy()
            message.success('操作成功');
            this.GETcomment();
        })
        .catch(err=>{
            message.destroy()
            message.error('操作失败');
        })
    }


    // 删除评论
    async deleteComment(ids: string[]) {
        message.loading('操作中', 0);
        Promise.all(ids.map(id => deleteComment(id)))
            .then(res => {
                message.destroy()
                message.success('删除成功');
                this.GETcomment();
            })
            .catch(err => {
                message.destroy()
                message.error('删除失败');
            })
    }
}
export default commentModel
