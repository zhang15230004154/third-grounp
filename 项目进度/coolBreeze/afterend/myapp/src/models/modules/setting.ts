import {getSetting} from "@/services/index"
import { makeAutoObservable } from "mobx"
class Setting{
    settingList:any=[]
   constructor(){
    makeAutoObservable(this)
   }
  
  async getSetting(){
     let result = await getSetting()
     this.settingList = result.data
     
  }
}

export default Setting