import { getlogin, getregister } from "@/services";
import { removeToken, setToken } from "@/utils/cookie";
import { makeAutoObservable } from "mobx"

class User {
    isLogin = false;
    userInfo = {};
    constructor() {
        makeAutoObservable(this);
    }

    async login(data: { name: string, password: string }) {
        let result = await getlogin(data);
        console.log(result)
        if (result.data) {
            this.isLogin = true;
            this.userInfo = result.data;
            localStorage.userdata = JSON.stringify(result.data)
            setToken(result.data.token);
        }
        return result.data;
    }
    async register(data: { name: string, password: string, confirm: string }) {
        let result = await getregister(data);
        return result.data;
    }
    loginout() {
        this.isLogin = false;
        this.userInfo = {};
        removeToken();
    }
}

export default User;
