
import { getMailList,deleteMailList,replayMail } from '@/services/modules/mail'
import { IReplyMail, MailObject } from '@/types'
import { message } from 'antd'
import {makeAutoObservable} from 'mobx'
class Mail {
    MailList:MailObject[]=[]
    MailNum=0
    static Mail:any
    constructor(){
        makeAutoObservable(this)
    }
    async getMailList(page:number,params:{[key:string]:string|boolean},pageSize:number){
        let result=await getMailList(page,params,pageSize);
        console.log(result,'reault')
        if(result.data){
            this.MailList=result.data[0]
            this.MailNum=result.data[1]
        }
        
        return {MailList:this.MailList,MailNUm:this.MailNum}
    }
    async deleteMailList(id:string){
        let result=await deleteMailList(id);
        let params={};
        if(result.data){
            this.getMailList(1,params,12)
        }
    }
    async deleteMail(ids:string[]){
        message.loading('操作中');
        let params={};
        Promise.all(ids.map(id=>deleteMailList(id)))
        .then(res=>{
            message.destroy();
            message.success('删除成功');
            this.getMailList(1,params,12)
        })
        .catch(err=>{
            message.destroy()
            message.error('删除失败')
        })
    }
    // 回复邮件
    async replayMail(data:IReplyMail){
      let result=await replayMail(data)
      return result;
    }
}
export default Mail
