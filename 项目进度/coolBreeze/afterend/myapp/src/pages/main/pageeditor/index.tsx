import React, { useState, useEffect } from "react"
import styles from "./index.less"
import classNames from "classnames"
import Editor from 'for-editor'
import { Button, Popover, Drawer, Input, Form, message,Popconfirm } from "antd"
import { makeHtml, makeToc } from '@/utils/markdown';
import useStore from "@/context/useStore"
import { IRouteComponentProps } from "_umi@3.5.17@umi"
const PageEditor: React.FC<IRouteComponentProps> = (props) => { //新增页面
    const store = useStore()
    const [value, setvalue] = useState('')//提交表单
    const [title, setTitleText] = useState('')//标题
    const [visible, setVisible] = useState(false);//抽屉开关
    const [form] = Form.useForm(); //抽屉数据

    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
        console.log(value)
    };

    const content = (
        <div>
            <p>查看</p>
            <p onClick={showDrawer}>设置</p>
            <p style={{ whiteSpace: "nowrap", }}>保存草稿</p>
            <p>删除</p>
        </div>
    );
    useEffect(() => {
        setvalue(
            '# 欢迎使用 Wipi Markdown 编辑器'
        )
        return () => {

        }
    }, [])
    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        subfield: true, // 单双栏模式
    }
    function setTitle(text: string) {
        setTitleText(text)

    }
    //   发布------------------------
    async function getPublish() { //点击发布
        const values = form.getFieldsValue();
        if (title == '') {
            message.warn('请输入文章标题');
            return;
        } else if (!values.path) {
            message.warn('请输入路径');
            setVisible(true)
        }

        // 添加文章md内容
        values.content = value;
        // 添加文章html内容
        values.html = makeHtml(value);
        // 添加文章大纲
        values.toc = makeToc(values.html);
        // 添加文章标题
        values.title = title;
        // 添加文章发布状态
        values.status = 'publish';
        let result = await store.Pageeditor.getNewPage(values);
        console.log('result...', result);
        // if (result.success) {
        //     message.success('文章发布成功');
        // }

    }

    //保存退出-------------------
    function exitEditor(){
       props.history.push('/home/pagesetup')
    }
    return (
        <div className={classNames(styles.Pageeditor)}>
            <div className={classNames(styles.editor_header)}>
                <div >
                    <Popconfirm placement="right" title={"确认关闭? 如果内容变更,请先保存。"} okText="确定" cancelText="取消" onConfirm={exitEditor}>
                        <Button>X</Button>
                    </Popconfirm>

                    <input type="text" value={title} onChange={e => setTitleText(e.target.value)} placeholder="请输入页面的名称..." />
                </div>
                <div>
                    <Button onClick={getPublish} type="primary">发布</Button>
                    <Popover placement="bottom" content={content} trigger="click">
                        <span style={{ color: "blue" }}>· · ·</span>
                    </Popover>

                </div>
            </div>
            <Editor toolbar={toolbar} value={value} onChange={value => { setvalue(value) }}></Editor>
            <Drawer title="页面属性" placement="right" onClose={onClose} visible={visible}

                footer={<Button onClick={onClose} type="primary">确认</Button>}>
                <Form
                    form={form}
                >
                    <Form.Item
                        label="封面"
                        name="cover"
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="路径"
                        name="path"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="顺序"
                        name="order"

                    >
                        <Input type="number" />
                    </Form.Item>
                </Form>
            </Drawer>
        </div>
    )
}
export default PageEditor