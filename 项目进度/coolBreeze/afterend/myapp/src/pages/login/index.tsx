import React, { useEffect } from "react";
import {IRouteComponentProps} from "umi"
import classNames from "classnames"
import styles from "./index.less"
import { Col, Row, Form, Input, Button,message} from 'antd';
import useStore from '@/context/useStore';
import {observer} from 'mobx-react-lite';
import {animation} from "@/utils/animation"
const Login: React.FC<IRouteComponentProps> = (props) => {

   
   useEffect(() => { //动画加载
      animation() 
      return () => {
      }
   }, [])

   const store = useStore();
   const onFinish = async(values: any) => { //点击登录
       let result =  await store.User.login(values)
       if(result){
          message.success('登陆成功')
          props.history.push('/home')
       }
   };

   const toReg=()=>{
      props.history.push("/register")
   }
   return (
      <div className={classNames(styles.login)}>
         <ul className={classNames(styles.login_box)}>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
         </ul>
         
         <Row className={classNames(styles.ant_row)} justify={"space-around"}>
            <Col className={classNames(styles.ant_col1)} lg={12} md={0} xs={0}>
               <img src={require('../../img/W$PNC77RQI$I5U0LM5}S10Z.png')} />
            </Col>
            <Col className={classNames(styles.ant_col2)} lg={12} md={24} xs={24}>
               <div className={classNames(styles.user_box)}>
                  <h1>系统登陆</h1>
                  <Form
                     name="basic"
                     labelCol={{ span: 2 }}
                     wrapperCol={{ span: 22 }}
                     onFinish={onFinish}

                  >
                     <Form.Item
                        label="账号"
                        name="name"
                        rules={[{ required: true, message: '请输入你的账号!' }]}
                     >
                        <Input />
                     </Form.Item>

                     <Form.Item
                        label="密码"
                        name="password"
                        rules={[{ required: true, message: '请输入你的密码!' }]}
                     >
                        <Input.Password />
                     </Form.Item>

                     <Form.Item wrapperCol={{ span: 24 }}>
                        <Button type="primary" htmlType="submit" style={{width:"100%",height:"100%"}}
                        >
                           登录
                        </Button>
                     </Form.Item>
                     Or <span onClick={toReg}>注册用户</span>
                  </Form>
               </div>
            </Col>
         </Row>

      </div>
   )
}
export default observer(Login);