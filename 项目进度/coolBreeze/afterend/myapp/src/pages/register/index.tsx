import React, { useEffect } from "react";
import { IRouteComponentProps } from "umi"
import classNames from "classnames"
import styles from "./index.less"
import { Col, Row, Form, Input, Button, message } from 'antd';
import useStore from '@/context/useStore';
import {observer} from 'mobx-react-lite';
import {animation} from "@/utils/animation"
const Register: React.FC<IRouteComponentProps> = (props) => {
   useEffect(() => { //动画加载
      animation()
      return () => {
      }
   }, [])

   const store = useStore();//获取仓库

   const onFinish = async (values: any) => { //点击注册
      let result = await store.User.register(values)
      if(result){
         message.success('注册成功')
         props.history.push('/login')
      }
   };
  
   const toLogin=()=>{ //去登录页
      props.history.push('/login')
   }
   return (
      <div className={classNames(styles.login)}>
         <ul className={classNames(styles.login_box)}>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
         </ul>

         <Row className={classNames(styles.ant_row)} justify={"space-around"}>
            <Col className={classNames(styles.ant_col1)} lg={12} md={0} xs={0}>
               <img src={require('../../img/E0LJ6N0BV)`EQQXQQ7594M1.png')} />
            </Col>
            <Col className={classNames(styles.ant_col2)} lg={12} md={24} xs={24}>
               <div className={classNames(styles.user_box)}>
                  <h1>用户注册</h1>
                  <Form
                     name="register"
                     onFinish={onFinish}
                     scrollToFirstError
                     labelCol={{ span: 2 }}
                     wrapperCol={{ span: 22 }}
                  >
                     <Form.Item
                        label="账号"
                        name="name"
                        rules={[{ required: true, message: '请输入你的账号!' }]}
                     >
                        <Input />
                     </Form.Item>

                     <Form.Item
                        name="password"
                        label="密码"
                        rules={[
                           {
                              required: true,
                              message: '请输入密码!',
                           },
                        ]}
                        hasFeedback
                     >
                        <Input.Password />
                     </Form.Item>

                     <Form.Item
                        name="confirm"
                        label="确认密码"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                           {
                              required: true,
                              message: '请输入密码!',
                           },
                           ({ getFieldValue }) => ({
                              validator(_, value) {
                                 if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                 }
                                 return Promise.reject(new Error('两次密码不一致!'));
                              },
                           }),
                        ]}
                     >
                        <Input.Password />
                     </Form.Item>
                     <Form.Item wrapperCol={{ span: 24 }}>
                        <Button type="primary" htmlType="submit" style={{ width: "100%", height: "100%" }}
                        >
                           注册
                        </Button>
                     </Form.Item>

                     Or <span onClick={toLogin}>去登陆</span>
                  </Form>
               </div>
            </Col>
         </Row>

      </div>
   )
}
export default observer(Register);