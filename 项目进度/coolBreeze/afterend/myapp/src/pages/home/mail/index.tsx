import React, { Key, useEffect, useState } from "react"
import styles from "./index.less"
import classNames from "classnames"
import { IRouteComponentProps, useDispatch, useSelector } from "umi"
import useStore from "@/context/useStore"
import { Button,Modal, Form, Input, Table, Popconfirm, Pagination, message } from 'antd'
import { MailObject } from "@/types"
import { observer } from 'mobx-react'
import { makeHtml } from "@/utils/markdown"

interface Iform {
  [key: string]: string | boolean
}
const { useForm } = Form;
const Mail: React.FC<IRouteComponentProps> = (props) => {
  let store = useStore();
  const [form] = useForm();
  let [page, setPage] = useState(1);
  let [mailList, setMailList] = useState<Array<MailObject>>([]);
  let [mailNum, setMailNum] = useState(1);
  let [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
  let [pageSize, setPageSize] = useState(12);
  const [showModal,setShowModal]=useState(false);
  const [replay,setReplay]=useState('');
  const [params, setParams] = useState<Iform>({});
  const [current,setCurrent]=useState<MailObject>()
  async function promi() {
    let res = await store.Mail.getMailList(page, params, pageSize);
    setMailList(i => i = res.MailList);
    setMailNum(c => c = res.MailNUm);
  }
  useEffect(() => {
    promi()
    console.log(store.Mail.MailList)
    // store.Mail.getMailList(page,10)
  }, [page, params])
  console.log(store.Mail.MailNum, 'asasassa');
  const warning = () => {
    message.warning('该方可无权进行访问')
  }
  function confirm() {
    store.Mail.deleteMail(selectedRowKeys as string [])
    message.warning('删除成功')
    store.Mail.getMailList(page,params,pageSize)
  }
  function deletes(id:string){
    store.Mail.deleteMailList(id)
  }
  const columns = [
    {
      title: '发件人',
      dataIndex: 'from',
      width: 300,
      render: (text: any) => <a>{text}</a>,
    },
    {
      title: '收件人',
      width: 300,
      dataIndex: 'from',
    },
    {
      title: '主题',
      width: 300,
      dataIndex: 'subject',
    },
    {
      title: '搜索时间',
      width: 300,
      dataIndex: 'createAt',
    },
    {
      title: '操作',
      width: 300,
      key: 'action',
      render: (item:MailObject) => (
        <span>
          <Popconfirm
            title="确认删除这个搜索记录吗?"
            onConfirm={()=>{
              deletes(item.id)
              // console.log(item.id,'idididid')
            }}
            okText='确认'
            cancelText='取消'
          >
            <Button>删除</Button>
            
          </Popconfirm>
          <Button onClick={()=>{
           
            setCurrent(item)
            setShowModal(true)
          }}>回复</Button>
        </span>
      ),
    },
  ];
  // 分页
  function changePage(value: number) {
    setPage(value)
  }
  function submit(){
    let values = form.getFieldsValue();
    let params: Iform = {};

    for (let key in values) {
      values[key] && (params[key] = values[key])
    }
    setParams(params)
  }
  function onReset(){
      form.resetFields()
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  function onSelectChange(selectedRowKeys: Key[]) {
    console.log(arguments, 'aaaaa')
    setSelectedRowKeys(selectedRowKeys)
  }

  function cancel(){

  }
  function showModals() {
    setShowModal(true)
};
// 关闭
function handleOk() {
  setShowModal(false)
};
function handleCancel() {
  setShowModal(false)
};
  // 回复邮件
  async function replayMail(){
    if(!replay){
      message.warn('请输入内容');
      return;
    }
    let result=await store.Mail.replayMail({
      to:current?.from!,
      subject:`回复${current?.subject}`,
      html:makeHtml(replay)
    })
    if(result.message!=='message'){
      message.warn(result.message)
    }else{
      message.success(result.message)
    }
    setShowModal(false)
  }
  return (
    <div className={classNames(styles.mail)}>
      <div className={classNames(styles.search)}>
        <Form form={form} onFinish={submit} style={{ display: 'flex', justifyContent: 'space-between' }} name="advanced_search">

          <Form.Item label='发件人' name='from'>
            <Input type='text' placeholder='请输入发件人' />
          </Form.Item>
          <Form.Item label='收件人' name='to'>
            <Input type='text' placeholder='请输入收件人' />
          </Form.Item>
          <Form.Item label='主题' name='subject'>
            <Input type='text' placeholder='请输入主题' />
          </Form.Item>
          <div className={classNames(styles.sou)} style={{ marginTop: 40 }}>
            <Button htmlType='submit' type='primary'
            >
              搜索
            </Button>
            <Button onClick={onReset}>重置</Button>
          </div>

        </Form>
      </div>
      <div className={classNames(styles.content)}>
      {
          selectedRowKeys.length ?
            <section>
               <Popconfirm
              title="确认删除?"
              onConfirm={confirm}
              onCancel={cancel}
              okText="确认"
              cancelText="取消"
            >
              <Button style={{color:'red'}}>删除</Button>
            </Popconfirm>
           
            </section>
          
            : null
        }
        <Table rowSelection={rowSelection} columns={columns} scroll={{ x: 1300 }} pagination={false} dataSource={mailList} />
        <div className={classNames(styles.page)}>
          共{store.Mail.MailNum}条
          <Pagination defaultCurrent={page} total={store.Mail.MailNum}
            onChange={changePage}
            current={page}
          ></Pagination>
        </div>
      </div>
      <Modal
      onOk={handleOk}
      onCancel={handleCancel}
      footer={<Button onClick={()=>{
        replayMail()
        showModals()
      }}>回复</Button>}
      visible={showModal}>
        <Input.TextArea 
        value={replay}
        rows={10}
      
        onChange={e=>setReplay(e.target.value)}
        style={{marginTop:'25px'}}
        placeholder='支持markdown,html和文本回复'></Input.TextArea>

      </Modal>
    </div>
  )
}
export default observer(Mail)
