import React, { useEffect, useState } from "react"
import styles from "./index.less"
import classNames from "classnames"
// import { useDispatch, useSelector } from "umi";
import { Input, Button, Card, Form, Pagination, Drawer, Popover, message, Popconfirm, Upload, } from 'antd'
import useStore from "@/context/useStore"
import { FileObject } from "@/types"

const { Dragger } = Upload;
const props = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info: { file: { name?: any; status?: any }; fileList: any }) {
        const { status } = info.file;
        if (status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    },
};
interface IForm {
    [key: string]: string | boolean
}
const { Meta } = Card;
const { useForm } = Form;
const File: React.FC = () => {
    const [form] = useForm();
    const [params, setParams] = useState<IForm>({})
    //修改页码
    let [fileObj, setFileObj] = useState<Partial<FileObject>>({})
    let [visible, setVisible] = useState(false);
    let [pageSize, setPageSize] = useState(12)
    let [page, setPages] = useState(1)
    let store = useStore();

    let [fileList, setFileList] = useState<Array<FileObject>>([])
    let [fileNum, setFileNum] = useState(1)
    console.log(store, 'store');
    async function promise() {
        let res = await store.File.getFileList(page, params, pageSize);
        setFileList(i => i = res.FileList)
        setFileNum(i => i = res.FileNum)
    }
    // console.log(store)
    useEffect(() => {
        store.File.getFileList(page, params, pageSize)
        promise()
    }, [page, params])

    //时间
    function renderTime(date: string) {
        let dates = new Date(date).toJSON();
        return new Date(+new Date(dates) + 8 * 3600 * 1000).toISOString().replace(/T/g, '').replace(/\.[\d]{3}Z/, '')
    }
    // 分页
    function sePage(page: number) {
        console.log(page, "pagepage")
        setPages(page)
    }
    function confirm() {
        message.warning('该方可无权进行访问')
    }
    function submit() {
        let values = form.getFieldsValue()
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        console.log(values)
        setParams(params)
    }
    // 重置
    function onReset() {
        form.resetFields()
    }
    function onshouwSize(page: number, pageSize: number) {
        setPages(page)
        setPageSize(pageSize)
        // setParams(params)
    }
    return (
        <div className={classNames(styles.file)}>
            <div className={classNames(styles.upload)}>
                <Dragger {...props}>
                    <p className="ant-upload-drag-icon">
                        {/* <Icon type="inbox" /> */}
                        <svg viewBox="0 0 1024 1024" focusable="false" data-icon="inbox"style={{width:'50px',height:'50px',color:'#09f'}} fill="currentColor" aria-hidden="true"><path d="M885.2 446.3l-.2-.8-112.2-285.1c-5-16.1-19.9-27.2-36.8-27.2H281.2c-17 0-32.1 11.3-36.9 27.6L139.4 443l-.3.7-.2.8c-1.3 4.9-1.7 9.9-1 14.8-.1 1.6-.2 3.2-.2 4.8V830a60.9 60.9 0 0060.8 60.8h627.2c33.5 0 60.8-27.3 60.9-60.8V464.1c0-1.3 0-2.6-.1-3.7.4-4.9 0-9.6-1.3-14.1zm-295.8-43l-.3 15.7c-.8 44.9-31.8 75.1-77.1 75.1-22.1 0-41.1-7.1-54.8-20.6S436 441.2 435.6 419l-.3-15.7H229.5L309 210h399.2l81.7 193.3H589.4zm-375 76.8h157.3c24.3 57.1 76 90.8 140.4 90.8 33.7 0 65-9.4 90.3-27.2 22.2-15.6 39.5-37.4 50.7-63.6h156.5V814H214.4V480.1z"></path></svg>
                    </p>
                    <p className="ant-upload-text" style={{fontSize:'18px'}}>点击选择文件或将文件拖拽到此处</p>
                    <p className="ant-upload-hint" style={{fontSize:'15px'}}>
                       文件将上传到OSS,如未配置请先配置
                    </p>
                </Dragger>
            </div>
            <div className={classNames(styles.search)}>
                <Form form={form} onFinish={submit} style={{ display: 'flex', justifyContent: 'space-between' }} name="advanced_search">

                    <Form.Item label='文件名称' name='originalname'>
                        <Input type='text' placeholder='请输入文件名' />
                    </Form.Item>
                    <Form.Item label='文件类型' name='type'>
                        <Input type='text' placeholder='请输入文件类型' />
                    </Form.Item>
                    <div className={classNames(styles.sou)} style={{ marginTop: 40 }}>
                        <Button htmlType='submit' type='primary'
                        >
                            搜索
                        </Button>
                        <Button onClick={onReset}>重置</Button>
                    </div>

                </Form>
            </div>
            <div className={classNames(styles.content)}>
                {
                    fileList && fileList.map((item: any, index: number) => {
                        return <Card
                            onClick={() => {
                                setVisible(true)
                                setFileObj(item)
                            }}
                            key={index}
                            hoverable
                            style={{ width: '230px', height: '270px', marginLeft: '10px' }}
                            cover={<img alt="example" src={item.url} style={{ height: '180px', width: '230px' }} />}
                        >
                            <Meta title={item.originalname} description={'上传于' + renderTime(item.createAt)} />
                        </Card>
                    })
                }

            </div>
            <div className={classNames(styles.page)}>
                共{store.File.FileNum}条
                <Pagination pageSizeOptions={["5", "7", "8"]}
                    showSizeChanger={true}
                    defaultCurrent={1}
                    onShowSizeChange={onshouwSize}
                    total={fileNum} onChange={(page) => {
                        sePage(page)
                    }}></Pagination>
            </div>
            {/* 抽屉 */}
            <div className={classNames(styles.details)}>
                <Drawer
                    width='650'
                    title="文件信息"
                    placement="right"
                    closable={false}
                    onClose={() => { setVisible(false) }}
                    visible={visible}
                >
                    <div className={classNames(styles.cont)}>
                        <p><img src={fileObj.url} alt="" style={{ width: 580, height: 350 }} /></p>
                        <p>文件名称：{fileObj.originalname}</p>
                        <p>存储路径：{fileObj.filename}</p>
                        <p><span>文件类型：{fileObj.type}</span><span>文件大小：{fileObj.size}</span>KB</p>
                        <p>访问链接<span>{fileObj.url}</span></p>
                    </div>

                    <div className={classNames(styles.btn)}>
                        <Button onClick={() => { setVisible(false) }}>关闭</Button> 
                        <Popconfirm
                            title="确认删除这个搜索记录吗?"
                            onConfirm={confirm}
                            okText='确认'
                            cancelText='取消'
                        >
                            <a><Button>删除</Button></a>
                        </Popconfirm>

                    </div>
                </Drawer>
            </div>

        </div>
    )
}
export default File
