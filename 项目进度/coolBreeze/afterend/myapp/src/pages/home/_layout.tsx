import React, { useState } from "react";
import "./index.less"
import { IRouteComponentProps,Link,NavLink, useLocation } from 'umi'
import { Layout, Menu, Button, Breadcrumb, Dropdown, message,Affix} from 'antd';
import { Menus } from "@/utils/Navtitle";
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';
import Myheader from "@/components/MyHeader"
import { getToken } from "@/utils/cookie";
import MyMenu from "@/components/MyMenu";
import {breadcrumbNameMap} from "@/utils/crumbs"
const { Content, Footer, Sider } = Layout;
const Home: React.FC<IRouteComponentProps> = (props) => {
   const [collapsed, setcollapsed] = useState(false);
   const store = useStore(); //获取仓库
   let locations = useLocation()
   const [visible, setVisible] = useState(false);

// ------------------- 面包屑封装----------
   const { location } = props;
   const pathSnippets = location.pathname.split('/').filter(i => i);
  const extraBreadcrumbItems = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
    return (
      <Breadcrumb.Item key={url}>
        <Link to={url}>{breadcrumbNameMap[url]}</Link>
      </Breadcrumb.Item>
    );
  });
  const breadcrumbItems = [
   <Breadcrumb.Item key="home">
     <Link to="/"></Link>
   </Breadcrumb.Item>,
 ].concat(extraBreadcrumbItems);
// ------------------- 面包屑封装----------

   const menu = (
      <Menu style={{ marginTop: '10px' }}>
         {
            Menus.map((item, index) => {
               return <Menu.Item key={index} style={{ height: '40px' }}>
                  <NavLink to={item.to}>{item.title}</NavLink>
               </Menu.Item>
            })
         }
      </Menu>
   );

   //Menu折叠事件--------
   function toggle() {
      setcollapsed(collapsed => !collapsed)
   }
   
   //路由守卫-------------
   let whiteList = ['/login', '/register','/main/ameditor'] //设置不用验证的白名单
   //如果不输入白名单 并且 没有token
   if (whiteList.indexOf(locations.pathname) == -1 && !getToken()) {
      message.error('请先登录')
      props.history.push('/login')
      return (<>你在干什么</>)
   } else {
      return (
         <div className="home">
          <Layout>
           
                  <Sider trigger={null} collapsible collapsed={collapsed}
                  >
                     <div className="logo" style={{ width: "80%", marginLeft: '15px', height: "50px", }}>
                        <div className="rv_9xUBx9vsH6qbhqqmOc"><img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" /><span style={{ margin: '0 14px' }}>管理后台</span></div>
                     </div>
                     <Dropdown overlay={menu}>
                        <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                           <Button className='button' type="primary" style={{ width: '70%', marginLeft: '20px', height: "40px", color: "#fff", fontSize: '16px' }}>
                              <b>+</b><span>新建</span>
                           </Button>
                        </a>
                     </Dropdown>
                     {/* 右侧组件列表 */}
                     <MyMenu></MyMenu>
                  </Sider>
    
               <Layout className="site-layout">
                  {/* 头部组件 */}
                  <Affix style={{zIndex:10}}>
                     <Myheader toggleup={toggle}></Myheader>
                  </Affix>

                  <Content style={{ margin: '24px 16px 0', overflow: 'hidden', overflowY:"auto"}}>
                     <Breadcrumb>{breadcrumbItems}</Breadcrumb>
                     <>
                        {props.children}
                     </>
                  </Content>
                  {/* <Footer style={{ textAlign: 'center' }}>Ant Design 2018 Created by Ant UED</Footer> */}
               </Layout>
            </Layout>
         </div>

      )
   }
}
export default observer(Home)