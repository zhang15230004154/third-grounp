import React, { useState, useEffect, createRef } from "react"
import styles from "./index.less"
import classNames from "classnames"
import { Form, Input, Button, Tabs, } from 'antd';
import * as monaco from 'monaco-editor/esm/vs/editor/editor.api.js';
import useStore from "@/context/useStore"
import { IRouteComponentProps } from "_umi@3.5.17@umi";
import { observer } from "mobx-react-lite"

const Setting: React.FC<IRouteComponentProps> = (props) => {
    const { TabPane } = Tabs;
    const store  = useStore()
    const enRef: any = createRef();
    const zhRef: any = createRef();
    const [keys, setKeys] = useState('')
    const onFinish = (values: any) => {
        console.log('Success:', values);

    };
    useEffect(() => {
       let result = store.Setting.getSetting()
       
    }, [])

    function tabNav(key: string) {
      let divs = document.querySelector('#settBox')?.children
      console.log(store.Setting.settingList)
    }
    return (
        <div className={styles.setting}>
            <div className={styles.settBox} id="settBox">
                <Tabs tabPosition={'left'} onChange={tabNav}>

                    <TabPane tab="系统设置" key="系统设置">
                        <Form
                            name="basic"
                            labelCol={{ span: 2 }}
                            wrapperCol={{ span: 24 }}
                            onFinish={onFinish}
                            autoComplete="off"
                            layout="vertical"
                        >
                            <Form.Item
                                label="系统地址"
                                name="systemUrl"
                            >
                                <Input placeholder={store.Setting.settingList.systemUrl}/>
                            </Form.Item>

                            <Form.Item
                                label="后台地址"
                                name="adminSystemUrl"
                            >
                                <Input placeholder={store.Setting.settingList.adminSystemUrl}/>
                            </Form.Item>

                            <Form.Item
                                label="系统标题"
                                name="systemTitle"
                            >
                                <Input placeholder={store.Setting.settingList.systemTitle}/>
                            </Form.Item>

                            <Form.Item
                                label="Logo"
                                name="systemLogo"
                            >
                                <Input placeholder={store.Setting.settingList.systemLogo}/>
                            </Form.Item>

                            <Form.Item
                                label="Favicon"
                                name="systemFavicon"
                            >
                                <Input placeholder={store.Setting.settingList.systemFavicon}/>
                            </Form.Item>

                            <Form.Item
                                label="页脚消息"
                                name="systemFooterInfo"
                            >
                                <Input.TextArea placeholder={store.Setting.settingList.systemFooterInfo}/>
                            </Form.Item>
                            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                                <Button type="primary" htmlType="submit">
                                    Submit
                                </Button>
                            </Form.Item>
                        </Form>
                    </TabPane>
                    <TabPane tab="国际化设置" key="国际化设置">
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="en" key="1" >
                                <div ref={enRef} className={styles.text}>
                                </div>
                            </TabPane>
                            <TabPane tab="zh" key="2" >
                               <div ref={zhRef} className={styles.text}>
                               </div>
                            </TabPane>
                        </Tabs>
                    </TabPane>


                    <TabPane tab="SEO设置" key="SEO设置">
                        Content of Tab 3
                    </TabPane>


                    <TabPane tab="数据统计" key="数据统计">
                        Content of Tab 3
                    </TabPane>


                    <TabPane tab="OSS设置" key="OSS设置">
                        Content of Tab 3
                    </TabPane>


                    <TabPane tab="SMTP服务" key="SMTP服务">
                        Content of Tab 3
                    </TabPane>

                </Tabs>
                {/* <div>
                       
                    </div>
                    <div>
                        国际化设置
                    </div>
                    <div>
                        SEO设置
                    </div>
                    <div>
                        数据设置
                    </div>
                    <div>
                        OSS设置
                    </div>
                    <div>
                        SMTP设置
                    </div> */}
            </div>
        </div>
    )
}

export default observer(Setting)