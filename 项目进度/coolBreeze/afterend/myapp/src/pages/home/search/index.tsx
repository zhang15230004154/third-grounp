import React, { Key, useEffect, useState } from "react"
import styles from "./index.less"
import classNames from "classnames"
import { Form, Input, Button, Table, Divider, Popconfirm, message, Pagination } from 'antd'
import useStore from "@/context/useStore"
import { observer } from 'mobx-react'


interface IForm {
  [key: string]: string | boolean
}
const { useForm } = Form;
const Search: React.FC = () => {
  let [page, setPage] = useState(1);
  let [pageSize, setPagesize] = useState(12);
  const [form] = useForm()
  const [params, setParams] = useState<IForm>({})
  let [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
  let store = useStore();
  useEffect(() => {
    store.Search.getSearchList(page, params, pageSize)
  }, [page, params])
  function submit() {
    let values = form.getFieldsValue();
    let params: IForm = {};

    for (let key in values) {
      values[key] && (params[key] = values[key])
    }
    setParams(params)
  }
  console.log(store.Search.SearchList, 'searchsearch')
  function renderTime(date: string) {
    let dates = new Date(date).toJSON();
    return new Date(+new Date(dates) + 8 * 3600 * 1000).toISOString().replace(/T/g, '').replace(/\.[\d]{3}Z/, '')
  }
  function confirm() {
    store.Search.deleteSearch(selectedRowKeys as string[])
     message.success('删除成功');
     store.Search.getSearchList(page,params,pageSize)
  }
  function deletes(id: string) {
    store.Search.deleteSearchList(id)
  }
  
  function onReset() {
    form.resetFields()
  }
  function onshowSize(page: number, pageSize: number) {
    setPage(page)
    setPagesize(pageSize)
  }
  function setPages(page: number) {
    setPage(page)
  }
  const columns = [
    {
      title: '搜索词',
      dataIndex: 'keyword',
      render: (text: any) => <a>{text}</a>,
    },
    {
      title: '搜索量',
      dataIndex: 'count',
      render: (text: number) => <a style={{ display: 'inline-block', width: '20px', height: '20px', borderRadius: '50%', color: '#fff', background: 'green', textAlign: 'center' }}>{text}</a>
    },
    {
      title: '搜索时间',
      dataIndex: 'createAt',
    },
    {
      title: '操作',
      key: 'action',
      render: (text: any, record: any) => (
        <Popconfirm
          title="确认删除这个搜索记录吗?"
          onConfirm={() => {
            deletes(text.id)
            console.log(text.id, 'ididiididid')
          }}
          okText='确认'
          cancelText='取消'
        >
          <a><Button style={{ border: 'none' }}>删除</Button></a>
        </Popconfirm>
      ),

    },
  ];
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  function onSelectChange(selectedRowKeys: Key[]) {
    console.log(arguments, 'aaaaa')
    setSelectedRowKeys(selectedRowKeys)
  }
  function cancel(){

  }
  return (
    <div className={classNames(styles.search)}>
      <div className={classNames(styles.search)}>

        <Form form={form} onFinish={submit} style={{ display: 'flex', justifyContent: 'space-between' }} name="advanced_search">

          <Form.Item label='类型' name='originalname'>
            <Input type='text' placeholder='请输入搜索类型' />
          </Form.Item>
          <Form.Item label='搜索词' name='keyword'>
            <Input type='text' placeholder='请输入搜索词' />
          </Form.Item>
          <Form.Item label='搜索量' name='count'>
            <Input type='text' placeholder='请输入搜索量' />
          </Form.Item>
          <div className={classNames(styles.sou)} style={{ marginTop: 40 }}>
            <Button htmlType='submit' type='primary'
            >
              搜索
            </Button>
            <Button onClick={onReset}>重置</Button>
          </div>

        </Form>

      </div>
      <div className={classNames(styles.content)}>
        {
          selectedRowKeys.length ?
            <section>
               <Popconfirm
              title="确认删除?"
              onConfirm={confirm}
              onCancel={cancel}
              okText="确认"
              cancelText="取消"
            >
              <Button style={{color:'red'}}>删除</Button>
            </Popconfirm>
            </section>
          
            : null
        }
        
        <Table rowKey='id' rowSelection={rowSelection} columns={columns} pagination={false} dataSource={store.Search.SearchList} />
        <div className={classNames(styles.page)}>
          共{store.Search.SearchNum}条
          <Pagination
            onChange={(page) => {
              setPages(page)
            }}
            pageSizeOptions={['5', '7', '12']}
            showSizeChanger={true}
            onShowSizeChange={onshowSize}
            defaultCurrent={1}
            total={store.Search.SearchNum}
          ></Pagination>
        </div>
      </div>
    </div>
  )
}
export default observer(Search)
