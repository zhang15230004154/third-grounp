import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import useStore from "@/context/useStore"
import { useEffect, useState } from "react"
import { Form, Input, Button, Card, Select, Pagination, Drawer, Switch, Upload, message, Popconfirm, Tooltip } from "antd"
import { observer } from "mobx-react-lite"
import { CCObject, iFiles, IKnowledge } from "../../../types/modules/Knowledge"
import ImageView from "@/components/ImageView"
interface IForm {
    [key: string]: string | boolean
}
interface Iprops {
    [key: string]: any
}
const Knowledge: React.FC = () => {
    let store = useStore();
    const { useForm } = Form;
    const [form] = useForm();
    let [page, setpage] = useState(1);
    let [pageSize, setpageSize] = useState(10);
    // 控制上传文件的抽斗
    let [visibles, setvisibles] = useState(false)
    // 抽斗状态
    let [visible, setvisible] = useState(false)
    const [params, setParams] = useState<Iprops>({})
    // 控制是上传还是草稿
    const [aler, setaler] = useState(false)
    // 保存抽斗显示的内容
    const [divlist, setdivlist] = useState<Partial<IKnowledge>>({});
    const { Meta } = Card;
    // 上传头像
    const { Dragger } = Upload;
    // 文件上传里的
    const [page1, setpage1] = useState(1);
    useEffect(() => {
        store.Knowledges.GETfiles(page1)
    }, [page1])
    // 文件里的每张图片
    function getclickid(id: string) {
        let items: any = store.Knowledges.filelist.filter(item => item.id == id);
        console.log(items[0].url, "Dsadasdsa")
        setdivlist(l => {
            return {
                ...l,
                cover: items[0].url
            }
        });
        setvisibles(false);
    };
    // 上传
    const props = {
        name: 'file',
        multiple: true,
        action: `${divlist.cover}`,
        onChange(info: any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    }
    useEffect(() => {
        store.Knowledges.GETKnowledge(page, pageSize, params)
    }, [page, params])
    // 搜索按钮
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {};
        console.log(params)
        console.log(values)
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params)
    }
    // 更新按钮
    function submit1() { }
    // 更新按钮
    function getsubmitid(id: any) {
        store.Knowledges.GETfileslist(id, form.getFieldValue());
        setvisible(false)
    }
    // 分页器
    function onShowSizeChange(current: number, pageSize: number) {
        console.log(current, pageSize);
    }
    // 新建展开
    function showDrawer(item: IKnowledge) {
        form.setFieldsValue({
            ...form,
            ...item
        })
        setvisible(true);
        setdivlist(item)
    };
    // 新建关闭
    function onClose() {
        setvisible(false)
    };
    // 评论开关
    function onChange(createAt: string | undefined) {
        console.log(`switch to ${createAt}`);
    }
    // 改变页码数
    function changepage(current: number, pageSize: any) {
        setpage(current)
        setpageSize(pageSize)
        console.log(current)
    }
    // 气泡确认框
    function confirm(id: string) {
        console.log(id)
        store.Knowledges.GETKnowledgedetal(id)
        setTimeout(() => {
            return window.location.reload()
        }, 100)
        message.success('删除成功');

    }
    // 取消删除
    function cancel(e: any) {
        console.log(e);
        message.error('已取消删除');
    }
    // 上传草稿
    function getchangekeonw(id: string) {
        let statese = store.Knowledges.knowledgelist.filter(item => item.id === id)
        let sum = statese.filter(item => item.status)
        if (sum[0].status === "draft") {
            sum[0].status = "publish";
            store.Knowledges.GETKnowledgeshang(id, sum[0].status)
        } else {
            sum[0].status = "draft"
            store.Knowledges.GETKnowledgeshang(id, sum[0].status)
        }
        console.log(sum[0].status)
    }
    // 上传文件抽斗显示隐藏
    function showDrawers() {
        setvisibles(true)
    };

    function onCloses() {
        setvisibles(false)
    }
    // 创建按钮
    function getcreact() {
        store.Knowledges.GETbooks(form.getFieldValue());
        setvisible(false)
    }
    return (
        <div className={classNames(styles.knowledge)}>
            <div className={styles.knowledge_serach}>
                <Form
                    form={form}
                    onFinish={submit}
                    className={(styles.comment_froms)}
                >
                    <Form.Item
                        name="title"
                        label="名称"
                        className={(styles.comment_inputs)}
                    >
                        <Input type="text" placeholder=" 请输入知识库名称" />
                    </Form.Item>
                    <Form.Item
                        name="isCommentable"
                        label="状态"
                        className={(styles.comment_inputs)}
                    >
                        <Select>
                            <Select.Option value="true">已发布</Select.Option>
                            <Select.Option value="false">草稿</Select.Option>
                        </Select>
                    </Form.Item>
                    <Button htmlType="submit" className={(styles.comment_buttons)}>搜索</Button>&emsp;
                    <Button htmlType="reset" className={(styles.comment_buttons)}>重置</Button>
                </Form>
            </div>
            <div className={styles.knowledge_imgss}>
                <Button type="primary" onClick={showDrawer} className={styles.knowledge_buttons}>
                    + 新建
                </Button>
                <div className={styles.knowledge_imgs}>
                    {
                        store.Knowledges.knowledgelist.map(item => {
                            return <Card
                                className={styles.knowledge_imgdiv}
                                key={item.id}
                                hoverable
                                style={{ width: 220, height: 230, marginLeft: "21.5px" }}
                                cover={<img alt="图片出错" src={item.cover} />}
                            >
                                <Meta title={item.title} />
                                <Meta title={item.summary} />
                                <div style={{ paddingLeft: "10px", marginTop: "25px", border: "solid 1px #ccc" }}>
                                    <span>编写</span>&nbsp; | &nbsp;
                                    <Tooltip title={item.status === "draft" ? "发布上线" : "设为草稿"}>
                                        <span onClick={() => { getchangekeonw(item.id) }}>{item.status === "draft" ? <span>上线</span> : <span>草稿</span>}</span>&nbsp; | &nbsp;
                                    </Tooltip>
                                    <span onClick={() => { showDrawer(item) }}>
                                        <span>设置</span>&nbsp; | &nbsp;
                                    </span>
                                    <Popconfirm
                                        title="确定要删除？"
                                        onConfirm={() => confirm(item.id)}
                                        onCancel={cancel}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <span>删除</span>
                                    </Popconfirm>
                                </div>
                            </Card>
                        })
                    }
                </div>
            </div>
            <Drawer
                title="新建知识库"
                placement="right"
                closable={false}
                onClose={onClose}
                visible={visible}
                width={500}
            >
                <div>
                    <div className={styles.knowledge_views}>
                        <Form
                            form={form}
                            onFinish={submit1}
                        >
                            <Form.Item
                                name="title"
                                label="名称"
                            >
                                <Input type="text" />
                            </Form.Item>
                            <Form.Item
                                name="summary"
                                label="描述"
                            >
                                <Input type="text" />
                            </Form.Item>
                            描述  <Switch defaultChecked onChange={() => onChange(divlist.createAt)} />
                            <br /> <br />
                            封面  <Dragger {...props}>
                                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                <p className="ant-upload-hint">
                                    <img src={divlist.cover} alt="" />
                                </p>
                            </Dragger>

                            <Input type="text" />
                            <Button onClick={showDrawers}>
                                上传文件
                            </Button>
                            <div className={styles.knowledge_create}>
                                {divlist.id ? <Button htmlType="submit" onClick={() => {
                                    getsubmitid(divlist.id)
                                }}>更新</Button>
                                    : <Button htmlType="submit" onClick={getcreact}>创建</Button>
                                }
                                <Button onClick={() => setvisible(false)}>取消</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </Drawer>
            <Drawer
                title="文件选择"
                placement="right"
                closable={false}
                width={600}
                onClose={onCloses}
                visible={visibles}
            >
                <div className={styles.files}>
                    {
                        store.Knowledges.filelist.map(item => {
                            return <dl key={item.id} className={styles.files_header}>
                                <dt><ImageView><img src={item.url} alt="" /></ImageView></dt>
                                <dd onClick={() => getclickid(item.id)}>{item.originalname}</dd>
                            </dl>
                        })
                    }
                </div>
            </Drawer>
            <div className={styles.knowledge_footer}>
                <span>
                    共{store.Knowledges.count}条&emsp;
                </span>
                <Pagination
                    showSizeChanger
                    onShowSizeChange={onShowSizeChange}
                    defaultCurrent={1}
                    total={store.Knowledges.knowledgelist.length}
                    onChange={(current, pageSize) => changepage(current, pageSize)}
                />
            </div>
        </div >
    )
}
export default observer(Knowledge)
