import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import { Upload, Icon, message } from 'antd';

const { Dragger } = Upload;

const Poster: React.FC = () => {
    const props = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    return (
        <div className={classNames(styles.poster)}>
            <Dragger {...props}>
                <p className="ant-upload-drag-icon">
                    <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Click or drag file to this area to upload</p>
                <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                    band files
                </p>
            </Dragger>
            <div className={classNames(styles.posterheader)}>
                <div className={classNames(styles.left)}>
                    <span>文件名称:</span>&ensp;<input type="text" placeholder="请输入文件名称" /> 
                </div>
                <div className={classNames(styles.right)}>
                    <button>搜 索</button>
                    <button>重 置</button>
                </div>
            </div>
            <div className={classNames(styles.posterfoolter)}>
                
            </div>
        </div>
    )
}
export default Poster
