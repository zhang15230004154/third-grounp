import { useEffect, useRef, useState } from 'react';
import io, { Socket } from 'socket.io-client';

interface IMessage {
    title?: string;
    message: string;
}
const Chat = () => {
    const [username, setUsername] = useState('');
    const [content, setContent] = useState('');
    const [message, setMessage] = useState<IMessage[]>([]);
    const [showDialog, setShowDialog] = useState(true);
    const instance = useRef<Socket>();
    useEffect(() => {
        // 建立连接
        instance.current = io('ws://127.0.0.1:3000');
        instance.current.on('login', (data) => {
            setMessage(message => [...message, {
                message: `Welcome to Socket.IO Chat –
            there's ${data.numUsers} participant`
            }]);
        });


        instance.current.on('new message', (data) => {

            setMessage(message => [...message, {
                title: data.username,
                message: data.message,
            }]);
        });


        instance.current.on('user joined', (data) => {

        });


        instance.current.on('user left', (data) => {

        });


        instance.current.on('typing', (data) => {

        });


        instance.current.on('stop typing', (data) => {

        });

        instance.current.on('disconnect', () => {

        });

        instance.current.on('reconnect', () => {

        });

        instance.current.on('reconnect_error', () => {

        });
    }, []);

    // 用户加入聊天室
    function joinChatRoom(e: React.KeyboardEvent) {
        if (e.keyCode === 13 && username) {
            setShowDialog(false);
            instance.current!.emit('add user', username);
        }
    }

    // 用户发送消息
    function sendMessage(e: React.KeyboardEvent) {
        if (e.keyCode === 13 && content) {
            setMessage(message => [
                ...message, {
                    title: username,
                    message: content
                }
            ])
            instance.current!.emit('new message', content);
            setContent('');
        }
    }
    const inp = (<input
    type="text" value={username}
    onChange={e => setUsername(e.target.value)}
    onKeyDown={joinChatRoom}
    style={{width:"150px",height:"30px",border:"1px solid #cccc"}}
    />)
    return <div>
        <p>聊天页面</p>
        {showDialog ? <div>
            <h1>你叫啥?</h1>
            <input
                type="text" value={username}
                onChange={e => setUsername(e.target.value)}
                onKeyDown={joinChatRoom}
                style={{width:"150px",height:"30px",border:"1px solid #cccc"}}
            />
            {/* <div dangerouslySetInnerHTML={{__html:inp}}>{inp}</div> */}
        </div> : <div>
            <div style={{width:'100%',height:'500px'}}>{
                message.map(item => {
                    return <p>
                        {item.title && <span>{item.title}:</span>}
                        <span style={{background:"#ccc"}}>{item.message}</span>
                    </p>
                })
            }</div>
            <input
                placeholder="输入内容"
                type="text" value={content}
                onChange={e => setContent(e.target.value)}
                onKeyDown={sendMessage}
                style={{width:"150px",height:"30px",border:"1px solid #cccc"}}
            />
        </div>}
    </div>
}

export default Chat
