import React, { useEffect, useState } from "react"
import styles from "./index.less"
import classNames from "classnames"
import useStore from "@/context/useStore"
import { observer } from 'mobx-react'
import { Form, Input, Button, Popconfirm, message } from 'antd'
import { Idata } from "@/types"

const { useForm } = Form;
const Classify: React.FC = () => {
    const [form] = useForm()
    const store = useStore();
    const [flag, setFlag] = useState(false);
    const [item,getItems]=useState({})
    useEffect(() => {
        store.catecory.getCategoryList()
    }, [])
    console.log(store.catecory.categoryList, 'aasasas')
    function submit(values: Idata) {
        console.log(values, 'valuevalue')
        let token = localStorage.token;
        store.catecory.addCategory(values, token)
    }
    function dels() {
        store.catecory.deleteCategory(item.id)
    }
    function getItem(values: Idata) {
        getItems(values)
        setFlag(true)
        form.setFieldsValue({
            label: values.label,
            value: values.value
        })
    }
    // function confirm(id:any){
    //     store.catecory.deleteCategory(id);
    //     message.warning('删除成功');
    //     store.catecory.getCategoryList()
    // }
    // function cancel(){

    // }
    function upDate(){
        let res=form.getFieldsValue();
        store.catecory.updateCategory(item.id,res);
        setFlag(false)
        form.setFieldsValue({
            label:'',
            value:''
        })
    }
    return (
        <div className={classNames(styles.classify)}>
            {
                flag ? (
                    <div className={classNames(styles.search)}>
                        <h2 style={{ padding: '10px', fontSize: '20px', color: 'rgb(50, 65, 65)' }}>添加标签</h2>
                        <Form form={form} onFinish={submit}>
                            <Form.Item name='label'>
                                <Input type='text' placeholder='请输入标签名' />
                            </Form.Item>
                            <Form.Item name='value'>
                                <Input type='text' placeholder='请输入标签' />
                            </Form.Item>

                            <div className={classNames(styles.sou)} style={{ marginTop: 40 }}>
                                <Button style={{ marginLeft: '10px' }} onClick={()=>upDate()} htmlType='submit' type='primary'> 更新</Button>
                                <Button style={{ marginLeft: '20px' }} onClick={() => setFlag(false)} htmlType='submit' type='default'>返回添加</Button>
                                    <Button style={{ marginLeft: '30px', color: 'red' }} type='default' onClick={()=>dels()}>删除</Button>
                               

                            </div>
                        </Form>
                    </div>
                ) : (
                    <div className={classNames(styles.search)}>
                        <h2 style={{ padding: '10px', fontSize: '20px', color: 'rgb(50, 65, 65)' }}>添加标签</h2>
                        <Form form={form} onFinish={submit}>
                            <Form.Item name='label'>
                                <Input type='text' placeholder='请输入标签名' />
                            </Form.Item>
                            <Form.Item name='value'>
                                <Input type='text' placeholder='请输入标签' />
                            </Form.Item>

                            <div className={classNames(styles.sou)} style={{ marginTop: 40 }}>
                                <Button htmlType='submit' type='primary'
                                >
                                    保存
                                </Button>

                            </div>
                        </Form>
                    </div>
                )
            }

            <div className={classNames(styles.article)}>
                <h2 style={{ padding: '10px', fontSize: '20px', color: 'rgb(50, 65, 65)' }}>所有标签</h2>
                <br />
                {
                    store.catecory.categoryList && store.catecory.categoryList.map((item, index) => {
                        return <Button onClick={() => getItem(item)} style={{ marginLeft: '10px', marginTop: '10px' }} key={index}>
                            {item.value}
                        </Button>
                    })
                }
            </div>
        </div>
    )
}
export default observer(Classify)