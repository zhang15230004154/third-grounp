import React, { Key, useEffect } from "react"
import styles from "./index.less"
import classNames from "classnames"
import useStore from "@/context/useStore"
import { useState } from "react"
import { Button, Input, Select, Table, Badge, Pagination, Tag, Form, Popconfirm, message } from 'antd'
import { observer } from "mobx-react-lite"
import { IComputedValue } from "mobx"
import { TypeAllariticle } from "@/types"
import { NavLink } from "@umijs/runtime"
const { Option } = Select;
interface InnerHTML{
  innerHTML:string
}
interface IForm {
  [key: string]: string | boolean  //索引类型
}
const { useForm } = Form

const Articleall: React.FC = () => {
  const store = useStore()
  const [page, setPage] = useState(1)  //发起请求
  const [pageSize, setPageSize] = useState(8)

  const [form] = useForm() //通过form拿到实例
  const [params, setParams] = useState<IForm>({})
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])

  function submit() {
    let values = form.getFieldsValue()
    let params: IForm = {}
    for (let key in values) {
      values[key] && (params[key] = values[key])
    }
    setParams(params)
    console.log(values)
  }

  useEffect(() => {
    store.allarticle.allAriticleList(page, pageSize, params)
  }, [page, pageSize, params])  //页面切换再发起请求
  console.log(store.allarticle.article)
  console.log(store.allarticle.articleNum)

  // function handleChange(value: any) {
  //   console.log(`selected ${value}`);
  // }
  function chang(id:string,e:any){
    if(e.target.innerHTML==='首焦推荐'){
      store.allarticle.updatePublish([id] as string[], { isRecommended: false })
    }else{
      store.allarticle.updatePublish([id] as string[], { isRecommended: true })
    }
    
  }
  // store.allarticle.updatePublish([row.id] as string[], { isRecommended: !true })
  const columns = [
    {
      title: '主题',
      dataIndex: 'title',
      //   render: (text: string) => <p  style={{fontSize:'15px'}}></p>,
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text: string) => {
        return text === 'publish' ? <Badge status="success" text="已发布" /> : <Badge status="warning" text="草稿" />
      }
    },
    {
      title: '分类',
      dataIndex: 'category',
      render: (text: any) => {
        return <span>{text && <Tag color={'#2db7f5'}>{text.label}</Tag>}</span>
      }
    },
    {
      title: '标签',
      dataIndex: 'tags',
      render: (text: {
        id: string,
        label: string,
        value: string,
        createAt: string,
        updateAt: string
      }[]) => {
        return <span>{text.length > 0 ? text.map(item => <Tag style={{ marginRight: '5px' }} color={'#' + Math.random().toString(16).substr(2, 6).toUpperCase()}>{item.label}</Tag>) : null}</span>
      }
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      render: (text: number) => <p className={classNames(styles.read)}>{text}</p>
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      render: (text: number) => <p className={classNames(styles.like)}>{text}</p>
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
    },
    {
      title: 'Action',
      render: (row: TypeAllariticle) => {
        return <p style={{ display: "flex" }}>
          <a style={{ fontSize: '15px', color: '#0188fb', marginLeft: '13px' }}>编辑</a>
          <a style={{ fontSize: '15px', color: '#0188fb', marginLeft: '13px' }}
            onClick={(e) =>chang(row.id,e)}
          >{row.isRecommended === true ? '首焦推荐' : '撤销首焦'}</a>
          <a style={{ fontSize: '15px', color: '#0188fb', marginLeft: '13px' }}>查看访问</a>
          <a style={{ fontSize: '15px', color: '#0188fb', marginLeft: '13px' }} onClick={()=>deleteInner(row.id)}>删除</a>
        </p>

      }
    },
  ];

  function onShowSizeChange(page: number, pageSize: number) {
    setPage(page)
    setPageSize(pageSize)
  }
  function onchange(value: number) {
    setPage(value)
  }

  //监听属性
  function onSelectChange(selectedRowKeys: Key[], items: TypeAllariticle[]) {
    //  console.log('arguments',arguments)
    setSelectedRowKeys(selectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  }

  function confirm() {
    store.allarticle.deletePublish(selectedRowKeys as string[])
    message.success('删除成功');
    store.allarticle.allAriticleList(page, pageSize, params)
  }

  function deleteInner(id:string){
    store.allarticle.deletePublish([id] as string[])
    message.success('删除成功');
    store.allarticle.allAriticleList(page, pageSize, params)
  }

  function cancel() {
  }

  return (

    <div className={classNames(styles.Allarticle)}>
      <div className={classNames(styles.search)}>
        <Form form={form} onFinish={submit}>
          <Form.Item
            name='title'
            label="标题"
          >
            <Input type="text" placeholder="请输入文章标题" />

          </Form.Item >

          <Form.Item
            name="status"
            label="状态"
          >
            <Select>
              <Select.Option value="publish">已发布</Select.Option>
              <Select.Option value="draft">草稿</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            name="category"
            label="分类"
          >
            <Select>
              <Select.Option value="category.label">测试</Select.Option>
              <Select.Option value="category.value">阿松大</Select.Option>
            </Select>
          </Form.Item>
          <Button htmlType="submit">搜索</Button>
          <Button htmlType="reset">重置</Button>
        </Form>
      </div>

      <div className={classNames(styles.box)}>
        <div className={classNames(styles.new)}>
          {selectedRowKeys.length > 0 && <section>
            <Button onClick={() => store.allarticle.updatePublish(selectedRowKeys as string[], { status: "publish" })}>发布</Button>
            <Button onClick={() => store.allarticle.updatePublish(selectedRowKeys as string[], { status: "draft" })}>草稿</Button>
            <Button onClick={() => store.allarticle.updatePublish(selectedRowKeys as string[], { isRecommended: true })}>首焦推荐</Button>
            <Button onClick={() => store.allarticle.updatePublish(selectedRowKeys as string[], { isRecommended: false })}>撤销首焦</Button>

            <Popconfirm
              title="确认删除?"
              onConfirm={confirm}
              onCancel={cancel}
              okText="确认"
              cancelText="取消"
            >
              <Button>删除</Button>
            </Popconfirm>

          </section>}
          <NavLink to="/main/editor" type='primary' className={classNames(styles.aa)}> + 新建</NavLink>
  
        </div>

        <Table rowSelection={rowSelection} columns={columns} dataSource={store.allarticle.article} rowKey="id" pagination={false} className={classNames(styles.table)} />

        <div className={classNames(styles.total)}>
          <span>共{store.allarticle.articleNum}条</span>
          <span><Pagination
            showSizeChanger
            onShowSizeChange={onShowSizeChange}
            onChange={onchange}
            pageSizeOptions={["8", "12", "24"]}
            total={store.allarticle.articleNum} />
          </span>
        </div>

      </div>
    </div>
  )
}
export default observer(Articleall)


