import React, { useEffect } from "react"
import styles from "./index.less"
import classNames from "classnames"
import useStore from "@/context/useStore"
import { observer } from 'mobx-react'
import { Form, Input, Button,Popconfirm } from 'antd'
import { Idata } from "@/types"


const { useForm } = Form;
const Label: React.FC = () => {
    const [form] = useForm()
    const store = useStore();
    useEffect(() => {
        store.tag.getTagList()
    }, [])
    //   console.log(store.catecory.categoryList,'aasasas')
    function submit(values: Idata) {
        let token = localStorage.token;
        console.log(values, 'valuevalue')
        store.tag.addTags(values, token);
        form.setFieldsValue({
            label: '',
            value: ''

        })
    }
   function dels(id:string){

   }
    return (
        <div className={classNames(styles.classify)}>
            <div className={classNames(styles.search)}>
                <h2 style={{ padding: '10px', fontSize: '20px', color: 'rgb(50, 65, 65)' }}>添加标签</h2>
                <Form form={form} onFinish={submit}>
                    <Form.Item name='label'>
                        <Input type='text' placeholder='请输入搜索类型' />
                    </Form.Item>
                    <Form.Item name='value'>
                        <Input type='text' placeholder='请输入搜索词' />
                    </Form.Item>

                    <div className={classNames(styles.sou)} style={{ marginTop: 40 }}>
                        <Button htmlType='submit' type='primary'
                        >
                            保存
                            
                        </Button>
                       {/* <Button type='primary'>更新</Button>
                       <Button type='default'>返回添加</Button>
                       <Button type='default' style={{color:'red'}}>删除</Button> */}
                    </div>
                </Form>
            </div>
            <div className={classNames(styles.article)}>
                <h2 style={{ padding: '10px', fontSize: '20px', color: 'rgb(50, 65, 65)' }}>所有标签</h2>
                <br />
                {
                    store.tag.tagList && store.tag.tagList.map((item, index) => {
                        return <span style={{ padding: '0px 10px', height: '30px', display: 'inline-block', border: 'solid #ccc 1px', margin: '10px', textAlign: 'center' }} key={index}>
                            {item.value}
                        </span>
                    })
                }
            </div>
        </div>
    )
}
export default observer(Label)