import React, { useState, useEffect } from "react"
import useStore from "@/context/useStore"
import { observer } from "mobx-react-lite"
import styles from "./index.less"
import classNames from "classnames"
import { Form, Row, Col, Input, Button, Select, Space, Badge, Popconfirm, Modal, Table, Divider, Tag } from 'antd';
import Icon, {
    ReloadOutlined
} from '@ant-design/icons';
import { IRouteComponentProps } from "umi"
const { Option } = Select;
const Pageset: React.FC<IRouteComponentProps> = (props) => {
    const store = useStore()//仓库
    const [form] = Form.useForm();
    const [page, setPage] = useState(1)//当前页
    const [pageSize, setPageSize] = useState(5)//每页数量
    const [visible, setVisible] = useState(false);//上线、下线、删除按钮显示隐藏
    const [isModalVisible, setIsModalVisible] = useState(false); //查看访问弹窗
    const [selectedRowKeys, setSelectedRowKeys]: any[] = useState([])//全选反选的id数组 
    //theader-------------------
    const columns = [
        {
            title: '名字',
            dataIndex: 'name',
            render: (text: string) => <a>{text}</a>,
            key: "name"
        },
        {
            title: '路径',
            dataIndex: 'path',
            key: "title"

        },
        {
            title: '顺序',
            dataIndex: 'order',
            key: "order"
        },
        {
            title: '阅读量',
            dataIndex: 'views',
            key: "view"
        },
        {
            title: '状态',
            dataIndex: "status",
            render: (row: string) => {
                return row == "publish" ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />
            },

            key: "status"
        },
        {
            title: ' 发布时间',
            dataIndex: 'publishAt',
            key: "publishAt"

        },
        {
            title: ' 操作',
            key: 'action',
            render: (text: any, record: any) => (
                <Space size="middle">
                    <a>编辑</a>
                    <a onClick={() => getTabUpdata(text)}>{text.status == "publish" ? "下线" : "发布"}</a>
                    <a onClick={getView}>查看访问</a>
                    <Popconfirm title="确定要删除吗？" okText="要" cancelText="不要" onConfirm={() => delConfirm(text)}>
                        <a href="#">删除</a>
                    </Popconfirm>
                </Space>
            ),
        }

    ];
    //theader----------------------

    useEffect(() => { //获取pageList
        store.Pageset.getPageList(page, pageSize, '')
    }, [page, pageSize])


    const rowSelection = { //table 多选事件
        onChange: (ids: React.Key[], selectedRows: any[]) => {
            if (ids.length > 0 || selectedRows.length > 0) {
                setVisible(true)
            } else {
                setVisible(false)
            }
            setSelectedRowKeys(ids)
        },
    };
    //分页事件-----
    function pageChange(value: { current?: number, pageSize?: number, total?: number }) {
        setPage(value.current ? value.current : 1)
    }
    function getTabUpdata(text: any) {//上下线事件
        let { id, status } = text
        if (status == "publish") {
            let value = { status: "draft" }
            store.Pageset.setTabUpdata(id, value)
        } else {
            let value = { status: "publish" }
            store.Pageset.setTabUpdata(id, value)
        }
    }
    function delConfirm(text: any) { //删除文章
        let { id } = text
        store.Pageset.setDelPage(id)
    }
    function setAllUpdata() { //发布全部
        selectedRowKeys.map((item: string) => {
            getTabUpdata({ id: item, status: "draft" })
        })
    }
    function setAlldraft() { //下线全部
        selectedRowKeys.map((item: string) => {
            getTabUpdata({ id: item, status: "publish" })
        })
    }
    function onFinish(value: any) { //搜索事件
        store.Pageset.getPageList(page, pageSize, value)
    };
    function getView() { //查看访问弹出层关闭
        setIsModalVisible(!isModalVisible)
    }
    function tableReload(){ //页面重新加载
        store.Pageset.getPageList(page, pageSize, '')
    }
    function newPage(){// 新建页面
        props.history.push('/main/pageeditor')
    }
    return (

        <div className={classNames(styles.pageset)}>
            <div className={classNames(styles.search)}>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >

                    <Row gutter={24}>
                        <Col span={7}>
                            <Form.Item
                                name="name"
                                label={`名称`}
                            >
                                <Input placeholder="请输入要搜索的名称" />
                            </Form.Item>
                        </Col>
                        <Col span={7}>
                            <Form.Item
                                name="path"
                                label={`路径`}
                            >
                                <Input placeholder="请输入要搜索的路径.." />
                            </Form.Item>
                        </Col>
                        <Col span={4}>
                            <Form.Item
                                name="status"
                                label="状态"
                            >
                                <Select
                                    placeholder=""
                                // allowClear
                                >
                                    <Option value="publish">已发布</Option>
                                    <Option value="draft">草稿</Option>
                                </Select>
                            </Form.Item>
                        </Col>

                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit">
                                搜索
                            </Button>
                            <Button
                                style={{ margin: '0 8px' }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                清空
                           </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
            <div className={classNames(styles.table)}>
                <div className={styles.add}>
                <Button type="primary" size={"large"} onClick={newPage}>
                     &nbsp;＋新建&nbsp; 
                </Button>
                <ReloadOutlined onClick={tableReload}/>
                </div>
                <Divider />
                <div className={classNames(styles.tag)}>
                    <Tag
                        visible={visible}
                        color="#ffff"
                    >
                        <Button size={'large'} onClick={setAllUpdata}>
                            发布
                        </Button >
                        <Button size={'large'} style={{ margin: "10px" }} onClick={setAlldraft}>
                            下线
                        </Button>
                        <Button size={'large'} danger>
                            删除
                        </Button>

                    </Tag>
                </div>
                <Table
                    rowSelection={
                        rowSelection
                    }
                    columns={columns}
                    dataSource={store.Pageset.pageList}
                    rowKey="id"
                    pagination={{ current: page, pageSize: pageSize, total: store.Pageset.pageCount }}
                    onChange={pageChange}
                />
            </div>

            <Modal title="访问统计" visible={isModalVisible} onCancel={getView}>
            </Modal>

        </div>
    )
}
export default observer(Pageset)
