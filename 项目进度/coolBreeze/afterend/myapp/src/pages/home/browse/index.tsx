import React, { useState,Key } from "react"
import styles from "./index.less"
import classNames from "classnames"
import { browseType } from "@/types"
// import { IRootState } from "@/types/index"
import { useSelector } from "react-redux"
import { useEffect } from "react"
import { useDispatch } from "@/.umi/plugin-dva/exports"
import useStore from '@/context/useStore';
import { Input, Button, Table, Form,Popconfirm, message  } from 'antd';
import { render } from "react-dom"
import { observer } from "mobx-react-lite"//实时更新 
const { useForm } = Form
const Browse: React.FC = () => {
  const store = useStore()
  const [form] = useForm()
  const dispatch = useDispatch()
  const [page, setPage] = useState(1);
  const [ipd, ipds] = useState("")
  const [google, googles] = useState("")
  const [engine,engines]=useState("")
  const [check,checks]=useState(false)
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
  // const browselist = useSelector((state: browseType) => state.browse) 
  useEffect(() => {
    store.browse.getbrowse(page)
  }, [page]) 
  function confirm(e: any ) {
    console.log(e);
    // store.browse.getbrowsdel(row.id) 
    store.browse.deletebrowse(selectedRowKeys as string[]) 
    message.success('Click on Yes');
  }
  
  function cancel(e: any) {
    console.log(e);
    
    message.error('Click on No');
  }
  
  function delConfirm(row: any  ) {  
    const {id}=row
    store.browse.getbrowsdel(id) 
    // store.browse.deletebrowse(selectedRowKeys as string[])
    store.browse.getbrowse()
    // store.browse.browseArr
    message.success('Click on Yes');
  }
  
  function cancels(e: any) {
    console.log(e);
    message.error('Click on No');
  }
  const columns: any = [
    {
      title: 'URL',
      width: 200,
      height: 54,
      // dataIndex: 'url',
      render: (row: any) => {
        // {row.url}
        return <a href={row.url} style={{ color: "#0188fb", cursor: "pointer" }}>{row.url}</a> 
      },
      key: 'url',
      fixed: 'left',
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      key: '1',
      width: 120,
    },
    {
      title: '浏览器',
      dataIndex: 'browser',
      key: '2',
      width: 120,
    },
    {
      title: '内核',
      dataIndex: 'engine',
      key: '3',
      width: 120,
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      key: '4',
      width: 120,
    },
    {
      title: '地址',
      dataIndex: 'address',
      key: '5',
      width: 120,
    },
    {
      title: '访问量',
      dataIndex: 'count',
      key: '6',
      width: 120,

    },
    {
      title: '访问时间',
      dataIndex: 'createAt',
      key: '7',
      width: 120,
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 120,
      render: (row: any) => {
        return <Popconfirm
          title="Are you sure delete this task?"
          onConfirm={()=>delConfirm(row) }
          onCancel={cancels}
          okText="Yes"
          cancelText="No"  
          
         >
          <a href="" style={{ color: "#0188fb", cursor: "pointer" }}>删除</a>  
           </Popconfirm> 
           
      }
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);   
      setSelectedRowKeys(selectedRowKeys);
      if(selectedRows.length){
        checks(true)
      }else{
        checks(false)
      }
    },
    onSelect: (record: any, selected: any, selectedRows: any) => {
      console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected: any, selectedRows: any, changeRows: any) => {
      console.log(selected, selectedRows, changeRows);
    },
  };
  function seatchs() { 
      // store.browse.getGoogle(google)
      console.log(engine);
      
      store.browse.getIP(ipd,google,engine)    
    // const iop = store.browse.browseArr.filter(itme => {
    //   return itme.ip.includes(ipd)
    // })
    // store.browse.browseArr = iop
  } 
  return (
    <div className={classNames(styles.browse)}>
      <div className={classNames(styles.browseheader)}>
        <div className={classNames(styles.headerchild)}>
          <div>
            <span>IP:</span> <Input placeholder="请输入IP地址" value={ipd} onChange={
              (e) => {
                ipds(e.target.value)
              }
            } />
          </div>
          <div>
            <span>UA:</span> <Input placeholder="请输入User Agent" />
          </div>
          <div>
            <span>URL:</span> <Input placeholder="请输入URL"/>
          </div>
          <div>
            <span>地址:</span> <Input placeholder="请输入地址" />
          </div>
          <div>
            <span>浏览器:</span> <Input placeholder="请输入浏览器" value={google} onChange={
              (e) => {
                googles(e.target.value)
              }
            }  />
          </div>
          <div>
            <span>内核:</span> <Input placeholder="请输入内核" value={engine} onChange={
              (e)=>{
                engines(e.target.value)
              }
            } />
          </div>
          <div>
            <span>OS:</span> <Input placeholder="请输入操作系统" />
          </div>
          <div>
            <span>设备:</span> <Input placeholder="请输入设备" />
          </div>
        </div>
        <div className={classNames(styles.divs)}>
          <Button type="primary" onClick={
            seatchs
          }>搜 索</Button>
          <Button type="primary">重 置</Button>
        </div>
      </div>
      <div className={classNames(styles.browsebody)}>
        <div className={classNames(styles.browsebodys)}>
         {check?<Popconfirm
          title="Are you sure delete this task?"
          onConfirm={confirm}
          onCancel={cancel}
          okText="Yes"
          cancelText="No" 
         >
          <Button>删除</Button> 
           </Popconfirm>:""}
        </div>
        共有{store.browse.browseArr.length}个<Table columns={columns} rowKey="id" style={{ height: "500" }}
          dataSource={store.browse.browseArr}
          scroll={{ x: 1500, }} rowSelection={rowSelection} /> 
      </div>
    </div>
  )
}
export default observer(Browse)

function e(e: any): void {
  throw new Error("Function not implemented.")
}
