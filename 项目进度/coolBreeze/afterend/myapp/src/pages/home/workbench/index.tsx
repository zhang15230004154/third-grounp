import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import Navigatio from "@/components/Navigatio"
import { useEffect } from "react"
import useStore from "@/context/useStore"
import NewArticle from "@/components/NewArticle"
import { useState } from "react"
import NewCommoend from "@/components/NewCommond"
import Bar from "@/components/Newborad"

const Workbench: React.FC = () => {

   let store = useStore()
   let [page, setPage] = useState(1)

   useEffect(() => {
      store.Workbench.workbenchs(1, 6)
   }, [])

   // console.log(store.Workbench.workCommend);
   // console.log(store.Workbench.workCommendNum)

   return (
      <div>
         <div className={classNames(styles.workbench)}>
            <main>
               <div className={classNames(styles.antd)}>
                  <p>面板导航</p>
                  <Bar></Bar>
               </div>
               <Navigatio></Navigatio>
               <NewArticle></NewArticle>
               <NewCommoend></NewCommoend>
            </main>
         </div>
      </div>
   )
}
export default Workbench
