//最外层
import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import {Redirect} from 'umi'
const Warp:React.FC=(props)=>{
   return(
       <div className={classNames(styles.warp)}>
             {props.children}
             {/* <Redirect to='/home/workbench'></Redirect> */}
       </div>
   )
}
export default Warp