import useStore from '@/context/useStore';
import { ICommentItem } from '@/types';
import { Form, Input, Select, Button, Table, Badge, Popover, Pagination, Modal } from 'antd'
import { Key, useEffect, useState } from 'react';
import { observer } from 'mobx-react'
import styles from "../../pages/home/comment/index.less"

interface IForm {
    [key: string]: string | boolean
}
const { useForm } = Form;
const Comment: React.FC = () => {
    const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [params, setParams] = useState<IForm>({})
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
    const [visible, setvisible] = useState(false)
    useEffect(() => {
        store.comment.GETcomment(page, params);
    }, [page, params])

    // 搜索按钮
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {};
        console.log(params)
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }

    const columns: any = [
        {
            title: '状态',
            width: 100,
            render: (text: string) => text ? <a style={{ color: "green" }}> 通过</a> : <a style={{ color: "red" }}>未通过</a>,
            key: '8',
            fixed: 'left',
            dataIndex: "pass"
        },
        { title: '称呼', dataIndex: 'name', key: '1' },
        { title: '联系方式', dataIndex: 'email', key: '2' },
        {
            title: '原始内容',
            render: (item: ICommentItem) => {
                return <Popover content={item.content} title="评论详情-原始内容">
                    <Button type="primary">查看内容</Button>
                </Popover>
            }
        }, {
            title: 'HTML内容',
            render: (item: ICommentItem) => {
                return <Popover content={<div dangerouslySetInnerHTML={{ __html: item.html }}></div>} title="评论详情-HTML内容">
                    <Button type="primary">查看内容</Button>
                </Popover>
            }
        }, {
            title: '管理文章',
            render: (item: ICommentItem) => {
                return <Popover content={<iframe src={'https://creation.shbwyz.com' + item.url}></iframe>} title="页面预览">
                    <Button type="primary">查看内容</Button>
                </Popover>
            }
        }, {
            title: '创建时间',
            dataIndex: 'createAt'
        }, {
            title: '父级评论',
            render: () => <a>无</a>
        },
        {
            title: '操作',
            key: 'operation',
            fixed: 'right',
            width: 260,
            render: (row: ICommentItem) => <p className={(styles.oper_p)}>
                <span onClick={() => store.comment.updateComment([row.id], { pass: true })}>通过</span><a>|</a>
                <span onClick={() => store.comment.updateComment([row.id], { pass: false })}>拒绝</span><a>|</a>
                <span> <Button type="primary" onClick={showModal} style={{ border: "none", background: "#fff" }}>
                    回复
                </Button></span><a>|</a>
                <span onClick={() => store.comment.deleteComment([row.id])}>删除</span>
            </p>
        }]
    function onSelectChange(selectedRowKeys: Key[], items: ICommentItem[]) {
        setSelectedRowKeys(selectedRowKeys);
    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }
    function onShowSizeChange(current: number, pageSize: any) {
        // setPage(current)
        setParams(pageSize);
        console.log(current, pageSize);
    }
    // 改变分页
    function changepage(current: number) {
        setPage(current)
        console.log(current)
    }
    // 对话框显示隐藏
    // 显示
    function showModal() {
        setvisible(true)
    };
    // 关闭
    function handleOk() {
        setvisible(false)
    };
    function handleCancel() {
        setvisible(false)
    };
    return <div className={(styles.comment)}>
        {/* 回复弹框 */}
        <Modal
            title="回复评论"
            visible={visible}
            onOk={handleOk}
            onCancel={handleCancel}
            cancelText="取消"
            okText="回复"
        >
            <Input style={{ height: 130, padding: "0 0 110px 10px" }} placeholder="支持Markdown" />
        </Modal>

        <div className={(styles.box_from)}>
            <Form
                form={form}
                onFinish={submit}
                className={(styles.comment_froms)}
            >
                <Form.Item
                    name="name"
                    label="称呼"
                    className={(styles.comment_inputs)}
                >
                    <Input type="text" placeholder=" 请输入称呼" />
                </Form.Item>
                <Form.Item
                    name="email"
                    label="邮箱"
                    className={(styles.comment_inputs)}
                >
                    <Input type="text" placeholder=" 请输入称呼" />
                </Form.Item>
                <Form.Item
                    name="pass"
                    label="状态"
                    className={(styles.comment_inputs)}
                >
                    <Select>
                        <Select.Option value="true">已通过</Select.Option>
                        <Select.Option value="false">未通过</Select.Option>
                    </Select>
                </Form.Item>
                <Button htmlType="submit" className={(styles.comment_buttons)}>搜索</Button>
                <Button htmlType="reset" className={(styles.comment_buttons)}>重置</Button>
            </Form>
        </div>
        {selectedRowKeys.length ? <section className={styles.comment_sections}>
            <Button onClick={() => store.comment.updateComment(selectedRowKeys as string[], { pass: true })} className={styles.comment_action_span}>通过</Button>
            <Button onClick={() => store.comment.updateComment(selectedRowKeys as string[], { pass: false })} className={styles.comment_action_span}>拒绝</Button>
            <Button onClick={() => store.comment.deleteComment(selectedRowKeys as string[])} className={styles.comment_action_span}>删除</Button>
        </section> : null}

        <div className={styles.comment_tables}>
            {/* pagination={{ pageSize: 5 }} */}
            <Table columns={columns} dataSource={store.comment.commentlist} scroll={{ x: 1300 }} rowKey="id" rowSelection={rowSelection} pagination={false} />
            <div className={styles.comment_paginations}>
                <span>共{store.comment.commentCount}条</span>
                <Pagination
                    showSizeChanger
                    onShowSizeChange={onShowSizeChange}
                    defaultCurrent={3}
                    total={store.comment.commentCount}
                    onChange={(current) => changepage(current)}
                />
            </div>
        </div>
    </div>
}

export default observer(Comment);

