/**
 * 柱状图
 */
import React ,{Component} from 'react';
import {Card} from 'antd'
import "./index.less"
import ReactEcharts from 'echarts-for-react';
class Bar extends Component{
    constructor(props: {} | Readonly<{}>){
        super(props)
        this.state = {
            sales:[120, 200, 150, 80, 70, 110,130],
            stores:[360, 290, 350, 110, 100, 120,170]
        }
    }

    /**
     * 柱状图的配置对象
     */
    getOption = (sales: any,stores: any) =>{
        return {
            title: {
                text: '每周用户访问指标'
            },
            tooltip: {},
            legend: {
                data:['评论数','访问量']
            },
            xAxis: {
                data: ["Mon"," Tue","Wed","Thu","Fri","Sat","Sun"]
            },
            yAxis: {},
            series: [{
                name: '评论数',
                type: 'bar',
                data: sales,
                color:"#c23531"
            },
            {
                name: '访问量',
                type: 'line',
                data: stores,
                color:"black"
            }]
        };
    }
    render(){
        const {sales,stores}:any = this.state;
        return(
            <div>
                <Card>
                    <ReactEcharts option={this.getOption(sales,stores)} />
                </Card>
            </div>
        )
    }
}
export default Bar;