import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import { NavLink } from "react-router-dom"
import useStore from "@/context/useStore"
import { useEffect } from "react"
import { Card } from 'antd';
const { Meta } = Card;
interface ID{
    id:string
}
const NewArticle: React.FC = (props) => {
    let store = useStore()
    useEffect(() => {
        store.Workbench.workbenchs(1, 6)
    }, [])
    // console.log( store.Workbench.workbench)
    // console.log( store.Workbench.workbenchnum)
    return <div className={classNames(styles.NewArticle)}>
        <div className={classNames(styles.div)}>
            <div className={classNames(styles.divs)}>最新文章</div>
            <div className={classNames(styles.diss)}>
                <NavLink
                    to="/home/article/allarticle"
                    style={{ marginLeft: "1050px", fontWeight: "bold" }}>
                    全部文章</NavLink>
            </div>
        </div>
        {
            store.Workbench.workbench && store.Workbench.workbench.map((item, index) => {
                return <div key={index} className={classNames(styles.word)}>
                  <NavLink to={`/article/editor/${item.id}`} className={classNames(styles.imgp)}>
                    <Card
                        hoverable
                        // style={{ width: 240 }}
                        cover={<img width='120' alt="文章封面" style={{marginTop:'20px',marginLeft:"20px",textAlign:'center'}} />}
                    >
                        <Meta title={item.title} style={{marginBottom:"20px",textAlign:'center'}}/>
                    </Card>
                    </NavLink>
                </div>
            })
        }
    </div>
}
export default NewArticle