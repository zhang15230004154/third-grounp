import {Menu} from 'antd'
import {Link} from 'umi'
import { Navtitle} from "@/utils/Navtitle";
const { SubMenu } = Menu;
const MyMenu:React.FC=()=>{
    return (
        <div>
         <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} >
                     {
                        Navtitle.map((item, index) => {
                           if (item.children) {
                              return <SubMenu
                                 key={index + 'a'}
                                 icon={<item.icon />}
                                 title='文章管理'
                                 style={{ fontSize: "14px" }}
                              >
                                 {
                                    item.children.map((item1, index1) => {
                                       return <Menu.Item style={{ height: '50px' }} icon={<item1.icon />} key={index1 + 'hh'}> <Link to={item1.to}>{item1.title}</Link></Menu.Item>
                                    })
                                 }
                              </SubMenu>

                           } else {
                              return <Menu.Item icon={<item.icon />} key={index} style={{ height: "50px", fontSize: "14px" }} ><Link to={item.to}>{item.title}</Link></Menu.Item>
                           }
                        })
                     }
                  </Menu>
        </div>
    )
}
export default MyMenu