import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import { NavLink } from "react-router-dom"
const Navigatio: React.FC = (props) => {
    return <div className={classNames(styles.antdCard)}>
        <p>快速导航</p>
        <div className={classNames(styles.antdBody)}>
            <div className={classNames(styles.antdBodys)}>
                <div className={classNames(styles.dis)}>
                    <NavLink to="/home/article/allarticle" style={{ fontWeight: 'bold' }}>文章管理</NavLink>
                </div>
                <div className={classNames(styles.dis)}>
                    <NavLink to="/home/comment" style={{ fontWeight: 'bold' }}>评论管理</NavLink>
                </div>
                <div className={classNames(styles.dis)}>
                    <NavLink to="/home/file" style={{ fontWeight: 'bold' }}>文件管理</NavLink>
                </div>
                <div className={classNames(styles.dis)}>
                    <NavLink to="/home/user" style={{ fontWeight: 'bold' }}>用户管理</NavLink>
                </div>
                <div className={classNames(styles.dis)}>
                    <NavLink to="/home/browse" style={{ fontWeight: 'bold' }}>访问管理</NavLink>
                </div>
                <div className={classNames(styles.dis)}>
                    <NavLink to="/home/setting" style={{ fontWeight: 'bold' }}>系统管理</NavLink>
                </div>
            </div>
        </div>
    </div>
}
export default Navigatio