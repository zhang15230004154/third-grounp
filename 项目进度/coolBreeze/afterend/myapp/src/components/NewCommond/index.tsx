import { useState } from "react"
import { message, Button, Popconfirm, Popover,Modal,Input } from 'antd';
import React from "react"
import styles from "./index.less"
import classNames from "classnames"
import { NavLink } from "umi"
import useStore from "@/context/useStore";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { makeHtml } from "@/utils/markdown";
import { MailObject } from "@/types";

const NewCommoend: React.FC = (props) => {
    let store = useStore()
    let [page, setPage] = useState(1)
    useEffect(() => {
        store.Workbench.workcommend(page)
    }, [page])

    //回复
    let [showreply, setShowreply] = useState(false)
    let [reply,SetReply]=useState('')

    //点击通过
    const warning = () => {

    };
    //点击取消
    function cancel() {

    }

    //更改状态通过
    function success(id: string, pass: boolean) {
        if (pass) {
            store.Workbench.updateCommend(id, pass)
            message.success('评论已通过');
        } else {

        }
    }
    //更改状态拒绝
    function reject(id: string, pass: boolean) {
        if (!pass) {
            store.Workbench.updateCommend(id, pass)
            message.success('评论已拒绝');
        } else {

        }
    }
    //点击回复
    function respose() {

    }

    // 点击删除
    function deletes(id: string) {
        store.Workbench.deleteCommend(id)
        console.log("11")
    }
    //回复
    const handleOk =() => {
    setShowreply(false)
    
      };
     //取消
      const handleCancel = () => {
        setShowreply(false)
      };



    return <div className={classNames(styles.commond)}>
        <div className={classNames(styles.div)}>
            <div className={classNames(styles.divs)}>最新评论</div>
            <div className={classNames(styles.diss)}>
                <NavLink
                    to="/home/comment"
                    style={{ marginLeft: "1050px", fontWeight: "bold" }}>
                    全部评论</NavLink>
            </div>
        </div>
        <div className={classNames(styles.table)}>
            <div className={classNames(styles.left)}>
                <div className={classNames(styles.center)}>
                    <div className={classNames(styles.container)}>
                        {
                            store.Workbench.workCommend && store.Workbench.workCommend.map((item, index) => {
                                return <ul key={index} className={classNames(styles.ul)}>
                                    <li className={classNames(styles.li)}>
                                        <span>{item.name}</span>&ensp;
                                        在
                                        {/* <a href="" className={classNames(styles.a)}></a>&ensp; */}
                                        <NavLink to={`/article/${item.id}`}>
                                            <Popover content={<h4 style={{ height: '200px', textAlign: "center", lineHeight: "200px" }}>404请求不到数据</h4>} title="页面预览">
                                                <Button type="primary" className={classNames(styles.button)} style={{ fontSize: '14px' }}>文章</Button>
                                            </Popover>
                                        </NavLink>
                                        评论
                                        <span>
                                            <Popover content={<h4 dangerouslySetInnerHTML={{ __html: item.html }}></h4>} title="评论详情-原始内容" >
                                                <Button type="primary" className={classNames(styles.button)} style={{ fontSize: '14px' }}>查看内容</Button>
                                            </Popover>
                                        </span>
                                        {
                                            item.pass ? <span className={classNames(styles.faces)} style={{ background: 'green' }}></span> : <span className={classNames(styles.faces)} style={{ background: '#faad14' }}></span>
                                        }&ensp;
                                        <span>{item.pass ? '通过' : '未通过'}</span>

                                        <span className={classNames(styles.spans)}>
                                            <a ><Button className={classNames(styles.pass)} onClick={() => {
                                                success(item.id, !item.pass)
                                                // console.log(item.id)
                                            }}>成功</Button></a>
                                            <a ><Button className={classNames(styles.pass)} onClick={() => {
                                                reject(item.id, !item.pass)
                                            }}>拒绝</Button></a>
                                            <a ><Button className={classNames(styles.pass)} onClick={()=>setShowreply(true)}>回复</Button></a>

                                            <Modal
                                                title="回复评论"
                                                visible={showreply}
                                                onOk={handleOk}
                                                onCancel={handleCancel}
                                                okText="回复"
                                                cancelText="取消"
                                                
                                            >
                                               <Input
                                               style={{height:'200px',lineHeight:'0px',textAlign:'center'}} 
                                               value={reply} 
                                               onChange={(e)=>{
                                                SetReply(e.target.value)
                                                  console.log(e.target.value)
                                               }} />
                                            </Modal>

                                            <Popconfirm
                                                title="确认删除这个评论吗?"
                                                onConfirm={() => {
                                                    deletes(item.id)
                                                }}
                                                onCancel={cancel}
                                                okText="确认"
                                                cancelText="取消"
                                            >
                                                <a href="#">
                                                    <Button className={classNames(styles.pass)}>删除</Button>
                                                </a>
                                            </Popconfirm>

                                        </span>
                                    </li>
                                </ul>
                            })
                        }
                    </div>
                </div>
            </div>

        </div>
    </div>
}
export default observer(NewCommoend)