import useStore from "@/context/useStore"
import { useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import styles from "./index.less"
import react from "react"
const Files: React.FC = () => {
    const [page1, setpage1] = useState(1);
    const store = useStore();
    useEffect(() => {
        store.Knowledges.GETfiles(page1)
    }, [page1])
    // 判断点击的元素
    function getclickid(id: string) {
        let items = store.Knowledges.filelist.filter(item => item.id == id);
    }
    return <div className={styles.files}>
        {
            store.Knowledges.filelist.map(item => {
                return <dl key={item.id} className={styles.files_header} onClick={() => getclickid(item.id)}>
                    <dt><img src={item.url} alt="" /></dt>
                    <dd>{item.originalname}</dd>
                </dl>
            })
        }
    </div>
}
export default observer(Files)