import { request } from "umi";
import {IReplyMail} from '@/types/modules/mail'

// 邮件管理
export function getMailList(page:number,params:{[key:string]:string|boolean},pageSize:number){
    return request(`/api/smtp?page=${page}&pageSize=${pageSize}`,{
        params
    })
}
export function deleteMailList(id:string){
    return request(`/api/smtp/${id}`,{
        method:'DELETE'
    })
}
// 回复邮件
export function replayMail(data:IReplyMail){
    return request('http://127.0.0.1:7001/mail',{
        method:'POST',
        data
    })
}
