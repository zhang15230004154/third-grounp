import { request } from "umi";

// 文件管理
export function getFileList(page:number,params:{[key:string]:string|boolean},pageSize:number){
    return request(`/api/file?page=${page}&pageSize=${pageSize}`,{params})
}
