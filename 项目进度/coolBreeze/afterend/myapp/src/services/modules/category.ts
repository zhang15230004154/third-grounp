import { Idata } from "@/types";
import { request } from "umi";

// 评论数据接口
export function getCategoryList() {
    return request(`/api/category`)
}

// 保存
export function addCategory(data:Idata,token:string){
   return request(`/api/category`,{
       method:'POST',
       data,
       headers:{'authorization':token}
   })
}
// 删除
export function deleteCategory(id:string){
    return request(`/api/category/${id}`,{
        method:'DELETE'
    })
}

//更新
export function updateCategory(id:string,data:Idata){
   return request(`/api/category/${id}`,{
       method:'PATCH',
       data
   })
}

