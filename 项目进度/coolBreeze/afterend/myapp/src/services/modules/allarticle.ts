import { request } from "umi";
export function allAriticleList(page:number,pageSize:number,params:{[key:string]:string|boolean}){
    return request(`/api/article?page=${page}&pageSize=${pageSize}`,{params})
}

//更改发布
export function updatePublish(id:string,data:{[key:string]:string|boolean}){
    return request(`/api/article/${id}`,{
        method:'PATCH',
        data
    })
}

//删除
export function deletePublish(id:string){
    return request(`/api/article/${id}`,{
        method:'DELETE'
    })
}