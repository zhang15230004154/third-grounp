import {request} from "umi"

export function getbrowse(page=1,pageSize=2000){ 
    console.log(123);
    return request(`/api/view?page=${page}&pageSize=${pageSize}`)
}

//删除
export function getbrowsdel(id:string){  
    
    return request(`/api/view/${id}`,{ 
        method:"DELETE"
    })
}
//批量删除
export function deletebrowse(id:string){
    return request(`/api/view/${id}`,{
        method:"DELETE"
    })
}
//搜索 ?
export function getIP(ip?:string,browser?:string,engine?:string,page=1,pageSize=2000){
    return request(`/api/view?page=${page}&pageSize=${pageSize}&ip=${ip}&browser=${browser}&engine=${engine}`,{
        method:"GET"
    })
}
//搜索浏览器
export function getGoogle(browser:string,page=1,pageSize=2000){
    return request(`/api/view?page=${page}&pageSize=${pageSize}&browser=${browser}`,{
        method:"GET"
    })
}
