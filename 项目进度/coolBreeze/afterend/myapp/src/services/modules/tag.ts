import { Idata } from "@/types";
import { request } from "umi";

// 评论数据接口
export function getTagList() {
    return request(`/api/tag`)
}
//保存
export function addLabel(data:Idata,token:string){
    return request(`/api/tag`,{
        method:'POST',
        data,
        headers:{'authorization':token}
    })
}
