import { request } from "umi";
// 搜索管理

export function getSearchList(page:number,params:{[key:string]:string|boolean},pageSize:number){
    return request(`/api/search?page=${page}&pageSize=${pageSize}`,{params})
}

// 删除评论
export function deleteSearchList(id:string){
    return request(`/api/search/${id}`,{
        method:'DELETE'
    })
}