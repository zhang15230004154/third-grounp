// import { IReplyMail } from "@/types";
import { IReplyMail } from "@/types";
import { request } from "umi";

export function workbenchs(page: number, pageSize: number) {
  return request("/api/article", {
    params: {
      page,
      pageSize
    }
  })
}

//获取评论
export function workcommend(page: number, pageSize = 4) {
  return request("/api/comment", {
    params: {
      page,
      pageSize,
    }
  })
}


//更改通过和拒绝的状态
export function updateCommend(id: string, pass: boolean) {
  return request(`/api/comment/${id}`, {
    data: {
      pass
    },
    method: "PATCH"
  })
}
// 删除评论
export function deleteCommend(id: string) {
  return request(`/api/comment/${id}`, {
    method: "DELETE"
  })
}

