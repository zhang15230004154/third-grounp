import { Ibooks } from "@/types"
import { request } from "umi"

// 知识小测
// export function GETKnowledge(page = 1, pageSize = 10, params: { [key: string]: string | boolean }) {
export function GETKnowledge(page = 1, pageSize = 10, params: any) {
    return request(`/api/knowledge?page=${page}&pageSize=${pageSize}`, { params })
}
// 删除
export function GETKnowledgedetal(id: string) {
    return request(`/api/knowledge/${id}`, {
        method: "DELETE"
    })
}
// 上传
export function GETKnowledgeshang(id: string,status:string) {
    return request(`/api/knowledge/${id}`, {
        method:"PATCH",
        data:{
            status
        }
    })
}
//最里面的 上传文件
export function GETfiles(page = 1, pageSize = 12) {
    return request(`/api/file?page=${page}&pageSize=${pageSize}`)
}
// 更新文件信息
export function GETfileslist(id: string, data: Ibooks) {
    return request(`/api/knowledge/${id}`, {
        method: "PATCH",
        data
    })
}
// 创建新的文件
export function GETbooks({ title, summary }: { title: string, summary: string }) {
    return request(`/api/knowledge/book`, {
        method: "POST",
        data: {
            title,
            summary
        }
    })
}