import {request} from "umi"

export function newPage(value:any){
   return request('/api/page',{
       method:"post",
       data:value
   })
}