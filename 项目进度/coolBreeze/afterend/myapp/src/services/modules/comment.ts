import { ICommentItem } from "@/types";
import { request } from "umi";

// 评论数据接口
export function GETcomment(page = 1, params: { [key: string]: string | boolean }, pageSize = 7) {
    return request(`/api/comment?page=${page}&pageSize=${pageSize}`, { params })
}

// 更改评论状态（通过或拒绝）
export function updateComment(id: string, data: { [key: string]: boolean } = {}) {
    return request(`/api/comment/${id}`, {
        method: 'PATCH',
        data
    })
}

// 删除评论
export function deleteComment(id: string) {
    return request(`/api/comment/${id}`, {
        method: 'DELETE'
    })
}