import { request } from "umi"

export function getList(page: number, pageSize: number, value: { name: string, path: string, status: string }) {
    if (value) {
        let { name, path, status } = value
        return request('/api/page',
            {
                method: "get",
                params: {
                    page, pageSize, name, path, status
                }
            }
        )
    } else {
        return request('/api/page',
            {
                method: "get",
                params: {
                    page, pageSize
                }
            }
        )
    }


}

export function tabUpdata(id:string,value:{status:string}) {
    return request(`/api/page/${id}`,
      {
        method: "patch",
        data:value
      }
   ) 

}

export function delPage(id:string) {
    return request(`/api/page/${id}`,
      {
        method: "delete",
      }
   ) 

}