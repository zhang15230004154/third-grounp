//umi 启动前配置文件
import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {
    compact:true
  },
  // 配置dav
  dva: {
    immer: true,
    hmr: true,
  },
  // mfsu: {},//开启mfsu加速方案
  // proxy: {
  //   '/api': {
  //     target: 'https://creationapi.shbwyz.com/',
  //     pathRewrite: { '^/api': '' },
  //     changeOrigin: true
  //   }
  // }
});
