'use strict';
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  /**
   *  Method	Path	Route Name	Controller.Action
    GET	/mail	mail	app.controllers.mail.index
    GET	/mail/new	new_post	app.controllers.mail.new
    GET	/mail/:id	post	app.controllers.mail.show
    GET	/mail/:id/edit	edit_post	app.controllers.mail.edit
    POST	/mail	mail	app.controllers.mail.create
    PUT	/mail/:id	post	app.controllers.mail.update
    DELETE	/mail/:id	post	app.controllers.mail.destroy
   */
 
  // Restful方式添加邮件路由
  router.resources('/mail', controller.mail);
  // Restful方式添加邮件路由
  router.resources('/pay', controller.pay);
  // Restful方式添加文件路由
  router.resources('/file', controller.file);
  router.get('/file/getBucket', controller.file.listBuckets);
  router.get('/file/listFile', controller.file.listFile);
  router.post('/file/saveChunk', controller.file.saveChunk);
  router.post('/file/mergeChunk', controller.file.mergeChunk);
};

